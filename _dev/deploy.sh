#!/bin/bash

DIR=$(dirname $0)

. ${DIR}/functions.sh

target=$1

echo "Use 'all' option to deploy to all machines."

if [ $(test_target "$target" "pi") -eq 0 ] ; then
    host="pi@192.168.1.48"
    #mounting swap
    ssh "$host" "sudo swapon /home/pi/swap.dd"
    send_src "$host" "/home/pi/schedule3000/src" "/home/pi/schedule3000/build" "."
    #unmounting swap
    ssh "$host" "sudo swapoff /home/pi/swap.dd ; sudo killall schedule3000"
fi

if [ $(test_target "$target" "playground" ) -eq 0 ] ; then
    send_src "administrator@172.16.1.240" "/home/administrator/schedule3000/src" "/home/administrator/schedule3000/build" "."
fi

if [ $(test_target "$target" "bar2" ) -eq 0 ] ; then
    send_src "plasmb2@192.168.1.179" "/home/plasmb2/schedule3000/src" "/home/plasmb2/schedule3000/build" "."
    ssh -t "$host" "sudo killall schedule3000"
fi

if [ $(test_target "$target" "schedule_kassa" ) -eq 0 ] ; then
    send_src "administrator@192.168.1.123" "/home/administrator/schedule3000/src" "/home/administrator/schedule3000/build" "."
    #ssh -t "$host" "sudo killall schedule3000"
fi

if [ $(test_target "$target" "manga" ) -eq 0 ] ; then
    send_src "administrator@192.168.1.33" "/home/administrator/schedule3000/src" "/home/administrator/schedule3000/build" "."
    #ssh -t "$host" "sudo killall schedule3000"
fi
if [ $(test_target "$target" "taxi" ) -eq 0 ] ; then
    send_src "administrator@192.168.1.132" "/home/administrator/schedule3000/src" "/home/administrator/schedule3000/build" "."
    #ssh -t "$host" "sudo killall schedule3000"
fi

