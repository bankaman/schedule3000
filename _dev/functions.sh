#!/bin/bash

function test_target(){
    target=$1
    current=$2
    if [ "$target" == "$current" ] || [ "$target" == "all" ]; then
        echo 0
    else
        echo 1
        echo "skipping '$current' " >&2
    fi
}

function send_src(){
    host=$1
    rfold=$2
    bfold=$3
    myfold=$4    

    
    rsync --exclude "/.hg/*" --exclude "/.git/*" --exclude "/img/*" --exclude "*.pro.user*" --exclude "/music/*" --exclude "/video/*" --progress -rt --rsync-path="mkdir -p '${rfold}' && rsync"  --del "${myfold}" "${host}:${rfold}"
    
    #building
    script=$(cat <<SCRIPT
    if [ ! -d "${bfold}" ] ; then
        mkdir "${bfold}"
    fi
    
    type qmake >/dev/null 2>&1 || { 
        cd "${rfold}/_dev/"
        sudo ./requirements_instaill.sh
    }
    
    cd "${bfold}";
    qmake "${rfold}";
    make;
SCRIPT
)
    ssh -t "$host" "$script"
}
