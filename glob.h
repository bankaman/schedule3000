#ifndef GLOB_H
#define GLOB_H

#include <SDL2/SDL.h>
#include <vlc/vlc.h>

#if SDL_COMPILEDVERSION<2005
    #error You need libsdl version at least 2.0.5 or bigger.
#endif

//#include "localdatabase.h"

class LocalDatabase;

class G
{
public:
    G(){}
    static bool debug;
    static libvlc_instance_t* vlc_instance;
    //static LocalDatabase* db;


    //filetype constants
    static const int PLAYLIST_TYPE_IMAGE  = 0;
    static const int PLAYLIST_TYPE_MUSIC  = 1;
    static const int PLAYLIST_TYPE_VIDEO  = 2;
};

#endif // GLOB_H
