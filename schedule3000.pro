TEMPLATE = app
#CONFIG+=debug
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += qt
win32 {
    include (lib/vlc/libvlc-sdk.pri)
    LIBS += -lmingw32 -lSDL2main -mwindows -lm -ldinput8 -ldxguid -ldxerr8 -luser32 -lgdi32 -lwinmm -limm32 -lole32 -loleaut32 -lshell32 -lversion -luuid -static-libgcc
}

unix {
    LIBS += -lvlc
}

QT += sql

LIBS += -lSDL2 -lSDL2_image -lSDL2_ttf -lmicrohttpd


SOURCES += main.cpp \
    texture.cpp \
    sdlmain.cpp \
    schedule.cpp \
    glob.cpp \
    modulegeneric.cpp \
    modules/imageviewer.cpp \
    modules/vlcmusicplayer.cpp \
    modules/vlcvideoplayer.cpp \
    modules/clockoverlay.cpp \
    webserver.cpp \
    scheduleelement.cpp \
    localdatabase.cpp \
    api.cpp \
    windowctr.cpp \
    modules/dublicator.cpp \
    api/new_playlist.cpp \
    api/get_playlists.cpp \
    api/add_event_from_template.cpp \
    api/add_event.cpp \
    api/upload_file.cpp \
    api/upload_file_part.cpp \
    api/get_playlist_files.cpp \
    api/delete_file_from_disk.cpp \
    api/get_api_functions_list.cpp \
    modules/js_module.cpp \
    duktape/src/duktape.c \
    api/get_modules.cpp \
    duktape_helpers/console/duk_console.c \
    api/get_timeline.cpp \
    api/remove_event_from_timeline.cpp \
    api/get_ram_timeline.cpp \
    api/remove_playlist.cpp \
    api/get_playlist_duration.cpp \
    api/set_playlist_settings.cpp \
    api/get_playlist_settings.cpp \
    api/get_windows_count.cpp \
    modules/logo_anim.cpp \
    modules/libvlcplayer.cpp \
    font.cpp \
    api/set_event_params.cpp


HEADERS += \
    texture.h \
    sdlmain.h \
    schedule.h \
    glob.h \
    modulegeneric.h \
    modules/imageviewer.h \
    modules/vlcmusicplayer.h \
    modules/vlcvideoplayer.h \
    modules/clockoverlay.h \
    webserver.h \
    scheduleelement.h \
    localdatabase.h \
    api.h \
    windowctr.h \
    modules/dublicator.h \
    api/new_playlist.h \
    api/get_playlists.h \
    api/add_event_from_template.h \
    api/add_event.h \
    api/upload_file.h \
    api/upload_file_part.h \
    api/get_playlist_files.h \
    api/delete_file_from_disk.h \
    api/get_api_functions_list.h \
    modules/js_module.h \
    duktape/src/duktape.h \
    duktape/src/duk_config.h \
    api/get_modules.h \
    duktape_helpers/console/duk_console.h \
    api/get_timeline.h \
    api/remove_event_from_timeline.h \
    api/get_playlist_duration.h \
    api/get_ram_timeline.h \
    api/remove_playlist.h \
    api/get_playlist_duration.h \
    api/set_playlist_settings.h \
    api/get_playlist_settings.h \
    api/get_windows_count.h \
    modules/logo_anim.h \
    modules/libvlcplayer.h \
    font.h \
    api/set_event_params.h

RESOURCES     = \
    resources.qrc
