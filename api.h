/*
 * Schedule3000 api class
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#ifndef API_H
#define API_H

#include <qbytearray.h>
#include <qstring.h>
#include <qjsondocument.h>
#include <qjsonobject.h>
#include <qjsonarray.h>
#include <qdebug.h>
#include <QFile>

class API_command{
public:
    API_command();
    virtual ~API_command(){}
    virtual void run(QByteArray &buffer, QByteArray &query, QJsonObject &param){Q_UNUSED(buffer);Q_UNUSED(query);Q_UNUSED(param);}

    static void error(QByteArray &buffer, QString error);
    static void ok(QByteArray &buffer);
    static void send(QByteArray &buffer, QJsonObject &object);
    static void clear(QJsonObject& object);
    static bool check_params(QByteArray &buffer, QJsonObject& object, QString params);
protected:
    virtual const char* get_id(){return nullptr;}
};

class API
{
    public:


    API();
    static void process(QByteArray &buffer, QByteArray query);
    static void clear();

    static void register_command(QString command_id, API_command *command);

    struct Command
    {
        QString command_id;
        API_command* command;
    };
    static QVector<Command*> *commands;

};


#endif // API_H
