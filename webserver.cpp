#include "webserver.h"
#include "glob.h"
#include "localdatabase.h"
#include <QMimeType>
#include <QMimeDatabase>

#ifdef WIN32
#include "io.h"
#endif

/*
 * webserver module
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

QString WebServer::password = "";
QString WebServer::user = "";
bool WebServer::enable_https = false;
QString WebServer::https_cert = "";
QString WebServer::https_key = "";

WebServer::WebServer()
{

}

WebServer::~WebServer()
{
    stop();
    this->wait();
}



MHD_Result WebServer::answer_to_connection(void *, MHD_Connection *connection, const char *url, const char *method, const char *, const char *upload_data, size_t *upload_data_size, void **con_cls)
{
    QByteArray buffer;
    bool from_file=true;
    int error = MHD_HTTP_OK;

    MHD_Result ret;

    struct MHD_Response *response;
    //struct MHD_PostProcessor *pp;

    struct iter *it;

    QString mime = "text/html";
    QString filename = url;

    //password check
    if(password.length()>0 && (!filename.startsWith("/errors/img/"))){
        char *user;
        char *pass;
        int fail;
        pass = NULL;
      user = MHD_basic_auth_get_username_password (connection, &pass);
      fail = ( (user == NULL) ||
           (0 != strcmp (user, WebServer::user.toStdString().c_str())) ||
           (0 != strcmp (pass, WebServer::password.toStdString().c_str()) ) );
      if (user != NULL) free (user);
      if (pass != NULL) free (pass);

      if(fail){
          error = MHD_HTTP_FORBIDDEN;
      }
    }

    if(error == MHD_HTTP_OK){
        //uploaded files
        if(filename.startsWith("/files/")){
            QString file_id_str;
            file_id_str=filename.mid(filename.lastIndexOf("/")+1);
            bool ok=true;
            int file_id = file_id_str.toInt(&ok);
            if(ok){
                qDebug()<<"file_id:"<<file_id;
                LocalDatabase db;
                QString fname = db.get_file_path_by_id(file_id);
                if(fname.length()!=0){
                    QFile file(fname);
                    if(file.exists()){
                        if(file.open(QIODevice::ReadOnly)){
                            qDebug()<<"Webserver has opened file: "<<fname<<"size:"<<file.size()<<"handle:"<<file.handle();
                            int handle = file.handle();
                            response = MHD_create_response_from_fd(file.size(),dup(handle));
                            QMimeDatabase db;
                            QMimeType mime = db.mimeTypeForFile(fname);
                            qDebug()<<"mime is:"<<mime.name();
                            MHD_add_response_header(response, "Content-Type", mime.name().toStdString().c_str());
                            ret = MHD_queue_response(connection, error, response);
                            MHD_destroy_response (response);
                            file.close();
                            return ret;
                        }
                    }
                }
            }
        }

        //api processing
        if((strcmp(url,"/api/")==0)||(strcmp(url,"/api")==0)){
            if(0==strcmp(method,"POST")){

                mime = "application/json";

                from_file=false;

                it=(iter*)*con_cls;
                if(NULL==it){
                    it=new iter;
                    *con_cls = it;
                    it->iter=0;
                    it->buffer.clear();
                    return MHD_YES;
                }
                it->iter++;
                if (0 != *upload_data_size){
                      it->buffer.append(upload_data, (long int)*upload_data_size);
                      *upload_data_size = 0;
                      return MHD_YES;
                }
                API::process(buffer,it->buffer);
                delete it;
            }else{
                filename="/errors/use_post.html";
            }
        }
        //static files
        if(from_file){

            if(filename == "/")
                filename = "/index.html";
            QFile fpage(":/html"+filename);
            if(!fpage.exists()){
                qDebug()<<"Error 404: "<<filename;
                error = MHD_HTTP_NOT_FOUND;
            }else{
                if(filename.endsWith(".css"))
                    mime = "text/css";
                if(filename.endsWith(".svg"))
                    mime = "image/svg+xml";
                if(filename.endsWith(".jpg"))
                    mime = "image/jpeg";
                if(filename.endsWith(".js"))
                    mime = "application/javascript";

                fpage.open(QIODevice::ReadOnly);
                buffer = fpage.readAll();
                fpage.close();
            }
        }
    }

    if(error==MHD_HTTP_NOT_FOUND){
        QFile fpage(":/html/errors/404.html");
        fpage.open(QIODevice::ReadOnly);
        buffer = fpage.readAll();
        fpage.close();
    }

    if(error==MHD_HTTP_FORBIDDEN){
        QFile fpage(":/html/errors/403.html");
        fpage.open(QIODevice::ReadOnly);
        buffer = fpage.readAll();
        fpage.close();
    }

    //forming page
    void *p = malloc(buffer.length());
    memcpy(p,buffer.data(),buffer.length());


    response = MHD_create_response_from_buffer (buffer.length(), p,
                     MHD_RESPMEM_MUST_FREE);
    MHD_add_response_header(response, "Content-Type", mime.toStdString().c_str());
    if(error == MHD_HTTP_FORBIDDEN){
        ret = MHD_queue_basic_auth_fail_response (connection,
                                "Please say me who you are [^_^]",
                                response);
    }else{
        ret = MHD_queue_response (connection, error, response);
    }
    MHD_destroy_response (response);

    return ret;
}

void WebServer::run()
{
    if(enable_https){
        QFile cert_file(https_cert);
        QFile key_file(https_key);
        if(!cert_file.exists() || !key_file.exists()){
            qDebug()<<"error starting web server '"<<https_cert<<"' exists: "<<cert_file.exists()<<" '"<<https_key<<"' exists: "<<key_file.exists();
            return;
        }
        if(!cert_file.open(QIODevice::ReadOnly)){
            qDebug()<<"error starting web server| Cant open cert file "<<cert_file.fileName();
            return;
        }
        if(!key_file.open(QIODevice::ReadOnly)){
            qDebug()<<"error starting web server| Cant open key file "<<key_file.fileName();
            return;
        }
        QByteArray cert = cert_file.readAll();
        QByteArray key = key_file.readAll();

        daemon = MHD_start_daemon (MHD_USE_SELECT_INTERNALLY | MHD_USE_SSL,
             port, NULL, NULL,
             &WebServer::answer_to_connection, NULL,
             MHD_OPTION_HTTPS_MEM_KEY, key.data(),
             MHD_OPTION_HTTPS_MEM_CERT, cert.data(),
             MHD_OPTION_END);

    }else{
        daemon = MHD_start_daemon (MHD_USE_SELECT_INTERNALLY, port, NULL, NULL,
        &WebServer::answer_to_connection, NULL, MHD_OPTION_END);
    }

    if (NULL == daemon){
        qDebug()<<"error starting web server";
        return;
    }

    do_run = true;
    while (do_run) {
        sleep(1);
    }

    MHD_stop_daemon (daemon);
}

void WebServer::stop()
{
    do_run = false;
}

void WebServer::set_port(int port)
{
    this->port = port;
}

QString WebServer::getFilename(QString url)
{
    return url.mid(url.lastIndexOf("/"));
}
