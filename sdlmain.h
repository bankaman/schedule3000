#ifndef SDLMAIN_H
#define SDLMAIN_H

/*
 * libsdl wrapper class
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <QDebug>
#include <QImage>

#include "modulegeneric.h"
#include "glob.h"

class SDLMain
{
public:
    SDLMain();
    virtual ~SDLMain();
    void pre_render();
    void create_window(int x, int y, int width, int height, bool borderless, bool fullscreen);
    void create_renderer();
    void input_handler(SDL_Event &e);
    SDL_Renderer *get_renderer();
    void set_window_id(int win_id);
    int get_width();
    int get_height();
    SDL_Window* get_window();

    virtual void load_timeline(){}
    virtual void on_keydown(SDL_Event &key_code){}
    virtual void on_keyup(SDL_Event &key_code){}

    bool start_text_input_mode(ModuleGeneric* module);
    bool stop_text_input_mode();



protected:
    SDL_Renderer* render;
    SDL_Window* window;
    int window_id;

private:
    int WIDTH=1024;
    int HEIGHT=576;

    SDL_DisplayMode fullscreen_mode = {SDL_PIXELFORMAT_RGBA8888,1920,1080,60,nullptr};

    //int current_w = WIDTH, current_h = HEIGHT;
    bool is_fullscreen=false;
    bool enter_down=false;

    virtual void rendering(SDL_Renderer* r){Q_UNUSED(r);}


    uint sdl_window_id;

    bool text_input_mode;
    ModuleGeneric* text_input_mode_user;

};

#endif // SDLMAIN_H
