/*
 * Schedule3000 api class
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include "api.h"
#include "glob.h"
#include "windowctr.h"

QVector<API::Command*> *API::commands=nullptr;

API::API()
{

}
//------------------processing request -----------------------
void API::process(QByteArray &buffer, QByteArray query)
{
    buffer.clear();

    QJsonDocument request;
    int zeroindex = query.indexOf((char)0);
    if(zeroindex>0)
        request = QJsonDocument::fromJson(query.mid(0,zeroindex));
    else
        request = QJsonDocument::fromJson(query);
    if(request.isNull()){
        API_command::error(buffer,"Error parsing json");
        return;
    }

    QJsonObject obj = request.object();
    QJsonObject param;

    if(!API_command::check_params(buffer, obj, "command,params"))
        return;

    QString com = obj.value("command").toString();
    param = obj.value("params").toObject();

    for(int i=0;i<commands->length();i++){
        if(com == commands->at(i)->command_id){
            commands->at(i)->command->run(buffer,query,param);
            return;
        }
    }
    return API_command::error(buffer, "Error! Unknown command: '"+com+"', try to use command 'get_api_functions_list'.");
}

void API::clear()
{

    for(int i=0;i<commands->length();i++)
        delete commands->at(i);
    delete commands;
}

void API::register_command(QString command_id, API_command *command)
{
    qDebug()<<"register api command "<<command_id;
    Command* com;
    com = new Command;
    com->command_id = command_id;
    com->command = command;
    if(commands == nullptr){
        commands = new QVector<Command*>;
    }

    commands->append(com);
}
//------------------api functions ---------------------------

//------------------helpful functions ------------------------
API_command::API_command(){}

void API_command::error(QByteArray &buffer, QString error)
{
    QJsonObject object{
                         {"result",""},
                         {"error",error}
                       };
    send(buffer,object);
}

void API_command::ok(QByteArray &buffer)
{
    QJsonObject object{
                         {"result","ok"},
                         {"error",""}
                       };
    send(buffer, object);
}

void API_command::send(QByteArray &buffer, QJsonObject& object)
{
    QJsonDocument doc;
    doc.setObject(object);
    buffer.append(doc.toJson());
}

void API_command::clear(QJsonObject& object)
{
    QStringList keys = object.keys();
    for(int i=0;i<keys.length();i++){
        object.remove(keys.at(i));
    }
}
/**
 * @brief checking is params in object
 * @param buffer
 * @param object
 * @param params example: "param1,param2"
 */
bool API_command::check_params(QByteArray &buffer, QJsonObject &object, QString params)
{
    QStringList list = params.split(",");
    for(int i=0;i<list.length();i++){
        if(object.value(list.at(i)).isUndefined()){
            error(buffer,"Error! Parameter '"+list.at(i)+"' is required!");
            return false;
        }
    }
    return true;
}
