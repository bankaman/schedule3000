/*
 * generic module class
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include "modulegeneric.h"
#include "windowctr.h"

ModuleGeneric::ModuleGeneric()
{
    mode = MODE_NORMAL;
    state = STATE_IDLE;
    module_name="UNK";
}

ModuleGeneric::~ModuleGeneric()
{
}

void ModuleGeneric::setWindow(int _window_id)
{
    window_id = _window_id;
    renderer = WindowCtr::get_renderer(window_id);
}

void ModuleGeneric::setMode(QString mode_string)
{
    if(mode_string == "normal") mode = MODE_NORMAL;
    if(mode_string == "permanent") mode = MODE_PERMANENT;
    if(mode_string == "daily") mode = MODE_DAILY;
}

void ModuleGeneric::setModuleName(QString _module_name)
{
    module_name = _module_name;
}

void ModuleGeneric::setPlaylistSettings(QVariantHash settings)
{
    qDebug()<<"setPlaylistSettings"<<settings;
    playlist_random = settings.value("is_random",0) == 1;
}

int ModuleGeneric::getDuration()
{
    return -1;
}

int ModuleGeneric::getMode()
{
    return mode;
}

void ModuleGeneric::setMode(int _mode)
{
    mode = _mode;
}

int ModuleGeneric::getState()
{
    return state;
}

