/*
 * local database class
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#ifndef LOCALDATABASE_H
#define LOCALDATABASE_H

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QVariant>
#include <QVector>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantHash>
#include <QTextCodec>

class LocalDatabase
{
public:
    LocalDatabase();
    ~LocalDatabase();

    //void get_timeline(int window_id, QVector<ScheduleElement*>* timeline);
    void set_as_played(int rowid);
    void set_as_skipped(int rowid);
    void set_duration(int rowid, int duration);
    void set_start_time(int rowid, int datetime_timeatamp);

    void set_file_as_uploaded(int file_id);
    void append_file_to_paylist(int file_id, int playlist_id);

    bool delete_event(int rowid);

    //api
    bool new_playlist(QString name, int type);
    bool add_event(int window_id, QString module, QString mode, uint time_begin, int priority, int draw_order, QString params, QString playlist_name);
    bool add_from_template(QString name, int time_begin);
    QStringList get_playlists();
    QJsonArray get_all_timeline();
    int create_file(QString filename, int type, long unsigned int byte_length);
    int get_playlist_id_by_name(QString name);
    QStringList get_playlist_filenames(int playlist_id);
    QJsonArray get_playlist_files(int playlist_id);
    QStringList get_playlist_filepaths(int playlist_id);
    QString get_file_path_by_id(int file_id);
    bool delete_file_from_disk(int file_id);
    bool remove_event_from_timeline(int event_id);
    int get_playlist_type(int playlist_id);
    int get_playlist_duration(int playlist_id);
    bool delete_playlist(int playlist_id);
    bool update_playlist_duration(int playlist_id, int duration);
    QJsonObject get_playlist_settings(int playlist_id);
    bool set_playlist_settings(int playlist_id, bool is_random);
    bool set_event_params(int event_id, QString params);

    QString get_last_error();

private:
    QSqlDatabase db;
    //QSqlQuery *query;

    QString last_error;

    bool query_execute(QSqlQuery &query ,QString query_string);
    bool query_execute(QSqlQuery &query );
    bool table_exsist(QString table_name);

    static bool first_init;
    static int next_id;
    int my_id;
};

#endif // LOCALDATABASE_H
