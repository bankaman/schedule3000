/*
 * Schedule3000 main
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include <stdio.h>
#include <stdlib.h>

#include "windowctr.h"


#include <QtCore/QCoreApplication>


void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
    case QtDebugMsg:
        //fprintf(stderr, "Debug: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fprintf(stderr, "%s> %s\n",QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss").toStdString().c_str() ,localMsg.constData());
        break;
    case QtInfoMsg:
        fprintf(stderr, "Info: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtWarningMsg:
        fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        abort();
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    qInstallMessageHandler(myMessageOutput);

    //qDebug()<<LIBVLC_VERSION_MAJOR<<LIBVLC_VERSION_MINOR<<LIBVLC_VERSION_REVISION;

    WindowCtr::init();
    return WindowCtr::start();




}
