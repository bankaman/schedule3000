#ifndef FONT_H
#define FONT_H

#include <string>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <QString>


class Font
{
public:
    Font();
    ~Font();

    bool load(SDL_Renderer *r, QString filename);
    void drawText(std::string &str,int x, int y);
    void drawText(QString &str,int x, int y);
    int get_height();
    int get_width();
    int get_text_width(std::string &str);
    int get_text_width(QString &str);
    void set_color(SDL_Color color);
    void set_size(int _size);
    void return_color();

    void destroy_font();

private:
    bool loaded;
    int size;
    int h,w;
    TTF_Font *font=nullptr;

    SDL_Renderer* render;
    SDL_Texture* sdl_texture;

    SDL_Rect position;
    SDL_Rect clip_rect;
    SDL_Point center;

    SDL_Color textColor;
    SDL_Color old_textColor;

    QString font_filename;

};

#endif // FONT_H
