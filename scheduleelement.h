#ifndef SCHEDULEELEMENT_H
#define SCHEDULEELEMENT_H

/*
 * schedule element class
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include <QDebug>
#include <QVariantHash>

#include "modulegeneric.h"

#include "modules/imageviewer.h"
#include "modules/clockoverlay.h"
#include "modules/vlcmusicplayer.h"
#include "modules/vlcvideoplayer.h"
#include "modules/dublicator.h"
#include "modules/js_module.h"

class ScheduleElement
{
public:
    ScheduleElement(QJsonObject obj);
    ScheduleElement(
            int _rowid,
            int _playlist_id,
            uint starttime,
            int _priority,
            int _draw_order,
            int _window_id,
            QString _module_name,
            QString _module_mode,
            QVariantHash _params);
    ~ScheduleElement();

    void draw();
    void setMode(int mode);
    int load();
    int getMode();
    int getState();
    void start();
    void pause();
    void pause(int element_rowid);
    void resume();
    void fadein();
    void fadeout();
    void seek(uint64_t position);
    int getDuration();
    QString getModuleName();
    void unload();
    void updatePlaylist(int _playlist_id);

    void setParams(QString params);

    bool is_error();

    ModuleGeneric *getModule();

    int rowid;
    bool is_played;
    uint starttime;
    int priority;
    int duration;
    int draw_order;
    int playlist_id;
    int paused_by;
    int window_id;
    QString module_name;

private:
    QVariantHash params;
    ModuleGeneric* module = nullptr;

    int mode;

    void log(QString func);

    void create_module(QString m_name);

    void set_up(int _rowid,
                int _playlist_id,
                uint starttime,
                int _priority,
                int _draw_order,
                int _window_id,
                QString _module_name,
                QString _module_mode,
                QVariantHash _params);

};

#endif // SCHEDULEELEMENT_H
