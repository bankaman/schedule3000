
/*
 * libsdl2 texture wrapper class
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include "texture.h"
#include "windowctr.h"

Texture::Texture()
{
    sdl_texture=NULL;
    sdl_surface = NULL;
    is_loaded=false;
    origW = 0;
    origH = 0;
    filename="<NONE>";
}
Texture::~Texture()
{
    SDL_DestroyTexture(sdl_texture);
    destroy_surface();
}

void Texture::destroy_surface()
{
    if(sdl_surface!=NULL){
        SDL_FreeSurface(sdl_surface);
        sdl_surface=NULL;
    }
}

/**
 * @brief constructing texture for streameng data into it
 * @param r - renderer
 * @param width
 * @param height
 * @param format
 */
void Texture::createForStreaming(SDL_Renderer *r, int width, int height, char* format)
{
    int fmt=-1;

    if( (strcmp(format,"YV12")==0) ||
        (strcmp(format,"I420")==0) )
            fmt = SDL_PIXELFORMAT_YV12;
    if(strcmp(format,"RGB32")==0)
        fmt = SDL_PIXELFORMAT_RGB888;
    if(strcmp(format,"RGBA32")==0)
        fmt = SDL_PIXELFORMAT_BGRA32;


    if(fmt==-1){
        qWarning()<<"unknown pixel '"<<filename<<"' format: "<<format;
        fmt=SDL_PIXELFORMAT_YV12;
    }

    if(sdl_texture!=nullptr){
        SDL_DestroyTexture(sdl_texture);
        sdl_texture=nullptr;
    }
    render=r;

    sdl_texture = SDL_CreateTexture(
                r,
                fmt, SDL_TEXTUREACCESS_STREAMING,
                width, height);
    if(sdl_texture == nullptr){
        qWarning()<<"Error creating texture '"<<filename<<"' : "<<IMG_GetError();
    }
    alpha=255;

    position.w = width;
    position.h = height;
    position.x=0;
    position.y=0;
    clip_rect.w=position.w;
    clip_rect.h=position.h;
    clip_rect.x=position.x;
    clip_rect.y=position.y;
    origW = position.w;
    origH = position.h;

    center.x=position.w/2;
    center.y=position.h/2;

    setBlendMode(SDL_BLENDMODE_BLEND);
    is_loaded=true;
}

void Texture::setBlendMode(SDL_BlendMode blending){
    SDL_SetTextureBlendMode(sdl_texture, blending);
}


Uint8 Texture::getAlpha(){
    return alpha;
}

void Texture::setAlpha(Uint8 alpha_){
    alpha=alpha_;
    if(!is_loaded)
        return;
    SDL_SetTextureAlphaMod(sdl_texture, alpha);
}

void Texture::resizeToScreen(int window_id)
{
    int w=WindowCtr::get_width(window_id);
    int h=WindowCtr::get_height(window_id);
    position.w = w;
    position.h = position.w * getOrigH() / getOrigW();
    if(position.h > h){
        position.h = h;
        position.w = position.h * getOrigW() / getOrigH();
    }
    position.x=w/2-position.w/2;
    position.y=h/2-position.h/2;
}

void Texture::createBlack(SDL_Renderer *r)
{

    sdl_surface = SDL_CreateRGBSurface(0,16,16,32,0,0,0,0);

    SDL_FillRect(sdl_surface,nullptr,SDL_MapRGB(sdl_surface->format, 0, 0, 0));

    if(!construct_texture(r)){
        qDebug()<<"error creating black texture";
    }
}

void Texture::rotate(double angle_){
    angle=angle_;
}

void Texture::setClipRect(int x, int y, int w, int h)
{
    clip_rect.x = x;
    clip_rect.y = y;
    clip_rect.w = w;
    clip_rect.h = h;
    if(clip_rect.w>position.w) clip_rect.w=position.w;
    if(clip_rect.w>position.h) clip_rect.h=position.h;
    if(clip_rect.w<0) clip_rect.w=0;
    if(clip_rect.h<0) clip_rect.h=0;
}

void Texture::setFlip(bool horisontal, bool vertical)
{
    flip = SDL_FLIP_NONE;

    if(horisontal)
        flip = (SDL_RendererFlip)(SDL_FLIP_HORIZONTAL|flip);
    if(vertical)
        flip = (SDL_RendererFlip)(SDL_FLIP_VERTICAL|flip);
}

//Эффекты
void Texture::fadeout(Uint8 fade){
    fadein_mode=false;
    fadeout_mode=true;
    fade_speed=fade;
}
void Texture::fadein(Uint8 fade){
    fadeout_mode=false;
    fadein_mode=true;
    fade_speed=fade;
}

/**
 * @brief Loading image from file system
 * @param r - Renderer
 * @param path
 * @return false if fails
 */
bool Texture::load(SDL_Renderer* r, QString path){
    if(!preload_surface(path))
        return false;
    if(!construct_texture(r))
        return false;
    return true;
}

/**
 * @brief loads image from QT resource system
 * @param path
 * @return false if fails
 */
bool Texture::load_from_resources(SDL_Renderer* renderer, QString path)
{
    if(!path.startsWith(":"))
        return false;

    filename = path;

    QImage img;
    img.load(path);

    bool known_format = false;
    int color = img.pixelFormat().colorModel();
    int bpp = img.pixelFormat().bitsPerPixel();
    int au  = img.pixelFormat().alphaUsage();
    char format[10];

    if( (color==QPixelFormat::RGB) && (bpp==32) && au==QPixelFormat::IgnoresAlpha){
        strcpy(format,"RGB32");
        known_format=true;
    }
    if( (color==QPixelFormat::RGB) && (bpp==32) && au==QPixelFormat::UsesAlpha){
        strcpy(format,"RGBA32");
        known_format=true;
    }

    if(!known_format){
        qWarning()<<"Unknown pixel format, color:"<<color<<"|"<<" bpp:"<<bpp<<"|alpha"<<au;
        return false;
    }

    SDL_Rect rect;
    rect.h = img.height();
    rect.w = img.width();
    rect.x = 0;
    rect.y = 0;

    createForStreaming(renderer,img.width(),img.height(),format);
    if(     (0==strcmp(format,"RGB32")) ||
            (0==strcmp(format,"RGBA32"))
            )
        SDL_UpdateTexture(sdl_texture,&rect,img.bits(),img.width()*4);

    return true;
}
/**
 * @brief Preloading surface into RAM (if you want to load texture from another thread)
 * @param path
 * @return
 */
bool Texture::preload_surface(QString path)
{
    destroy_surface();

    filename = path;

    sdl_surface = IMG_Load( path.toStdString().c_str() );
    if(sdl_surface == NULL){
        qWarning()<<"Error loading texture: "<<path<<" error:"<<IMG_GetError()<<endl;
        return false;
    }
    alpha=255;

    return true;
}
/**
 * @brief forming texture from surface which was loaded before
 * @param r
 * @return
 */
bool Texture::construct_texture(SDL_Renderer *r)
{
    if(sdl_surface==NULL)
        return false;

    render=r;
    sdl_texture = SDL_CreateTextureFromSurface( r, sdl_surface );
    if(sdl_texture == NULL){
        qWarning()<<"Error creating texture: "<<filename<<" error:"<<IMG_GetError()<<endl;
        return false;
    }

    setBlendMode(SDL_BLENDMODE_BLEND);

    position.w = sdl_surface->w;
    position.h = sdl_surface->h;
    position.x=0;
    position.y=0;
    clip_rect.w=position.w;
    clip_rect.h=position.h;
    clip_rect.x=position.x;
    clip_rect.y=position.y;
    origW = position.w;
    origH = position.h;

    destroy_surface();

    center.x=position.w/2;
    center.y=position.h/2;
    is_loaded=true;
    return true;
}

/**
 * @brief drawing texture on screen
 */
void Texture::draw(){

    if(!is_loaded) return;

    if(alpha>0)
        SDL_RenderCopyEx( render, sdl_texture, &clip_rect, &position, angle, &center, flip);

    //Обработка Эффектов
    if(fadeout_mode){
        if(alpha>0)
            alpha-=fade_speed;
        else{
            alpha=0;
            fadeout_mode=false;
        }
        setAlpha(alpha);
    }
    if(fadein_mode){
        if(alpha<255)
            alpha+=fade_speed;
        else{
            alpha=255;
            fadein_mode=false;
        }
        setAlpha(alpha);
    }

}

int Texture::getOrigW(){
    return origW;
}

int Texture::getOrigH(){
    return origH;
}

bool Texture::getIsLoaded()
{
    return is_loaded;
}

SDL_Texture *Texture::getTexturePointer()
{
    return sdl_texture;
}
