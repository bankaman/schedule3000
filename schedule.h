#ifndef SCHEDULE_H
#define SCHEDULE_H

/*
 * schedule timeline class
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include "sdlmain.h"
#include "texture.h"
#include "webserver.h"
#include "scheduleelement.h"
#include "localdatabase.h"

#include "modules/imageviewer.h"
#include "modules/vlcmusicplayer.h"
#include "modules/vlcvideoplayer.h"
#include "modules/clockoverlay.h"
#include "modules/logo_anim.h"

#include <QDebug>
#include <QDir>
#include <QMutex>

class ScheduleElement;
class Schedule : public SDLMain
{
public:
    Schedule();
    ~Schedule();

    static Uint32 change_image_callback(Uint32 interval, void* param);
    void load_timeline();
    void add_element(ScheduleElement *el);
    void update_playlist(int playlist_id);
    void remove_event(int event_id);
    void remove_event_by_playlist(int playlist_id);
    void set_event_params(int event_id, QString params);
    QVector<ScheduleElement *> get_timeline();
    QVector<ScheduleElement *> get_now_playing();

    ModuleGeneric *get_module(int event_id);

    void on_keydown(SDL_Event &e);
    void on_keyup(SDL_Event &e);

private:
    void rendering(SDL_Renderer* r);

    void delete_from_timeline(int id);
    void pause_by_priority(ScheduleElement*);
    void unpause_by_priority(ScheduleElement*);
    void check_priority_and_unpause(ScheduleElement* el);
    time_t get_next_day_timestamp(time_t time);
    int get_timeline_id_by_rowid(int rowid);
    void order_by_draworder();

    //placegolder
    logo_anim* placeholder;
    bool pholder_loaded;
    void load_placeholder();

    //timeline
    QVector<ScheduleElement*> timeline;

    /*QVector<int> pri_paused_ids;
    QVector<int> pri_paused_count;*/

    QDateTime time;

    QMutex timeline_mutex;
    // SDLMain interface

    void check_timeline_dublic();

    LocalDatabase db;

};

#endif // SCHEDULE_H
