
/*
 * schedule element class
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include "scheduleelement.h"

ScheduleElement::ScheduleElement(QJsonObject obj)
{
    QByteArray par = obj.value("params").toString().toUtf8();

    //qDebug()<<"ScheduleElement:"<<
    set_up(
                obj.value("id").toVariant().toInt(),
                obj.value("playlist_id").toVariant().toInt(),
                obj.value("date_start_sec").toVariant().toUInt(),
                obj.value("priority").toVariant().toInt(),
                obj.value("draw_order").toVariant().toInt(),
                obj.value("window_id").toVariant().toInt(),
                obj.value("module").toString(),
                obj.value("mode").toString(),
                QJsonDocument::fromJson(par).object().toVariantHash()
                );
}

ScheduleElement::ScheduleElement(
        int _rowid,
        int _playlist_id,
        uint _starttime,
        int _priority,
        int _draw_order,
        int _window_id,
        QString _module_name,
        QString _module_mode,
        QVariantHash _params)
{
    set_up(_rowid,_playlist_id,_starttime,_priority,_draw_order,_window_id,_module_name,_module_mode,_params);
}

ScheduleElement::~ScheduleElement()
{
    unload();
}

void ScheduleElement::create_module(QString m_name)
{
    //Разбор типа модуля
    if(m_name == "imageviewer")
        module = reinterpret_cast<ModuleGeneric*>(new ImageViewer());

    if(m_name == "clockoverlay")
        module = reinterpret_cast<ModuleGeneric*>(new ClockOverlay());

    if(m_name == "musicplayer")
        module = reinterpret_cast<ModuleGeneric*>(new VlcMusicPlayer());

    if(m_name == "videoplayer")
        module = reinterpret_cast<ModuleGeneric*>(new VlcVideoPlayer());

    if(m_name == "dublicator")
        module = reinterpret_cast<ModuleGeneric*>(new Dublicator());

    //trying js module
    if(QFile::exists(WindowCtr::js_modules_folder+m_name)){
        js_module *mod = new js_module();
        mod->set_js(m_name);
        module = reinterpret_cast<ModuleGeneric*>(mod);
    }


    if(module==nullptr)
        qDebug()<<"Error! Unknown module: '"<<m_name<<"'";

    qDebug()<<"module "<<module_name<<"was created";
    LocalDatabase db;
    module->setWindow(window_id);
    module->setParams(params);
    module->setPlaylistSettings(db.get_playlist_settings(playlist_id).toVariantHash());
    module->setFileList(db.get_playlist_filepaths(playlist_id));
    module->setMode(mode);
    module->setModuleName(module_name);
}

void ScheduleElement::set_up(int _rowid, int _playlist_id, uint _starttime, int _priority, int _draw_order, int _window_id, QString _module_name, QString _module_mode, QVariantHash _params)
{
    starttime = _starttime;
    params = _params;
    priority = _priority;
    draw_order = _draw_order;
    playlist_id = _playlist_id;
    module_name = _module_name;
    window_id = _window_id;
    module = nullptr;

    qDebug()<<"starttime:"<<starttime;
    qDebug()<<"params:"<<params;

    rowid = _rowid;
    is_played = false;

    mode=-1;
    paused_by=-1;


    QString mode_string;
    mode_string = _module_mode;
    if(mode_string == "normal") mode = ModuleGeneric::MODE_NORMAL;
    if(mode_string == "permanent") mode = ModuleGeneric::MODE_PERMANENT;
    if(mode_string == "daily") mode = ModuleGeneric::MODE_DAILY;

    qDebug()<<"mode_string:"<<mode_string;

    if(mode==ModuleGeneric::MODE_PERMANENT){
        load();
        module->start();
        is_played = false;
    }
}

void ScheduleElement::draw()
{
    module->process();
    module->draw();
}

void ScheduleElement::setMode(int mode)
{
    this->module->setMode(mode);
    this->mode = mode;
}

int ScheduleElement::load()
{
    log("load()");
    if(module==nullptr){
        create_module(module_name);

    }
    module->init();
    return 0;
}
void ScheduleElement::unload()
{
    if(module!=nullptr){
        log("unload()");
        delete module;
        module = nullptr;
    }
}

void ScheduleElement::updatePlaylist(int _playlist_id)
{
    LocalDatabase db;
    if(module==nullptr)
        return;

    if(_playlist_id == playlist_id){
        log("updatePlaylist()");
        module->setFileList(db.get_playlist_filepaths(playlist_id));
    }
}

void ScheduleElement::setParams(QString par_)
{
    qDebug()<<"new params! "<<par_;
    QByteArray par = par_.toUtf8();
    params = QJsonDocument::fromJson(par).object().toVariantHash();
    module->setParams(params);
}

bool ScheduleElement::is_error()
{
    return mode<0;
}

ModuleGeneric *ScheduleElement::getModule()
{
    return module;
}

void ScheduleElement::log(QString func)
{
    qDebug()<<func<<" rowid:"<<rowid<<module_name<<"priority:"<<priority<<"paused_by:"<<paused_by;
}

int ScheduleElement::getMode()
{
    return mode;
}

int ScheduleElement::getState()
{    
    if(module!=nullptr){
        return module->getState();
    }else{
        return ModuleGeneric::STATE_IDLE;
    }
}

void ScheduleElement::start()
{
    log("start()");
    module->start();
}

void ScheduleElement::pause()
{
    log("pause()");
    if(module==nullptr)
        return;
    module->pause();
}

void ScheduleElement::pause(int element_rowid)
{
    log("pause("+QString::number(element_rowid)+")");
    paused_by=element_rowid;
    pause();
}

void ScheduleElement::resume()
{
    log("resume()");
    paused_by=-1;
    if(module!=nullptr)
        module->resume();
}

void ScheduleElement::fadein()
{
    log("fadein()");
    module->fadein();
}

void ScheduleElement::fadeout()
{
    log("fadeout()");
    module->fadeout();
}

void ScheduleElement::seek(uint64_t position)
{
    log("seek("+QString::number(position)+")");
    if(module==nullptr)
        return;
    module->seek(position);
}

int ScheduleElement::getDuration()
{
    /*if(duration>=0)
        return duration;*/
    if(module==nullptr)
        return -1;
    duration = module->getDuration();
    return duration;
}

QString ScheduleElement::getModuleName()
{
    return module_name;
}






