/*
 * schedule timeline class
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include "schedule.h"

Schedule::Schedule() : SDLMain(){
    pholder_loaded = false;
}

Schedule::~Schedule()
{
    ScheduleElement* el;
    foreach (el, timeline) {
        delete el;
    }

    if(pholder_loaded)
        delete placeholder;
}

void Schedule::rendering(SDL_Renderer*){

    check_timeline_dublic();

    timeline_mutex.lock();
    time = QDateTime::currentDateTimeUtc();
    ScheduleElement* el;
    for(int i=0;i<timeline.length();i++){
        el = timeline.at(i);
        if(el->is_played)
            continue;

        //Проверки отрисовки
        if(el->getMode()==ModuleGeneric::MODE_PERMANENT){
            timeline_mutex.unlock();
            el->draw();
            timeline_mutex.lock();
        }else{
            switch(el->getState()){
                case ModuleGeneric::STATE_FADE:
                case ModuleGeneric::STATE_PLAYING:
                case ModuleGeneric::STATE_PAUSED:
                    timeline_mutex.unlock();
                    el->draw();
                    timeline_mutex.lock();
                break;
            }
        }

        //Проверка линии времени
        if(el->getMode()!=ModuleGeneric::MODE_PERMANENT){
            switch(el->getState()){
                case ModuleGeneric::STATE_IDLE:
                    if(time.toTime_t() > el->starttime){
                        if(abs((int)(el->starttime - time.toTime_t()))<5){
                            timeline_mutex.unlock();
                            el->load();
                            el->fadein();
                            timeline_mutex.lock();
                            pause_by_priority(el);
                        }else{
                            if(el->getMode()==ModuleGeneric::MODE_NORMAL){
                                timeline_mutex.unlock();
                                el->load();
                                timeline_mutex.lock();
                                double duration = el->getDuration();
                                duration=duration/1000;
                                qDebug()<<"duration:"<<duration<<"start_time:"<<el->starttime;
                                int seek = (int)time.toTime_t()-el->starttime;
                                qDebug()<<"seek:"<<seek<<(seek>0 && seek<duration);
                                if(seek>0 && seek<duration){
                                    timeline_mutex.unlock();
                                    qDebug()<<"seeking!";
                                    el->seek(seek*1000);
                                    pause_by_priority(el);
                                    timeline_mutex.lock();
                                }else{
                                    db.set_as_skipped(el->rowid);
                                    delete_from_timeline(i);
                                    i--;
                                }
                            }else{
                                el->starttime = get_next_day_timestamp(el->starttime);
                                db.set_start_time(el->rowid, el->starttime);
                                el->unload();
                            }
                        }
                    }
                break;
                case ModuleGeneric::STATE_OVER:
                    unpause_by_priority(el);
                    if(el->getMode()==ModuleGeneric::MODE_NORMAL){
                        el->is_played = true;
                        db.set_as_played(el->rowid);
                        delete_from_timeline(i);
                        i--;
                    }else{
                        //Отправляем сегодня в завтрашний день :-)
                        el->starttime = get_next_day_timestamp(el->starttime);
                        db.set_start_time(el->rowid, el->starttime);
                        el->unload();
                    }
                break;
            }
        }
        //проверка элементов запаузенного несуществующим событием
        /*if(el->paused_by>0 && get_timeline_id_by_rowid(el->paused_by)<0 && el->getState()==ModuleGeneric::STATE_PLAYING)
            check_priority_and_unpause(el);
*/

    }
    //placeholder if timeline is empty
    if(timeline.length()==0){
        if(!pholder_loaded)
            load_placeholder();
        //placeholder.resizeToScreen(window_id);
    }
    if(pholder_loaded){
        placeholder->draw();
        //qDebug()<<"placeholder state:"<<placeholder->getState();
        if(timeline.length()>0 && placeholder->getState()==ModuleGeneric::STATE_PLAYING){
            placeholder->fadeout();
        }
        if(placeholder->state==ModuleGeneric::STATE_OVER){
            delete placeholder;
            pholder_loaded=false;
        }
    }
    timeline_mutex.unlock();
}

void Schedule::delete_from_timeline(int id)
{   
    ScheduleElement *el;
    el = timeline.at(id);
    el->unload();
    unpause_by_priority(el);
    delete el;
    //qDebug()<<"timeline length:"<<timeline.length();
    timeline.removeAt(id);
    qDebug()<<"timeline length:"<<timeline.length();
    //qFatal("");
}

void Schedule::pause_by_priority(ScheduleElement* m_el)
{
    qDebug()<<"pause_by_priority()";
    if(m_el->priority<0)
        return;

    QVector<ScheduleElement*> now_playing = get_now_playing();
    foreach (ScheduleElement* npe, now_playing) {
        if(npe->priority>m_el->priority){
            m_el->pause(npe->rowid);
            qDebug()<<"    self_paused: "<<m_el->rowid;
            return;
        }
    }

    foreach (ScheduleElement* npe, now_playing) {
        if(
            (npe->priority>=0) &&
            (npe->priority<m_el->priority) &&
            (npe->getState()==ModuleGeneric::STATE_PLAYING)
          )
        {
            qDebug()<<"    pausing: "<<npe->rowid;
            npe->pause(m_el->rowid);

        }
    }
}

void Schedule::unpause_by_priority(ScheduleElement* m_el)
{
    qDebug()<<"unpause_by_priority("<<m_el->rowid<<") state:"<<m_el->getState();
    ScheduleElement* el;
    for(int i=0; i<timeline.length(); i++){
        el=timeline.at(i);
        if(m_el->rowid==el->paused_by)
            check_priority_and_unpause(el);
    }
}

void Schedule::check_priority_and_unpause(ScheduleElement* el)
{
    bool do_pause=false;
    QVector<ScheduleElement*> now_playing = get_now_playing();
    foreach (ScheduleElement* npe, now_playing) {
        if(npe->priority>el->priority){
            el->pause(npe->rowid);
            do_pause=true;
            break;
        }
    }
    if(!do_pause){
        qDebug()<<"    unpause: "<<el->rowid;
        el->resume();
    }
}

time_t Schedule::get_next_day_timestamp(time_t stime)
{
    time_t t = stime;
    while(t<time.toTime_t()){
        t+=24*60*60;
    }
    return t;
}

int Schedule::get_timeline_id_by_rowid(int rowid)
{
    ScheduleElement *el;
    for(int i=0;i<timeline.length();i++){
        el = timeline.at(i);
        if(el->rowid == rowid)
            return i;
    }
    return -1;
}

void Schedule::order_by_draworder()
{
    int max=timeline.length();
    int i;
    ScheduleElement *temp;
    while(max>=0){
        for(i=0;i<max-1;i++){
            if(timeline[i]->draw_order>timeline[i+1]->draw_order){
                temp = timeline[i];
                timeline[i] = timeline[i+1];
                timeline[i+1] = temp;
            }
        }
        max--;
    }
}

void Schedule::remove_event_by_playlist(int playlist_id)
{
    timeline_mutex.lock();
    ScheduleElement *el;
    for (int i=0;i<timeline.length();i++) {
        el=timeline.at(i);
        if(playlist_id == el->playlist_id){
            delete_from_timeline(i);
        }
    }
    timeline_mutex.unlock();
}

void Schedule::set_event_params(int event_id, QString params)
{
    //qDebug()<<"set event params "<<event_id<<params;
    for(int i=0;i<timeline.length();i++){
        if(timeline[i]->rowid==event_id){
            timeline[i]->setParams(params);
        }
    }
}

void Schedule::load_placeholder()
{
    if(pholder_loaded)
        return;
    placeholder = new logo_anim();
    placeholder->setWindow(window_id);
    placeholder->fadein();

    pholder_loaded=true;
}

void Schedule::check_timeline_dublic()
{
    timeline_mutex.lock();
    ScheduleElement *e1;
    ScheduleElement *e2;
    for(int i=0;i<timeline.length();i++){
        e1 = timeline[i];
        for(int j=0;j<timeline.length();j++){
            if(i==j) continue;
            e2 = timeline[j];
            if(
                (e1->playlist_id == e2->playlist_id) &&
                (e1->starttime == e2->starttime)
            ){
                qDebug()<<"dublicate elements! i:"<<i<<" j:"<<j<<"rowid1:"<<e1->rowid<<" rowid2:"<<e2->rowid;
                //qFatal("");
            }
        }
    }
    timeline_mutex.unlock();
}


void Schedule::load_timeline()
{

    //db.get_timeline(window_id, &timeline);
    QJsonArray ar = db.get_all_timeline();
    ScheduleElement *el;
    for(int i=0;i<ar.size();i++){
        if(ar.at(i).toObject().value("window_id").toInt()!=window_id)
            continue;
        el = new ScheduleElement(ar.at(i).toObject());
        add_element(el);
    }
}

void Schedule::add_element(ScheduleElement *el)
{
    qDebug()<<"adding "<<el->rowid;
    //return;
    timeline_mutex.lock();

    ScheduleElement *e2;
    for(int i=0;i<timeline.length();i++){
        e2 = timeline[i];

        if(
            (el->playlist_id == e2->playlist_id) &&
            (el->starttime == e2->starttime)
        ){
            qDebug()<<"adding dublicate elements! i:"<<i<<" rowid1:"<<el->rowid<<" rowid2:"<<e2->rowid;
            //qFatal("FAIL!");
            //el->unload();
            delete el;
            timeline_mutex.unlock();
            return;
        }
    }

    timeline.append(el);
    order_by_draworder();
    timeline_mutex.unlock();

    qDebug()<<el->rowid<<" added!";
}

void Schedule::update_playlist(int playlist_id)
{
    for(int i=0;i<timeline.length();i++){
        timeline[i]->updatePlaylist(playlist_id);
    }
}

void Schedule::remove_event(int event_id)
{
    qDebug()<<"remove_event:"<<event_id<<"len:"<<timeline.length();
    timeline_mutex.lock();
    for(int i=0;i<timeline.length();i++){
        //qDebug()<<"    ? "<<timeline[i]->rowid<<"=="<<event_id;
        if(timeline[i]->rowid==event_id){
            delete_from_timeline(i);
            //qDebug()<<"        remove_event event deleted:"<<event_id;
            break;
        }
    }
    //qDebug()<<"remove_event end:"<<event_id;
    timeline_mutex.unlock();
}

QVector<ScheduleElement *> Schedule::get_timeline()
{
    return timeline;
}

QVector<ScheduleElement *> Schedule::get_now_playing()
{
    QVector<ScheduleElement*> ret;
    foreach (ScheduleElement* el, timeline) {
        if(el->getState()==ModuleGeneric::STATE_PLAYING || el->getState()==ModuleGeneric::STATE_PAUSED)
            ret.append(el);
    }
    qDebug()<<"now_playing_size:"<<ret.length();
    return ret;
}

ModuleGeneric *Schedule::get_module(int event_id)
{
    ModuleGeneric *ret = NULL;
    for(int i=0;i<timeline.length();i++){
        if(timeline[i]->rowid == event_id)
            ret = timeline[i]->getModule();
    }
    return ret;
}

void Schedule::on_keydown(SDL_Event &e)
{
    for(int i=0;i<timeline.length();i++){
        if(timeline[i]->getModule() != 0x0){
            timeline[i]->getModule()->on_keydown(e);
        }
    }
}

void Schedule::on_keyup(SDL_Event &e)
{
    for(int i=0;i<timeline.length();i++){
        if(timeline[i]->getModule() != 0x0){
            timeline[i]->getModule()->on_keyup(e);
        }
    }
}
