#include "remove_event_from_timeline.h"

using namespace api;

remove_event_from_timeline::remove_event_from_timeline()
{
    API::register_command("remove_event_from_timeline",this);
}


void remove_event_from_timeline::run(QByteArray &buffer, QByteArray&, QJsonObject &param)
{
    if(!check_params(buffer, param, "event_id")){
        return;
    }
    LocalDatabase db;
    int event_id = param.value("event_id").toInt();
    if(!db.remove_event_from_timeline(event_id))
        return error(buffer,"Error: "+db.get_last_error());

    return ok(buffer);
}
