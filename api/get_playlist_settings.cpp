#include "get_playlist_settings.h"

#include "glob.h"
#include "localdatabase.h"

using namespace api;


get_playlist_settings::get_playlist_settings()
{
    API::register_command(this->get_id(),this);
}

void get_playlist_settings::run(QByteArray &buffer, QByteArray&, QJsonObject &param)
{
    if(!check_params(buffer, param, "name"))
        return;

    LocalDatabase db;

    int pl_id = db.get_playlist_id_by_name(param.value("name").toString());

    if(pl_id<0)
        return error(buffer,"playlist '"+param.value("name").toString()+"' not found");

    QJsonObject obj = {
        {"result",db.get_playlist_settings(pl_id)},
        {"error",""}
    };
    return send(buffer,obj);
}
