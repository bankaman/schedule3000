#ifndef UPLOAD_FILE_H
#define UPLOAD_FILE_H

#include <api.h>
#include <localdatabase.h>

namespace api {
class upload_file : public API_command
{
public:
    struct UPFile {
        int id;
        QString name;
        long unsigned int byte_size;
        int chunk_size;
        int playlist_id;
        int playlist_type;
        QFile *file;
    };

    upload_file();
    ~upload_file();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);

    //upload files
    static QVector<UPFile*> upload;

private:
    bool string_in(QString &str, QString variants);
} ;

}
#endif // UPLOAD_FILE_H
