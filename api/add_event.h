#ifndef ADD_EVENT_H
#define ADD_EVENT_H

#include <api.h>
#include <glob.h>
#include <localdatabase.h>

namespace api {

class add_event : public API_command
{
public:
    add_event();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} command_add_event;

}
#endif // ADD_EVENT_H
