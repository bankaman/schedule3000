#ifndef GET_RAM_TIMELINE_H
#define GET_RAM_TIMELINE_H

#include <api.h>
#include <glob.h>
#include <windowctr.h>

namespace api {

class get_ram_timeline : public API_command
{
public:
    get_ram_timeline();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} command_get_ram_timeline;

}

#endif // GET_RAM_TIMELINE_H
