#include "get_ram_timeline.h"

using namespace api;

get_ram_timeline::get_ram_timeline()
{
    API::register_command("get_ram_timeline",this);
}


void get_ram_timeline::run(QByteArray &buffer, QByteArray&, QJsonObject&)
{
    QJsonObject obj;
    QJsonArray windows;
    int count = WindowCtr::get_windows_count();

    for(int i=0;i<count;i++){
        QJsonArray timeline;

        Schedule *s=WindowCtr::get_schedule(i);

        QVector<ScheduleElement*> tl = s->get_timeline();

        for(int k=0;k<tl.length();k++){
            timeline.append(QJsonObject({
                                            {"rowid",tl[k]->rowid},
                                            {"module",tl[k]->getModuleName()},
                                            {"state",tl[k]->getState()},
                                            {"mode",tl[k]->getMode()},
                                            {"start_time",(int)tl[k]->starttime},
                                            {"paused_by",(int)tl[k]->paused_by}
                                        }));
        };
        windows.append({
                           {timeline}
                       });
    }

    obj = {
        {"result",windows},
        {"error",""}
    };

    return send(buffer,obj);
}
