#include "get_playlist_duration.h"

using namespace api;

api::get_playlist_duration::get_playlist_duration()
{
    API::register_command(this->get_id(),this);
}

void api::get_playlist_duration::run(QByteArray &buffer, QByteArray&, QJsonObject &param)
{
    if(!check_params(buffer,param,"name"))
        return;

    QString name = param.value("name").toString();

    LocalDatabase db;

    int playlist_id = db.get_playlist_id_by_name(name);
    if(playlist_id<0)
        return error(buffer,"Cannot find playlist '"+name+"'");

    int duration = db.get_playlist_duration(playlist_id);

    QJsonObject obj = {
        {"result", duration},
        {"error",""}
    };
    return send(buffer, obj);
}
