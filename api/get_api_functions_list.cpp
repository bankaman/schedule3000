#include "get_api_functions_list.h"

using namespace api;
get_api_functions_list::get_api_functions_list()
{
    API::register_command("get_api_functions_list",this);
}

void get_api_functions_list::run(QByteArray &buffer, QByteArray&, QJsonObject&)
{
    QJsonArray ar;
    for(int i=0;i<API::commands->length();i++)
        ar.append(API::commands->at(i)->command_id);

    QJsonObject obj = {
        {"result", ar},
        {"error",""}
    };
    return send(buffer, obj);
}
