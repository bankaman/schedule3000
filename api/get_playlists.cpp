#include "get_playlists.h"

using namespace api;

get_playlists::get_playlists()
{
    API::register_command("get_playlists",this);
}

void get_playlists::run(QByteArray &buffer, QByteArray&, QJsonObject &)
{
    QJsonObject obj;
    LocalDatabase db;
    QJsonArray temp_arr = QJsonArray::fromStringList(db.get_playlists());
    obj = {
        {"result",temp_arr},
        {"error",""}
    };

    return send(buffer,obj);
}
