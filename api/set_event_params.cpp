#include "set_event_params.h"

using namespace api;

set_event_params::set_event_params()
{
    API::register_command(this->get_id(),this);
}

void set_event_params::run(QByteArray &buffer, QByteArray &query, QJsonObject &param)
{
    if(!check_params(buffer, param, "event_id,params"))
        return;

     QJsonDocument doc;
     doc.setObject(param.value("params").toObject());

     LocalDatabase db;
     if(!db.set_event_params(
                 param.value("event_id").toInt(),
                 QTextCodec::codecForName("UTF-8")->toUnicode(doc.toJson())
                 )
        ){
         return error(buffer,"Error: "+db.get_last_error());
     }

     return ok(buffer);
}
