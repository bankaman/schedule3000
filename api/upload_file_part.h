#ifndef UPLOAD_FILE_PART_H
#define UPLOAD_FILE_PART_H

#include <api.h>
#include "upload_file.h"

namespace api {
class upload_file_part : public API_command
{
public:
    upload_file_part();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
    upload_file command_upload_file;
} command_upload_file_part;
}
#endif // UPLOAD_FILE_PART_H
