#ifndef GET_PLAYLIST_SETTINGS_H
#define GET_PLAYLIST_SETTINGS_H

#include <api.h>

namespace api {

class get_playlist_settings : public API_command
{
public:
    get_playlist_settings();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);

protected:
    const char *get_id(){return "get_playlist_settings";}
} command_get_playlist_settings;

}
#endif // GET_PLAYLIST_SETTINGS_H
