#include "add_event.h"
using namespace api;
add_event::add_event()
{
    API::register_command("add_event",this);
}

void add_event::run(QByteArray &buffer, QByteArray&, QJsonObject &param)
{
    if(!check_params(buffer, param, "window_id,module,mode,date_start,priority,draw_order,playlist_name,params"))
        return;
    QJsonDocument doc;

    qDebug()<<param;

    int priority,draw_order;
    if(param.value("priority").isString()){
        priority = param.value("priority").toString().toInt();
    }else{
        priority = param.value("priority").toInt();
    }

    if(param.value("draw_order").isString()){
        draw_order = param.value("draw_order").toString().toInt();
    }else{
        draw_order = param.value("draw_order").toInt();
    }

    //getting playlist type
    QString playlist_name = param.value("playlist_name").toString();
    LocalDatabase db;
    int playlist_id = db.get_playlist_id_by_name(param.value("playlist_name").toString());
    int playlist_type = db.get_playlist_type(playlist_id);

    //checking playlist type
    QString module_name = param.value("module").toString();
    if(module_name=="imageviewer" && playlist_type!=G::PLAYLIST_TYPE_IMAGE)
        return error(buffer, "Cannot use this playlist for imageviewer, playlist type is "+QString::number(playlist_type));

    if(module_name=="musicplayer" && playlist_type!=G::PLAYLIST_TYPE_MUSIC)
        return error(buffer, "Cannot use this playlist for musicplayer, playlist type is "+QString::number(playlist_type));

    if(module_name=="videoplayer" && playlist_type!=G::PLAYLIST_TYPE_VIDEO)
        return error(buffer, "Cannot use this playlist for videoplayer, playlist type is "+QString::number(playlist_type));

    doc.setObject(param.value("params").toObject());
    if(!db.add_event(
                    param.value("window_id").toVariant().toInt(),
                    module_name,
                    param.value("mode").toString(),
                    param.value("date_start").toInt(),
                    priority,
                    draw_order,
                    QTextCodec::codecForName("UTF-8")->toUnicode(doc.toJson()),
                    playlist_name
                )){
        return error(buffer,"Error: "+db.get_last_error());
    }

    return ok(buffer);
}
