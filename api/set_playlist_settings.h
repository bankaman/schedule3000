#ifndef SET_PLAYLIST_IS_RANDOM_H
#define SET_PLAYLIST_IS_RANDOM_H

#include <api.h>

namespace api {


class set_playlist_settings : public API_command
{
public:
    set_playlist_settings();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);

protected:
    const char *get_id(){return "set_playlist_settings";}
} command_set_playlist_settings;


}

#endif // SET_PLAYLIST_IS_RANDOM_H
