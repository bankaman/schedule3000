#include "get_playlist_files.h"

using namespace api;

get_playlist_files::get_playlist_files()
{
    API::register_command("get_playlist_files",this);
}

void get_playlist_files::run(QByteArray &buffer, QByteArray&, QJsonObject &param)
{
    if(!check_params(buffer, param, "name"))
        return;

    LocalDatabase db;

    int pl_id = db.get_playlist_id_by_name(param.value("name").toString());

    if(pl_id<0)
        return error(buffer,"playlist '"+param.value("name").toString()+"' not found");

    QJsonObject obj = {
        {"result",db.get_playlist_files(pl_id)},
        {"error",""}
    };

    return send(buffer,obj);
}
