#ifndef NEW_PLAYLIST_H
#define NEW_PLAYLIST_H

#include <api.h>
#include <glob.h>
#include <localdatabase.h>

namespace api{

class new_playlist : public API_command
{
public:
    new_playlist();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject& param);
    const char* get_id(){return "new_playlist";}
} command_new_playlist;

}
#endif // NEW_PLAYLIST_H
