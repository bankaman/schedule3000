#ifndef DELETE_FILE_FROM_DISK_H
#define DELETE_FILE_FROM_DISK_H

#include <api.h>
#include <glob.h>
#include <localdatabase.h>


namespace api {
class delete_file_from_disk : public API_command
{
public:
    delete_file_from_disk();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} command_delete_file_from_disk;
}
#endif // DELETE_FILE_FROM_DISK_H
