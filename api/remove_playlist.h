#ifndef REMOVE_PLAYLIST_H
#define REMOVE_PLAYLIST_H

#include <api.h>
#include <glob.h>
#include <localdatabase.h>

namespace api{

class remove_playlist : public API_command
{
public:
    remove_playlist();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);

protected:
    const char *get_id(){return "remove_playlist";}
} command_remove_playlist;

}

#endif // REMOVE_PLAYLIST_H
