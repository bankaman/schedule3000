#include "remove_playlist.h"

using namespace api;

api::remove_playlist::remove_playlist()
{
    API::register_command(this->get_id(), this);
}

void api::remove_playlist::run(QByteArray &buffer, QByteArray&, QJsonObject &param)
{
    if(!check_params(buffer,param,"name"))
        return;

    QString name = param.value("name").toVariant().toString();

    LocalDatabase db;

    int playlist_id = db.get_playlist_id_by_name(name);
    if(playlist_id<0)
        return error(buffer,"Cannot find playlist'"+name+"'");

    if(!db.delete_playlist(playlist_id))
        return error(buffer,"Error removing playlist:"+db.get_last_error());

    return ok(buffer);
}

