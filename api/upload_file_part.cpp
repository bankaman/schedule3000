#include "upload_file_part.h"
#include "glob.h"
#include "windowctr.h"

using namespace api;

upload_file_part::upload_file_part()
{
    API::register_command("upload_file_part",this);
}

void upload_file_part::run(QByteArray &buffer, QByteArray &query, QJsonObject &param)
{
    if(!check_params(buffer, param, "file_id,data_length,chunk_num"))
        return;
    upload_file::UPFile *f;
    int id = param.value("file_id").toInt();
    int f_id=-1;
    for(int i=0;i<upload_file::upload.length();i++){
        f = upload_file::upload[i];
        if(f->id==id){
            f_id=i;
            break;
        }
    }
    if(f_id<0){
        return error(buffer, "This file_id is not found in this session!");
    }
    int chunk_num = param.value("chunk_num").toInt();
    //qDebug()<<"chunck: "<<chunk_num;
    f->file->seek(chunk_num*f->chunk_size);
    if(f->file->write(query.mid(query.indexOf((char)0)+1))<0){
        return error(buffer, "error write chunk "+QString::number(chunk_num));
    }
    //qDebug()<<"name:"<<f->name<<" size:"<<f->byte_size;
    LocalDatabase db;
    if((long unsigned int)chunk_num*f->chunk_size>=f->byte_size){
        db.set_file_as_uploaded(f->id);
        db.append_file_to_paylist(f->id,f->playlist_id);
        f->file->close();
        delete f->file;
        WindowCtr::update_playlist(f->playlist_id);
        //determne length of playlist
        int duration=-1;
        switch(f->playlist_type){
            case G::PLAYLIST_TYPE_VIDEO:{
                VlcVideoPlayer player;
                player.setFileList(db.get_playlist_filepaths(f->playlist_id));
                qDebug()<<"duration:"<<player.getDuration();
                duration = player.getDuration();
                }break;
            case G::PLAYLIST_TYPE_MUSIC:{
                VlcMusicPlayer player;
                player.setFileList(db.get_playlist_filepaths(f->playlist_id));
                qDebug()<<"duration:"<<player.getDuration();
                duration = player.getDuration();
            }break;
        }

        db.update_playlist_duration(f->playlist_id,duration);

        delete f;
        upload_file::upload.remove(f_id);
    }
    QJsonObject obj = {
        {"result",chunk_num},
        {"error",""}
    };
    return send(buffer,obj);
}
