#include "delete_file_from_disk.h"

using namespace api;

delete_file_from_disk::delete_file_from_disk()
{
    API::register_command("delete_file_from_disk",this);
}

void delete_file_from_disk::run(QByteArray &buffer, QByteArray&, QJsonObject &param)
{
    if(!check_params(buffer, param, "file_id")){
        return;
    }

    LocalDatabase db;

    if(!db.delete_file_from_disk(param.value("file_id").toInt()))
        return error(buffer,"Error: "+db.get_last_error());

    return ok(buffer);
}
