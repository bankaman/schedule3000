#include "get_windows_count.h"
#include "windowctr.h"

using namespace api;

api::get_windows_count::get_windows_count()
{
    API::register_command(this->get_id(),this);
}

void api::get_windows_count::run(QByteArray &buffer, QByteArray &query, QJsonObject &param)
{
    QJsonObject obj = {
        {"result", WindowCtr::get_windows_count()},
        {"error",""}
    };
    return send(buffer, obj);
}
