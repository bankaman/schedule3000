#include "set_playlist_settings.h"

#include "glob.h"
#include "localdatabase.h"

using namespace api;

set_playlist_settings::set_playlist_settings()
{
    API::register_command(this->get_id(),this);
}

void set_playlist_settings::run(QByteArray &buffer, QByteArray &query, QJsonObject &param)
{
    if(!check_params(buffer, param, "name,is_random"))
        return;

    LocalDatabase db;

    int pl_id = db.get_playlist_id_by_name(param.value("name").toString());
    qDebug()<<"playlist:"<<pl_id;

    if(pl_id<0)
        return error(buffer,"playlist '"+param.value("name").toString()+"' not found");

    bool is_random;
    is_random = param.value("is_random").toInt() == 1;
    if(!db.set_playlist_settings(pl_id,is_random))
        return error(buffer,"error set params");

    return ok(buffer);
}
