#ifndef GET_PLAYLIST_FILES_H
#define GET_PLAYLIST_FILES_H

#include <api.h>
#include <glob.h>
#include <localdatabase.h>

namespace api {

class get_playlist_files : public API_command
{
public:
    get_playlist_files();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} commnd_get_playlist_files;

}

#endif // GET_PLAYLIST_FILES_H
