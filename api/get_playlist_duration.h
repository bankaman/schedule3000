#ifndef GET_PLAYLIST_DURATION_H
#define GET_PLAYLIST_DURATION_H

#include <api.h>
#include <glob.h>
#include <localdatabase.h>

namespace api {

class get_playlist_duration : public API_command
{
public:
    get_playlist_duration();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);

protected:
    const char *get_id(){return "get_playlist_duration";}
} command_get_playlist_duration;

}
#endif // GET_PLAYLIST_DURATION_H
