#ifndef GET_API_FUNCTIONS_LIST_H
#define GET_API_FUNCTIONS_LIST_H

#include <api.h>

namespace api {
class get_api_functions_list : public API_command
{
public:
    get_api_functions_list();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_get_api_functions_list;

}

#endif // GET_API_FUNCTIONS_LIST_H
