#include "add_event_from_template.h"

using namespace api;

add_event_from_template::add_event_from_template()
{
    API::register_command("add_event_from_template",this);
}

void add_event_from_template::run(QByteArray &buffer, QByteArray&, QJsonObject &param)
{
    if(!check_params(buffer, param, "name,time_begin"))
        return;
    LocalDatabase db;
    if(!db.add_from_template(param.value("name").toString(), param.value("time_begin").toInt()))
        return error(buffer,"Error: "+db.get_last_error());

    return ok(buffer);
}
