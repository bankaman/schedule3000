#ifndef REMOVE_FROM_TIMELINE_H
#define REMOVE_FROM_TIMELINE_H

#include <api.h>
#include <glob.h>
#include <localdatabase.h>
#include <windowctr.h>

namespace api {

class remove_event_from_timeline : public API_command
{
public:
    remove_event_from_timeline();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} command_remove_event_from_timeline;

}
#endif // REMOVE_FROM_TIMELINE_H
