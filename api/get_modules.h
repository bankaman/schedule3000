#ifndef GET_MODULES_H
#define GET_MODULES_H

#include <api.h>
namespace api {

class get_modules : public API_command
{
public:
    get_modules();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_get_modules;

}

#endif // GET_MODULES_H
