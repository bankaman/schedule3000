#ifndef GET_TIMELINE_H
#define GET_TIMELINE_H

#include <api.h>
#include <glob.h>
#include <localdatabase.h>

namespace api{

class get_timeline : public API_command
{
public:
    get_timeline();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} command_get_timeline;

}
#endif // GET_TIMELINE_H
