#include "new_playlist.h"

using namespace api;

new_playlist::new_playlist()
{
    API::register_command(this->get_id(), this);
}

void new_playlist::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
    if(!check_params(buffer, param, "name,type"))
        return;

    int type;
    if(param.value("type").isString()){
        type = param.value("type").toString().toInt();
    }else{
        type=param.value("type").toInt();
    }

    LocalDatabase db;
    if(!db.new_playlist(param.value("name").toString(),type))
        return error(buffer,"Error creating playlist:"+db.get_last_error());

    return ok(buffer);
}


