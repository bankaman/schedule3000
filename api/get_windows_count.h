#ifndef GET_WINDOWS_COUNT_H
#define GET_WINDOWS_COUNT_H

#include <api.h>

namespace api {

class get_windows_count : public API_command
{
public:
    get_windows_count();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
protected:
    const char *get_id(){return "get_windows_count";}
} command_get_windows_count;

}
#endif // GET_WINDOWS_COUNT_H
