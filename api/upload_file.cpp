#include "upload_file.h"
#include "windowctr.h"

using namespace api;
//struct upload_file::UPFile;
QVector<upload_file::UPFile*> upload_file::upload;

upload_file::upload_file()
{
    API::register_command("upload_file", this);
}

void api::upload_file::run(QByteArray &buffer, QByteArray&, QJsonObject &param)
{
    if(!check_params(buffer, param, "playlist_name,file_name,type,byte_length,chunk_size"))
        return;

    //getting file length
    long unsigned int length=0;
    if(param.value("byte_length").isString()){
        QString slength = param.value("byte_length").toString();
        qDebug()<<"file length:"<<slength;
        length = slength.toLong();
    }else{
        length = param.value("byte_length").toInt();
    }

    LocalDatabase db;
    //getting playlist type
    int playlist_id = db.get_playlist_id_by_name(param.value("playlist_name").toString());
    int playlist_type = db.get_playlist_type(playlist_id);

    //checking file extantion
    QString filename = param.value("file_name").toString();
    QString ext = filename.mid(filename.lastIndexOf(".")+1);
    switch (playlist_type) {
    case G::PLAYLIST_TYPE_IMAGE:
        if(!string_in(ext,"jpg,png,bmp,tiff,jpeg")){
            return error(buffer,"Unknown file extantion '"+ext+"' for image playlist.");
        }
    break;
    case G::PLAYLIST_TYPE_MUSIC:
        if(!string_in(ext,"mp3,wav,flac,ogg,aac,m4a")){
            return error(buffer,"Unknown file extantion '"+ext+"' for music playlist.");
        }
    break;
    case G::PLAYLIST_TYPE_VIDEO:
        if(!string_in(ext,"mp4,avi,mkv,webm,mov")){
            return error(buffer,"Unknown file extantion '"+ext+"' for video playlist.");
        }
    break;
    default:
        qWarning()<<"unknown playlist type "<<playlist_type<<" on playlist_id="<<playlist_id;
        break;
    }


    int id=db.create_file(filename, param.value("type").toInt(), length);
    if(id<0){
        return error(buffer,"Error: "+db.get_last_error());
    }
    UPFile *f;
    f=new UPFile();
    f->byte_size=length;
    f->id=id;
    f->chunk_size = param.value("chunk_size").toInt();
    f->name = filename;
    f->file = new QFile(WindowCtr::file_storage_folder+QString::number(id)+"."+ext);
    f->file->open(QFile::WriteOnly);
    f->playlist_id = playlist_id;
    f->playlist_type = playlist_type;

    f->file->resize(f->byte_size);

    upload.append(f);
    QJsonObject obj = {
        {"result",id},
        {"error",""}
    };
    return send(buffer,obj);
}

api::upload_file::~upload_file()
{
    for(int i=0;i<upload.length();i++){
        upload[i]->file->close();
        delete upload[i]->file;
        delete upload[i];
    }
}

bool api::upload_file::string_in(QString &str, QString variants)
{
    QStringList list = variants.split(",");
    for(int i=0;i<list.length();i++){
        if(list.at(i).toUpper() == str.toUpper()){
            return true;
        }
    }
    return false;
}
