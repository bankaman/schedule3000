#include "get_modules.h"

#include <QDir>
#include "windowctr.h"

api::get_modules::get_modules()
{
    API::register_command("get_modules",this);
}

void api::get_modules::run(QByteArray &buffer, QByteArray&, QJsonObject&)
{
    QJsonArray ar;

    //standart c modules
    ar.append(QString("imageviewer"));
    ar.append(QString("clockoverlay"));
    ar.append(QString("musicplayer"));
    ar.append(QString("videoplayer"));
    ar.append(QString("dublicator"));

    //js modules
    QDir dir;
    QStringList list;
    QStringList filters;
    filters << "*.js";
    dir.setPath(WindowCtr::js_modules_folder);
    dir.setNameFilters(filters);
    list = dir.entryList();

    for(int i=0; i<list.length();i++)
        ar.append(list.at(i));

    QJsonObject obj = {
        {"result", ar},
        {"error",""}
    };
    return send(buffer, obj);
}
