#ifndef ADD_EVENT_FROM_TEMPLATE_H
#define ADD_EVENT_FROM_TEMPLATE_H

#include <api.h>
#include <glob.h>
#include <localdatabase.h>

namespace api{

class add_event_from_template : public API_command
{
public:
    add_event_from_template();

    // API_command interface
    public:
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} command_add_event_from_template;
}
#endif // ADD_EVENT_FROM_TEMPLATE_H
