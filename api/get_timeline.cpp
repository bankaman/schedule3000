#include "get_timeline.h"


using namespace api;

get_timeline::get_timeline()
{
    API::register_command("get_timeline",this);
}


void get_timeline::run(QByteArray &buffer, QByteArray&, QJsonObject&)
{
    QJsonObject obj;
    LocalDatabase db;
    QJsonArray temp_arr = db.get_all_timeline();

    obj = {
        {"result",temp_arr},
        {"error",""}
    };

    return send(buffer,obj);
}
