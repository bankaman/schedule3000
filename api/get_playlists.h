#ifndef GET_PLAYLISTS_H
#define GET_PLAYLISTS_H

#include <api.h>
#include <glob.h>
#include <localdatabase.h>

namespace api{

class get_playlists : public API_command
{
public:
    get_playlists();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} command_get_playlists;

}
#endif // GET_PLAYLISTS_H
