#ifndef SET_EVENT_PARAMS_H
#define SET_EVENT_PARAMS_H

#include <api.h>
#include <glob.h>
#include <localdatabase.h>

namespace api {

class set_event_params : public API_command
{
public:
    set_event_params();    
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);

protected:
    const char *get_id(){return "set_event_params";}


} command_set_event_params;


}
#endif // SET_EVENT_PARAMS_H
