#include "font.h"
#include <iostream>

#include <QFile>
#include <QDebug>

Font::Font()
{
    sdl_texture = nullptr;
    size = 20;
    textColor = { 0, 0, 0 };
    loaded=false;
    font=nullptr;
    h=-1;
    w=-1;
}

Font::~Font()
{
    destroy_font();
}

bool Font::load(SDL_Renderer *r, QString filename)
{
    destroy_font();
    render = r;
    QFile file;
    font_filename=filename;
    file.setFileName(filename);
    if(!file.exists()){
        qDebug()<<"file not found"<<filename;
        return false;
    }
    font = TTF_OpenFont( filename.toStdString().c_str() , size );

    TTF_SizeUTF8(font, "T", &w, &h);

    if(font==nullptr){
        qDebug()<<"error loading font, "<<TTF_GetError();
        return false;
    }
    loaded=true;
    return true;
}

void Font::drawText(std::string &str, int x, int y)
{
    if(!loaded)
        return;
    if(str.length()==0)
        return;
    if(sdl_texture!=nullptr)
        SDL_DestroyTexture( sdl_texture );

    SDL_Surface* textSurface = nullptr;

    std::string t=str;

    textSurface = TTF_RenderUTF8_Blended( font, t.c_str(), textColor );

    if(textSurface==nullptr){
        qDebug()<<"unable to create surface for font";
        return ;
    }
    sdl_texture = SDL_CreateTextureFromSurface( render, textSurface);
    if(sdl_texture==nullptr){
        qDebug()<<"unable to create texture for font";
        return;
    }

    position.x = x;
    position.y = y;
    int w,h;
    SDL_QueryTexture(sdl_texture,nullptr,nullptr,&w,&h);
    position.w = w;
    position.h = h;
    clip_rect.x=0;
    clip_rect.y=0;
    clip_rect.w=position.w;
    clip_rect.h=position.h;
    center.x = position.w/2;
    center.y = position.h/2;

    SDL_RenderCopyEx( render, sdl_texture,&clip_rect,&position,0,&center,SDL_FLIP_NONE);

    SDL_FreeSurface( textSurface );
    textSurface=nullptr;

}

void Font::drawText(QString &str, int x, int y)
{
    std::string str_ = str.toStdString();
    drawText(str_,x,y);
}

int Font::get_height()
{
    return h;
}

int Font::get_width()
{
    return w;
}

int Font::get_text_width(std::string &str)
{
    if(!loaded)
        return -1;
    int mw=0;

    TTF_SizeUTF8(font, str.c_str(), &mw, &h);

    return  mw;
}

int Font::get_text_width(QString &str)
{
    std::string str_ = str.toStdString();
    return get_text_width(str_);
}

void Font::set_color(SDL_Color color)
{
    old_textColor = textColor;
    textColor = color;
}

void Font::set_size(int _size)
{
    size=_size;
    load(render,font_filename);
}

void Font::return_color()
{
    textColor = old_textColor;
}

void Font::destroy_font()
{
    if(font==nullptr)
        return;
    //TTF_CloseFont(font);
    font=nullptr;
}
