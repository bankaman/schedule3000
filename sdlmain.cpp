
/*
 * libsdl2 wrapper class
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include "sdlmain.h"

SDLMain::SDLMain()
{
    window = nullptr;
    render = nullptr;
    text_input_mode = false;
    text_input_mode_user = nullptr;
}

SDLMain::~SDLMain()
{
    //delete settings;
    SDL_DestroyRenderer(render);
    SDL_DestroyWindow(window);
}

void SDLMain::create_window(int x, int y, int width, int height, bool borderless, bool fullscreen)
{
    uint win_settings = SDL_WINDOW_SHOWN|SDL_WINDOW_RESIZABLE;
    if(borderless)
        win_settings = win_settings | SDL_WINDOW_BORDERLESS;
    WIDTH = width;
    HEIGHT = height;
    window = SDL_CreateWindow("Schedule3000 Pro", x, y, WIDTH, HEIGHT,win_settings);
    if(window==nullptr){
        qFatal("Error creating SDL Window: %s",SDL_GetError());
    }
    sdl_window_id = SDL_GetWindowID(window);
    is_fullscreen = fullscreen;
    if(fullscreen)
        SDL_SetWindowFullscreen( window, SDL_TRUE );

    QImage img;
    img.load(":/resources/icon.png");
    SDL_Surface* surf;

    surf = SDL_CreateRGBSurfaceWithFormatFrom(img.bits(),img.width(),img.height(),24,img.width()*4,SDL_PIXELFORMAT_BGRA32);

    SDL_ShowCursor(SDL_DISABLE);
    SDL_SetWindowIcon(window,surf);
    SDL_FreeSurface(surf);

}

void SDLMain::create_renderer()
{
    if(window==nullptr)
        qFatal("No window was created!");

    render = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC|SDL_RENDERER_TARGETTEXTURE);
    if(render == nullptr){
        qFatal("Error creating SDL render:  %s",SDL_GetError());
    }
    SDL_SetRenderDrawColor( render, 0x0, 0x0, 0x0, 0xFF );
}


//------------------------------------ Рендер окна ----------------------------------
void SDLMain::pre_render(){
    if(render==nullptr)
        return;
    //Clear screen
    SDL_RenderClear( render );
    rendering(render);
    //Update screen
    SDL_RenderPresent( render );
}

void SDLMain::input_handler(SDL_Event &e)
{
    if(e.window.windowID != sdl_window_id)
        return;

    //Разворачиваем окно на весь экран
    if(e.type == SDL_KEYDOWN){
        if(e.key.keysym.sym == SDLK_RETURN && (e.key.keysym.mod & KMOD_ALT)>0){
            if(!enter_down){
                enter_down=true;
                if(is_fullscreen)
                    SDL_SetWindowFullscreen( window, SDL_FALSE );
                else{
                    SDL_SetWindowDisplayMode(window,&fullscreen_mode);
                    SDL_SetWindowFullscreen( window, SDL_TRUE );
                }
                is_fullscreen=!is_fullscreen;
            }
        }
        this->on_keydown(e);
    }
    if(e.type == SDL_KEYUP){
        this->on_keyup(e);
        if(e.key.keysym.sym == SDLK_RETURN)
            enter_down=false;
    }
    //События изменения окна
    if( e.type == SDL_WINDOWEVENT ){
        switch(e.window.event){
            case SDL_WINDOWEVENT_SIZE_CHANGED:
                WIDTH = e.window.data1;
                HEIGHT = e.window.data2;
            break;
        }
    }

    if(e.type==SDL_TEXTINPUT){
        if(text_input_mode){
            text_input_mode_user->on_textinput(e);
        }
    }
    if(e.type==SDL_TEXTEDITING){
        if(text_input_mode){
            text_input_mode_user->on_textedit(e);
        }
    }
}

SDL_Renderer *SDLMain::get_renderer()
{
    return render;
}

void SDLMain::set_window_id(int win_id)
{
    window_id = win_id;
}

int SDLMain::get_width()
{
    return WIDTH;
}

int SDLMain::get_height()
{
    return HEIGHT;
}

SDL_Window *SDLMain::get_window()
{
    return window;
}

bool SDLMain::stop_text_input_mode()
{
    if(!text_input_mode)
        return false;

    text_input_mode=false;
    text_input_mode_user=nullptr;
    SDL_StopTextInput();
}

bool SDLMain::start_text_input_mode(ModuleGeneric *module)
{
    if(text_input_mode){
        qDebug()<<"Faill to request text input mode for module "<<module->module_name<<". Mode already used by "<<text_input_mode_user->module_name;
        return false;
    }
    text_input_mode=true;
    text_input_mode_user = module;
    SDL_StartTextInput();
}




