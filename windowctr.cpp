
/*
 * window controller class
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include "windowctr.h"

bool WindowCtr::do_main_loop=false;
QVector<Schedule*> WindowCtr::windows;
WebServer WindowCtr::webserver;
QString WindowCtr::file_storage_folder="./files/";
QString WindowCtr::js_modules_folder="./js_modules/";

void WindowCtr::init()
{
    check_settings_file();
    //Чтение настроек
    QSettings *s = new QSettings("settings.conf",QSettings::IniFormat);
    //global section
    int win_count = s->value("global/windows_count",1).toInt();
    bool start_webserver = ((s->value("global/start_webserver",1)).toInt()>0);
    int webserver_port = s->value("global/webserver_port",3000).toInt();
    WebServer::password = s->value("global/webserver_password","").toString();
    WebServer::user = s->value("global/webserver_user","tokiko").toString();
    WebServer::enable_https = ((s->value("global/webserver_https_enable", 0).toInt())>0);
    WebServer::https_cert = s->value("global/webserver_https_cert", "server.pem").toString();
    WebServer::https_key = s->value("global/webserver_https_key", "server.key").toString();
    file_storage_folder = s->value("global/file_storage","./files/").toString();
    if(!QDir(file_storage_folder).exists()){
        if(!QDir().mkdir(file_storage_folder))
            qWarning()<<"Error creating file storage folder '"<<file_storage_folder<<"'";
    }
    js_modules_folder = s->value("global/js_modules","./js_modules/").toString();
    if(!QDir(js_modules_folder).exists()){
        if(!QDir().mkdir(js_modules_folder))
            qWarning()<<"Error creating js_modules folder '"<<js_modules_folder<<"'";
    }
    //
    if(start_webserver){
        webserver.set_port(webserver_port);
        webserver.start();
    }
    //G::db = new LocalDatabase();

    Schedule* sch;
    //Создание окон
    int x,y,width,height;
    bool borderless;
    bool fullscreen;
    bool sdl_initialized=false;
    for(int i=0;i<win_count;i++){
        //чтение настроек окна
        x = s->value("window_"+QString::number(i)+"/x",100).toInt();
        y = s->value("window_"+QString::number(i)+"/y",100).toInt();
        width = s->value("window_"+QString::number(i)+"/width",1024).toInt();
        height = s->value("window_"+QString::number(i)+"/height",576).toInt();
        borderless = (s->value("window_"+QString::number(i)+"/borderless",0).toInt()>0);
        fullscreen = (s->value("window_"+QString::number(i)+"/fullscreen",0).toInt()>0);
        //
        sch = new Schedule();
        sch->create_window(x,y,width,height,borderless,fullscreen);
        if(!sdl_initialized){
            //Инициализация SDL
            SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" );
            if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_TIMER) < 0)
                qFatal("SDL could not initialize! SDL Error:  %s",SDL_GetError());
            //Initialize image loading
            int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG | IMG_INIT_TIF;
            if( !( IMG_Init( imgFlags ) & imgFlags ) )
                qFatal("SDL_image could not initialize! SDL_image Error: %s",IMG_GetError());

            if(TTF_Init()==-1)
                    qFatal("Error initializing ttf, ",TTF_GetError());
            sdl_initialized=true;
        }
        qDebug()<<"initializing window #"<<i;
        sch->create_renderer();

        sch->set_window_id(windows.length());
        windows.append(sch);
    }

    delete s;


    //
    //Загрузка линий времени
    for(int i=0;i<windows.length();i++)
        windows.at(i)->load_timeline();
}

int WindowCtr::start()
{
    do_main_loop=true;
    SDL_Event e;
    while(do_main_loop){
        //Events handler
        while( SDL_PollEvent( &e ) != 0 ) {
            if(e.type==SDL_KEYDOWN)
                if(e.key.keysym.sym==27)
                    quit();
            if(e.type == SDL_QUIT) quit();
            for(int i=0;i<windows.length();i++){
                windows[i]->input_handler(e);
            }
        }
        for(int i=0;i<windows.length();i++){
            windows[i]->pre_render();
        }
        SDL_Delay(16);
    }
    release();
    return 0;
}

void WindowCtr::quit()
{
    do_main_loop=false;
}

SDL_Renderer *WindowCtr::get_renderer(int window_id)
{
    return windows[window_id]->get_renderer();
}

int WindowCtr::get_width(int window_id)
{
    return windows[window_id]->get_width();
}

int WindowCtr::get_height(int window_id)
{
    return windows[window_id]->get_height();
}

Schedule *WindowCtr::get_schedule(int window_id)
{
    return windows[window_id];
}

void WindowCtr::update_playlist(int playlist_id)
{
    for(int i=0;i<windows.length();i++)
        windows[i]->update_playlist(playlist_id);
}

ModuleGeneric *WindowCtr::get_module(int window_id, int event_id)
{
    if(window_id>windows.length() || window_id < 0)
        return nullptr;
    return windows[window_id]->get_module(event_id);
}

SDL_Window *WindowCtr::get_window(int window_id)
{
    if(window_id>windows.length())
        return nullptr;
    return windows[window_id]->get_window();
}

int WindowCtr::get_windows_count()
{
    return windows.length();
}

void WindowCtr::remove_event(int event_id)
{
    for(int i=0;i<windows.length();i++){
        windows[i]->remove_event(event_id);
    }
}

void WindowCtr::set_event_params(int event_id, QString params)
{
    for(int i=0;i<windows.length();i++){
        windows[i]->set_event_params(event_id, params);
    }
}

void WindowCtr::remove_events_by_playlist(int playlist_id)
{
    for(int i=0;i<windows.length();i++){
        windows[i]->remove_event_by_playlist(playlist_id);
    }
}


void WindowCtr::release()
{
    //release windows
    for(int i=0;i<windows.length();i++){
        delete windows.at(i);
    }

    webserver.stop();
    //delete G::db;
    API::clear();

    IMG_Quit();
    SDL_Quit();
}

void WindowCtr::check_settings_file()
{
    if(QFile::exists("settings.conf"))
        return;

    QSettings *s = new QSettings("settings.conf",QSettings::IniFormat);
    s->setValue("global/windows_count", 1);
    s->setValue("global/start_webserver", 1);
    s->setValue("global/webserver_port", 3000);
    s->setValue("global/webserver_user", "tokiko");
    s->setValue("global/webserver_password", "");
    s->setValue("global/webserver_https_enable", 0);
    s->setValue("global/webserver_https_cert", "server.pem");
    s->setValue("global/webserver_https_key", "server.key");
    s->setValue("global/file_storage", "./files/");
    s->setValue("global/js_modules", "./js_modules/");



    s->setValue("window_0/x", 100);
    s->setValue("window_0/y", 100);
    s->setValue("window_0/width", 1024);
    s->setValue("window_0/height", 576);
    s->setValue("window_0/borderless", 0);
    s->setValue("window_0/fullscreen", 0);

    s->sync();

    delete s;
}

bool WindowCtr::stop_text_input_mode(int window_id)
{
    if(window_id>windows.length())
        return false;
    windows[window_id]->stop_text_input_mode();
}

bool WindowCtr::start_text_input_mode(int window_id, ModuleGeneric *module)
{
    if(window_id>windows.length())
        return false;

    windows[window_id]->start_text_input_mode(module);
}
