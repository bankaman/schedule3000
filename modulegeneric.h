#ifndef MODULEGENERIC_H
#define MODULEGENERIC_H

/*
 * generic module class
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include <QString>
#include <QHash>
#include <QVariant>
#include <QStringList>
#include <QDebug>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

class ModuleGeneric
{
public:
    ModuleGeneric();
    virtual ~ModuleGeneric();

    virtual void setFileList(QStringList files){Q_UNUSED(files);}
    virtual void setParams(QHash<QString,QVariant>){}
    virtual void setWindow(int _window_id);
    virtual void setMode(QString mode_string);
    virtual void setModuleName(QString module_name);
    virtual void setPlaylistSettings(QVariantHash settings);
    virtual void init(){}
    virtual void process(){}
    virtual void draw(){}
    virtual void nextElement(){}
    virtual void prevElement(){}
    virtual void start(){}
    virtual void stop(){}
    virtual void pause(){}
    virtual void resume(){}
    virtual void fadein(){}
    virtual void fadeout(){}
    virtual void seek(uint64_t){state=STATE_OVER;}
    virtual int getDuration();
    virtual int getMode();
    virtual void setMode(int _mode);
    virtual int getState();

    virtual void on_keydown(SDL_Event &){}
    virtual void on_keyup(SDL_Event &){}
    virtual void on_textinput(SDL_Event &){}
    virtual void on_textedit(SDL_Event &){}


    static const int STATE_IDLE = 0;
    static const int STATE_PLAYING = 1;
    static const int STATE_PAUSED = 2;
    static const int STATE_OVER = 3;
    static const int STATE_FADE = 4;

    static const int MODE_NORMAL = 0;
    static const int MODE_PERMANENT = 1;
    static const int MODE_DAILY = 2;

    int mode, state;

    SDL_Renderer *renderer;
    int window_id;
    QString module_name;

    bool playlist_random;
};

#endif // MODULEGENERIC_H
