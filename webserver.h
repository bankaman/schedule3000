#ifndef WEBSERVER_H
#define WEBSERVER_H

/*
 * webserver module
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include <QThread>
#include <QDebug>
#include <QFile>
#include <QString>

#include <microhttpd.h>

#include <api.h>

#define POSTBUFFERSIZE 512


class WebServer : public QThread
{
    struct iter{
        int iter;
        QByteArray buffer;
    };

public:
    WebServer();
    ~WebServer();


    static MHD_Result answer_to_connection (void *cls, struct MHD_Connection *connection,
                          const char *url, const char *method,
                          const char *version, const char *upload_data,
                            size_t *upload_data_size, void **con_cls);
    static int iterate_post (void *coninfo_cls, enum MHD_ValueKind kind, const char *key,
                  const char *filename, const char *content_type,
                  const char *transfer_encoding, const char *data, uint64_t off,
                  size_t size);
    void run() override;
    void stop();
    void set_port(int port);
    static QString password;
    static QString user;
    static bool enable_https;
    static QString https_cert;
    static QString https_key;


private:
    bool do_run;
    int port;
    struct MHD_Daemon *daemon;
    static QString getFilename(QString url);
};

#endif // WEBSERVER_H
