#ifndef WINDOWCTR_H
#define WINDOWCTR_H

/*
 * window controller class
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <qsettings.h>
#include <qvector.h>
#include <qdebug.h>

#include "glob.h"
#include "schedule.h"
#include "modulegeneric.h"

class Schedule;
class WindowCtr
{
public:
    static void init();
    static int start();
    static void release();
    static void quit();
    static SDL_Renderer* get_renderer(int window_id);
    static int get_width(int window_id);
    static int get_height(int window_id);
    static Schedule *get_schedule(int window_id);
    static void update_playlist(int playlist_id);
    static ModuleGeneric* get_module(int window_id, int event_id);
    static SDL_Window* get_window(int window_id);
    static int get_windows_count();
    static void remove_event(int event_id);
    static void set_event_params(int event_id, QString params);
    static void remove_events_by_playlist(int playlist_id);

    static void check_settings_file();

    static bool do_main_loop;

    static QVector<Schedule*> windows;

    static WebServer webserver;

    static QString file_storage_folder;
    static QString js_modules_folder;

    static bool start_text_input_mode(int window_id, ModuleGeneric* module);
    static bool stop_text_input_mode(int window_id);
};

#endif // WINDOWCTR_H
