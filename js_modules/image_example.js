//Пример работы с API Schedule3000

var swing = null;
var a=90;
var speed=0;
var blink = null;
var blink_transparency = 1;
var blink_speed=-0.005;

//Инициализация, вызывается при создании элемента расписания на линии времени
function init(){
    //Вывод в консоль
    console.log('init');
    
}

//Запуск, выполняется перед началом выполнения модуля
function start(){

    //Загрузка изображения (относительно папки js_modules)
    swing = new Image('img/tokiko_swing.png');   
    blink = new Image('img/light.png');
    
    //Обрезка изображения
    swing.set_clip(
        0,                  //x
        0,                  //y
        300,                //ширина
        swing.get_height()  //высота
    );
}

//Отрисовка - вызывается при отрисовке нового кадра
function draw(){
    //Поворот изображения
    swing.rotate(
        a,                  //угол поворота 
        swing.get_width()/4,  //центр поворота по x 
        0                   //центр поворота по y
    );
    
    
    //Отрисовка изображения
    Screen.draw(
        swing,        //Изображение для рисования
        Screen.get_width()/2-swing.get_width()/4,       // положение x
        Screen.get_height()/2-swing.get_height()/2,     // положение y
        swing.get_width()/2,                            // ширина (необязательно, по умолчению - ширина изображения)
        swing.get_height()                            // высота (необязательно, по умолчению - высота изображения)
    );
    
    //Установка прозрачности изображения значения от 0.0 до 1.0
    blink.set_transparency(blink_transparency);
    
    Screen.draw(blink, Screen.get_width()/2-100, Screen.get_height()/2-300, 200, 200);
    
    
    
    
    
    
    
    //--------------------- вычисления ---------------------
    if(a>0)
        speed-=0.04;
    if(a<0)
        speed+=0.04;
        
    a+=speed;
    
    if(speed<-0.5)
        swing.set_clip(0, 0, 300, swing.get_height());
    if(speed>0.5)
        swing.set_clip(300, 0, 300, swing.get_height());
        
    blink_transparency+=blink_speed;
    if(blink_transparency<0.3)
        blink_speed=0.005;
    if(blink_transparency>1){
        blink_transparency=1;
        blink_speed=-0.005;
    }
}

//Текстовый режим
var text_mode=false;

function text_input(str){
    console.log(str);
}

function keydown(keycode){ //считывание клавиш
    //console.log(keycode);
    if(keycode==9){
        if(!text_mode){
            console.log("switching to text mode");
            Sys.start_text_input_mode();//переход в текстовый режим
            text_mode=true;
        }else{
            console.log("switching to key mode");
            Sys.stop_text_input_mode();//выход из текстового режима
            text_mode=false;
        }
    }
}

function keyup(keycode){ //отжатие клавиши
}



