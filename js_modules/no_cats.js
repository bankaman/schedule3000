/*var urls= [
    "http://thesecatsdonotexist.com/",
    ["https://www.thiswaifudoesnotexist.net/example-",".jpg",0,69998]
    ];
    */
count = 0;
cat = null;
old_cat = null;
placeholder = null;

cat_op = 1;
old_cat_op = 1;

function init(){
    placeholder = new Image('img/404.jpg');
    cat = placeholder;
}

function draw(){
    if(cat==placeholder)
        get_cat();
    
    cat.set_transparency(cat_op);
    if(cat_op<1)
        cat_op+=0.05;
    
    if(old_cat!=null){
        old_cat.set_transparency(old_cat_op);
        if(old_cat_op>0)
            old_cat_op-=0.05;
    }
        
    draw_fullscreen(cat);
    draw_fullscreen(old_cat);
    
    count++;
    if(count>600){
        count=0;
        get_cat();
    }
}

function draw_fullscreen(img){
    if(img==null)
        return;
    var pw = img.get_width();
    var ph = img.get_height();
    var sw = Screen.get_width();
    var sh = Screen.get_height();
    var npw = pw*sh/ph;
    var nph = npw*ph/pw;
    
    if(npw>sw){
        nph=sw*ph/pw;
        npw=nph*pw/ph;
    }
    
    Screen.draw(img,sw/2-npw/2,sh/2-nph/2,npw,nph);
    
}

function get_cat(){
    var t =(Math.random()*3)+1;
    //console.log(t);
    t = Math.floor(t);
    //console.log(t);
    var url="";
    switch(t){
        case 1:{
            var waifu_num = Math.floor((Math.random() * 69998));    
            url = "https://www.thiswaifudoesnotexist.net/example-"+waifu_num+".jpg";
            
        }break;
        case 2:{
            var folderNumber = Math.floor((Math.random() * 3) + 1);
            var path;
            switch (folderNumber) {
                case 1:
                    var catNumber = Math.floor((Math.random() * 8000) + 1);
                    path = "0" + folderNumber + "/cat" + catNumber + ".jpg";
                    break;
                case 2:
                    var catNumber = Math.floor((Math.random() * 8000) + 1);
                    path = "0" + folderNumber + "/cat" + catNumber + ".jpg";
                    break;
                case 3:
                    var catNumber = Math.floor((Math.random() * 8000) + 1);
                    path = "0" + folderNumber + "/cat" + catNumber + ".jpg";
                    break;
            }
            url = "https://d2ph5fj80uercy.cloudfront.net/" + path;
            break;
        }
        case 3:{
            url = "https://thiscatdoesnotexist.com/";
        }
        break;
    }
    
    console.log(url);
    //console.log("wget "+'--user-agent="Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0" '+url+' -O '+"img/cat.jpg");
    var out;
    if(t==1){
        out = Sys.system("wget",[
                    url,
                    "-O","img/cat.jpg"
                ]);    
    }else{    
        out = Sys.system("wget",[
                    '--user-agent="Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0"',
                    url,
                    "-O","img/cat.jpg"
                ]);
    }
               
    

    if(old_cat!=null)
        old_cat.free();
    old_cat=cat;
    if(out.exit_status==0){
        cat = new Image("img/cat.jpg");
    }else{
        cat = new Image('img/404.jpg');
        console.log(out.error);
        console.log(out.output);
    }
    old_cat_op=1;
    cat_op=0;
    
}
