var font_big=null;
var font_small=null;

var users=[];

function draw(){
	
	for(var i=0;i<users.length;i++)
		users[i].draw();
}

function init(){
}

function start(){
	font_big=new Font('worktime_counter/font.ttf');
	font_big.set_color(255,255,255,255);
	font_big.set_size(80);
	
	/*font_small=new Font('worktime_counter/font.ttf');
	font_small.set_color(255,255,255,255);
	font_small.set_size(30);*/
	
	users.push(new user("banka",1678, 10,10));
	users.push(new user("nWss ",2780, 10,100));
	users.push(new user("dkuzmin ",2811, 10,190));
}


function user(name,user_id,x,y){
	this.user_id=user_id;
	this.name = name;
	this.data = [];
	this.enter_timestamp=0;
	this.seconds = 0;
	this.week_seconds = 0;
	this.x=x;
	this.y=y;
	
	this.last_update_time=0;
	
	this.draw = function(){
		
		var seconds = this.seconds;
		if(this.data.length>0)
			if(this.data[this.data.length-1].direction==1)
				seconds = (this.seconds+(Math.floor(Date.now() / 1000)-this.enter_timestamp));
			
		var date = new Date();
		if(date.getHours()<18){
			if(seconds<8*60*60){
				font_big.set_color(255,0,0,255); //red
			}else{
				font_big.set_color(0xff,0xe7,0,255); //yellow 
			}
		}else if(date.getHours()>=18){
			if(seconds<8*60*60){
				font_big.set_color(255,0,0,255); //red
			}else if(seconds>=8*60*60 && seconds<9*60*60){
				font_big.set_color(0,255,0,255); //green
			}else{
				font_big.set_color(0x00,0xd3,0xff,255); //blue
			}
		}
			
			
		var time = this.format_time(seconds);
		var week_time = this.format_time(seconds+this.week_seconds);
		
		font_big.draw_text(this.name+": "+time+" | "+week_time,this.x,this.y);

		if(Math.floor(Date.now() / 1000) - this.last_update_time > 60){
			this.update_data();
		}
	}
	
	this.format_time = function (seconds){
	    var hour = Math.floor(seconds / 60 / 60);
		var min = Math.floor(seconds / 60 % 60);
		var sec = Math.floor(seconds % 60);
		var time = (hour<10) ? "0"+hour : hour;
		time += ":";
		time += (min<10) ? "0"+min : min;
		time += ":";
		time += (sec<10) ? "0"+sec : sec;
		
		return time;
	}
	
	this.update_data=function(){
		//console.log('update data');
		this.enter_timestamp=0;
		this.seconds = 0;
		this.week_seconds = 0;
		var d = Sys.system("php",["worktime_counter/get_time_for_day.php",""+this.user_id]);
		
		try {
			this.data = JSON.parse(d.output);
		}catch(e){
			console.log('Error: '+d.output+'| '+d.error);
			this.last_update_time = Math.floor(Date.now() / 1000);
			return;
		}
		console.log(this.name+':'+this.data.length);
		
        var current_date = new Date();
        current_date.setHours(0,0,0,0);
        var tooday_timestamp = Math.floor(current_date.getTime()/1000);
		
		for(var i=0;i<this.data.length;i++){
			var d = this.data[i];
			
			if(!this.enter_timestamp){
				if(d.direction == 0)
					continue;
				this.enter_timestamp = d.time;
				continue;
			}
			
			if(d.direction==0){
			    if(d.time>tooday_timestamp)
    				this.seconds += d.time-this.enter_timestamp;
    			else
    			    this.week_seconds += d.time-this.enter_timestamp;
				this.enter_timestamp = 0;
			}
		}
		this.last_update_time = Math.floor(Date.now() / 1000);
	}
	
	this.update_data();
}


