
var G="magical-mirai-countdown/";
var mirai_date = new Date(2019,8,01,17,00);
var num_size;

var logo;
var numbers;

function init(){
    //Вывод в консоль
    console.log('init');

}

function start(){
    logo = new Image(G+"logo.png");
    numbers = new Image(G+"numbers.png");
    
    num_size = numbers.get_width()/10;
}

function draw(){

    //Screen.draw(logo,Screen.get_width()/2-logo.get_width()/2,80);
    
    var d = new Date();
    var diff = Math.abs(Math.floor((mirai_date.getTime()-d.getTime()) / 1000));
    var days = Math.floor(diff/60/60/24);
    var hours = Math.floor(diff/60/60)%24;
    var minutes = Math.floor(diff/60)%60;
    var seconds = diff%60;
    
    ns = num_size/2;
    
    draw_number(days,Screen.get_width()/2-num_size/2,Screen.get_height()/2-numbers.get_height()/2-50);
    
    draw_number(hours,Screen.get_width()/2-     (3*ns) -50  ,  Screen.get_height()/2);
    draw_number(minutes,Screen.get_width()/2-   (ns)        , Screen.get_height()/2);
    draw_number(seconds,Screen.get_width()/2+   (ns) +50    , Screen.get_height()/2);
}


function draw_number(num,x,y){
    var d1 = Math.floor(num/10)%10;
    var d2 = num%10;
    
    numbers.set_clip(d1*num_size,0,num_size,numbers.get_height());
    Screen.draw(numbers,x,y,num_size/2,numbers.get_height()/2);
    
    numbers.set_clip(d2*num_size,0,num_size,numbers.get_height());
    
    Screen.draw(numbers,x+num_size/2,y,num_size/2,numbers.get_height()/2);
}
