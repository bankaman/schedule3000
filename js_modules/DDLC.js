var f="DDLC/";

var natsuki;
var monika;
var yuri;
var sayori;
var tokiko;

function init(){
    monika = new Character("m");
    sayori = new Character("s");
    natsuki = new Character("n");
    yuri = new Character("y");
    tokiko = new Character("tokiko");
    
    sayori.counter=20;
    natsuki.counter=40;
    yuri.counter=60;
    
    monika.x = 100;
    sayori.x = 200;
    natsuki.x = 300;
    yuri.x = 400;
}

function start(){
    
}

function draw(){
    yuri.draw();
    natsuki.draw();
    monika.draw();
    sayori.draw();
    tokiko.draw();
}


function Character(c){
    this.image = [];
    this.image[0] = new Image(f+c+"_sticker_1.png");
    this.image[1] = new Image(f+c+"_sticker_2.png");
    this.x = 300;
    this.y = 0;
    this.speedx = 0;
    this.speedy = 0;
    this.flip=false;
    this.counter = 0;
    this.cur_img=0;
    
    this.draw = function(){
        var img = this.image[this.cur_img];
        Screen.draw(img,this.x,this.y);        
        
        
        this.y+=this.speedy;
        this.x+=this.speedx;
        
        if(this.speedx>0 && !this.flip)
            this.do_flip(true);
        if(this.speedx<0 && this.flip)
            this.do_flip(false);
        
        if(this.speedy==0){
            this.counter++;
        }
        if(this.counter>100){
            this.counter=0;
            if(Math.random()>0.8){
                this.speedy=-10;
                if(Math.random()>0.5){
                    this.speedx = Math.random()*7-7/2;
                }
            }
        }
        
        if(this.y<Screen.get_height()-Screen.get_height()/20-img.get_height()){
            this.speedy+=1;
            if(this.speedy>20)
                this.speedy=20;
        }
        if(this.y>Screen.get_height()-Screen.get_height()/20-img.get_height()){
            this.speedy=0;
            this.speedx=0;
            this.cur_img=0;
            this.y=Screen.get_height()-Screen.get_height()/20-img.get_height();
        }
        
        if(this.x>Screen.get_width()+50){
            this.teleport();
        }
        if(this.x+this.image[0].get_width()<-50){
            this.teleport();
        }
    }
    
    this.teleport = function(){
        this.x=Math.random()*Screen.get_width();
        this.y=-300;
        this.cur_img=1;
    }
    
    this.do_flip = function(flip){
        this.flip = flip;
        this.image[0].set_flip(this.flip,false);
        this.image[0].set_flip(this.flip,false);
    }
}
