<?php
define('MYSQL_HOST', '192.168.1.219');
define('MYSQL_DB', 'info');
define('MYSQL_LOGIN', 'timelord');
define('MYSQL_PASSWORD', 'minipass');

function std_debug($message,$die=true){
	print_r($message);
	
	if($die)
		die;
}

function fetch_result($mysql, $query){
	$q =$mysql->query($query);
	if(!$q)
		std_debug('query error');
	$res = [];
	while($row = $q->fetch_assoc()){
		$res[]=$row;
	}
	return $res;
}

$user_id = $argv[1];


$mysql = new mysqli(MYSQL_HOST,MYSQL_LOGIN,MYSQL_PASSWORD,MYSQL_DB);
if(!$mysql)
	std_debug('connection error');
	

$curday = new \DateTime();
while($curday->format('w')!=1){
    $curday->sub(new DateInterval("P1D"));
}

$curday=$curday->format('Y-m-d');

$data = fetch_result($mysql, 
	"SELECT time,direction FROM info.i_time_lord
		where u_id = {$user_id}
		and time>'{$curday}'
		order by time");
			

$out = [];
foreach($data as $d){
	$out[] = [
		'time' => date_create_from_format('Y-m-d H:i:s',$d['time'])->getTimestamp(),
		'direction' => $d['direction']
	];
}

echo json_encode($out);
		
/*$seconds = 0;
$enter_timestamp = 0;
foreach($data as $d){
	$date = date_create_from_format('Y-m-d H:i:s',$d['time']);
	$timestamp = $date->getTimestamp();
	$dir = $d['direction'];
		
	if(!$enter_timestamp){
		if($dir == 0)
			continue;
		$enter_timestamp = $timestamp;
		continue;
	}
	
	if($dir==0){
		$seconds += $timestamp-$enter_timestamp;
		$enter_timestamp = 0;
	}
}

$last_dir=$data[count($data)-1]['direction'];

if($last_dir==1)
	$seconds+=(new \DateTime())->getTimestamp() - $enter_timestamp;
	
echo $seconds;*/
