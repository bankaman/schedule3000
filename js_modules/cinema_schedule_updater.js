
var PLAYLIST_VIDEO_ADV = "Видео реклама";
var PLAYLIST_VIDEO_ADV_VOLUME = 100;

var PLAYLIST_AUDIO_ADV = "Аудио реклама";
var PLAYLIST_AUDIO_ADV_VOLUME = 100;

var PLAYLIST_PRM_INVITE = "Приглашение Премьерный";
var PLAYLIST_STAR_INVITE = "Приглашение Звёздный";
var PLAYLIST_GREEN_INVITE = "Приглашение Зелёный";
var PLAYLIST_BLUE_INVITE = "Приглашение Синий";
var PLAYLIST_RED_INVITE = "Приглашение Красный";
var PLAYLIST_GOLD_INVITE = "Приглашение Золотой";
var PLAYLIST_INVITE_VOLUME = 100;

var THREE_MINUTES = 180;
var WINDOW_ID = 0;

var order = 3;

var sessions = null;

var is_initialised = false;

var timeline = {
    tl:[],
    hit_test:function(begin,duration,priority){
        var result = null;

	    var x1=Number(begin);
	    var x2=Number(begin)+Number(duration);

        for(var i=0;i<this.tl.length;i++){
            var el = this.tl[i];
		    var y1=Number(el.date_start);
		    var y2=Number(el.date_start)+Number(el.duration);
		    
		    if((((x1>y1)&&(x1<y2))||((x2>y1)&&(x2<y2))||((y1>x1)&&(y1<x2))||((y2>x1)&&(y2<x1)))&&(priority<=el.priority)){
			    result = el;
			    break;
		    }
	    }
	return result;
    },
    add: function(module,mode,begin,duration,playlist_name, priority,volume){
        do{
            hit=this.hit_test(begin,duration,priority);
            if(hit!==null){
                hit.duration+=duration;
                priority=hit.priority+1;
            }
        }while(hit!==null);
        this.tl.push({
            window_id: WINDOW_ID,
            module: module,
            mode: mode,
            date_start: begin,
            duration: duration,
            playlist_name:playlist_name,
            priority:priority,
            draw_order: priority,
            params: {volume : volume},
            cinema_updater_old:false
        });
    },
    import_tl: function(){
        var timeline = api('get_timeline');
        for(var i=0;i<timeline.length;i++){
            var e = timeline[i];
            e.date_start = e.date_start_sec;
            e.cinema_updater_old = true;
            this.tl.push(e);
        }
    },
    export_tl:function(){
        for(var i=0;i<this.tl.length;i++){
            var el = this.tl[i];
            if(!el.cinema_updater_old)
                api('add_event',el);
        }
    }
}

//update_sessions();

//Инициализация, вызывается при создании элемента расписания на линии времени
function init(){
    //Вывод в консоль
    console.log('init cinema schedule updater');
	is_initialised = false;
}

//Запуск, выполняется перед началом выполнения модуля
function start(){
	console.log('Start!');
}

//Отрисовка - вызывается при отрисовке нового кадра
function draw(){
	//console.log('draw!');
	if(!is_initialised){
		is_initialised = true;
		console.log('start cinema schedule updater');
		console.log("state:",Sys.get_state());
		update_sessions();
		Sys.set_state(3); // OVER
	}
}



function update_sessions(){
    
    console.log('Получение сеансов...');
    sessions = get_sessions().data;
    
    //Проверяем есть ли необходимые плейлисты
    console.log('Проверка плейлистов...');
    var playlists = api('get_playlists');
    var has_video_adv=false;
    var has_audio_adv=false;
    var has_prm_invite=false;
    var has_star_invite=false;
    var has_green_invite=false;
    var has_red_invite=false;
    var has_blue_invite=false;
    var has_gold_invite=false;
    for(var i=0;i<playlists.length;i++){
        if(playlists[i]==PLAYLIST_AUDIO_ADV) has_audio_adv=true;
        if(playlists[i]==PLAYLIST_VIDEO_ADV) has_video_adv=true;
        if(playlists[i]==PLAYLIST_PRM_INVITE) has_prm_invite=true;
        if(playlists[i]==PLAYLIST_STAR_INVITE) has_star_invite=true;
        if(playlists[i]==PLAYLIST_GREEN_INVITE) has_green_invite=true;
        if(playlists[i]==PLAYLIST_RED_INVITE) has_red_invite=true;
        if(playlists[i]==PLAYLIST_BLUE_INVITE) has_blue_invite=true;
        if(playlists[i]==PLAYLIST_GOLD_INVITE) has_gold_invite=true;
    }
    
    if(!has_audio_adv) api('new_playlist',{name:PLAYLIST_AUDIO_ADV,type:1});
    if(!has_video_adv) api('new_playlist',{name:PLAYLIST_VIDEO_ADV,type:2});
    if(!has_prm_invite) api('new_playlist',{name:PLAYLIST_PRM_INVITE,type:1});
    if(!has_star_invite) api('new_playlist',{name:PLAYLIST_STAR_INVITE,type:1});
    if(!has_green_invite) api('new_playlist',{name:PLAYLIST_GREEN_INVITE,type:2});
    if(!has_red_invite) api('new_playlist',{name:PLAYLIST_RED_INVITE,type:2});
    if(!has_blue_invite) api('new_playlist',{name:PLAYLIST_BLUE_INVITE,type:2});
    if(!has_gold_invite) api('new_playlist',{name:PLAYLIST_GOLD_INVITE,type:2});
    
    console.log('Удаление старых событий');
    remove_from_timeline_by_playlist(PLAYLIST_AUDIO_ADV);
    remove_from_timeline_by_playlist(PLAYLIST_PRM_INVITE);
    remove_from_timeline_by_playlist(PLAYLIST_STAR_INVITE);
    remove_from_timeline_by_playlist(PLAYLIST_GREEN_INVITE);
    remove_from_timeline_by_playlist(PLAYLIST_RED_INVITE);
    remove_from_timeline_by_playlist(PLAYLIST_BLUE_INVITE);
    remove_from_timeline_by_playlist(PLAYLIST_GOLD_INVITE);
    remove_from_timeline_by_playlist(PLAYLIST_VIDEO_ADV);
    
    console.log('Импорт линии времени...');
    timeline.import_tl();
    
    console.log('Добавление аудио рекламы...');
    var date = new Date();
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    var audio_adv_duration = get_playlist_duration(PLAYLIST_AUDIO_ADV);
    console.log("audio_adv_duration:",audio_adv_duration);
    for(var i=0;i<24;i++){
        date.setHours(i);
        timeline.add(
            'musicplayer', 
            'daily', 
            Math.floor(date.getTime()/1000), 
            audio_adv_duration,
            PLAYLIST_AUDIO_ADV,
	        10,
			PLAYLIST_AUDIO_ADV_VOLUME //volume
			);
    }
    console.log('Добавление видео рекламы...');
    var video_adv_duration = get_playlist_duration(PLAYLIST_VIDEO_ADV);
    console.log("video_adv_duration:",video_adv_duration);
    for(var i=0;i<sessions.length;i++){
        if(sessions[i].ucs_hall_id!=1)
            continue;
        var date = sessions[i].datetime;
        timeline.add(
            'videoplayer', 
            'normal', 
            date - video_adv_duration - THREE_MINUTES, 
            video_adv_duration,
            PLAYLIST_VIDEO_ADV,
            11,
			PLAYLIST_VIDEO_ADV_VOLUME //volume
		);
    }
    
    console.log('Добавление приглашений...');
    var invite_prm_duration = get_playlist_duration(PLAYLIST_PRM_INVITE);
    var invite_star_duration = get_playlist_duration(PLAYLIST_STAR_INVITE);
    var invite_blue_duration = get_playlist_duration(PLAYLIST_BLUE_INVITE);
    var invite_red_duration = get_playlist_duration(PLAYLIST_RED_INVITE);
    var invite_gold_duration = get_playlist_duration(PLAYLIST_GOLD_INVITE);
    var invite_green_duration = get_playlist_duration(PLAYLIST_GREEN_INVITE);
    for(var i=0;i<sessions.length;i++){
        var date = sessions[i].datetime;
        switch(sessions[i].ucs_hall_id){
            case 1: //Премьерный
                timeline.add(
                    'musicplayer', 
                    'normal', 
                    date - THREE_MINUTES, 
                    invite_prm_duration,
                    PLAYLIST_PRM_INVITE,
                    12,
					PLAYLIST_INVITE_VOLUME //volume
					);
            break;
            case 2: //Звёздный
                timeline.add(
                    'musicplayer', 
                    'normal', 
                    date - THREE_MINUTES, 
                    invite_star_duration,
                    PLAYLIST_STAR_INVITE,
                    12,
					PLAYLIST_INVITE_VOLUME //volume
					);
            break;
            case 34: //Синий
                timeline.add(
                    'videoplayer', 
                    'normal', 
                    date - THREE_MINUTES, 
                    invite_blue_duration,
                    PLAYLIST_BLUE_INVITE,
                    12,
					PLAYLIST_INVITE_VOLUME //volume
					);
            break;
            case 35: //Красный
                timeline.add(
                    'videoplayer', 
                    'normal', 
                    date - THREE_MINUTES, 
                    invite_red_duration,
                    PLAYLIST_RED_INVITE,
                    12,
					PLAYLIST_INVITE_VOLUME //volume
					);
            break;
            case 36: //Золотой
                timeline.add(
                    'videoplayer', 
                    'normal', 
                    date - THREE_MINUTES, 
                    invite_gold_duration,
                    PLAYLIST_GOLD_INVITE,
                    12,
					PLAYLIST_INVITE_VOLUME //volume
					);
            break;
            case 37: //Зелёный
                timeline.add(
                    'videoplayer', 
                    'normal', 
                    date - THREE_MINUTES, 
                    invite_green_duration,
                    PLAYLIST_GREEN_INVITE,
                    12,
					PLAYLIST_INVITE_VOLUME //volume
					);
            break;
        }
    }
    console.log('Экспорт линии времени...');
    timeline.export_tl();
    
}










//-----------------------------------------------------------------------------------

function get_playlist_duration(playlist){
    return Math.round(api('get_playlist_duration',{name:playlist})/1000);
}

function get_sessions(){
    var date = new Date();
    var day = date.getDate();   
    day = day<10 ? "0"+day : ""+day;
    var month = date.getMonth()+1;
    month = month<10 ? "0"+month : ""+month;
  
    var query = JSON.stringify({
            json:true,
            query:'getsessions',
            dates:day+"."+month+"."+date.getFullYear()
        });
    
    var out = Sys.system("curl",["https://192.168.1.120:1920/","--data",'data='+query,"--insecure"]);
    
    if(out.exit_status!=0){
        console.log(out.error);
        return;
    }
    
    return JSON.parse(out.output);
}


function api(command, params){
    if(params===undefined)
        params=[];	
    
    var result = Sys.api({command:command,params:params});
    if(result.error){
        console.log(result.error);
        return false;
    }
    return result.result;
}

function remove_from_timeline_by_playlist(playlist){
    var timeline = api('get_timeline');
    for(var i=0;i<timeline.length;i++){
        if(timeline[i].playlist_name == playlist){
            api('remove_event_from_timeline',{event_id:timeline[i].id});
        }
    }
}

/*function format_date(date){
    var day = date.getDate();   
    day = day<10 ? "0"+day : ""+day;
    var month = date.getMonth()+1;
    month = month<10 ? "0"+month : ""+month;
    
    return date.getFullYear()+'-'+month+'-'+day;
}*/
function keydown(keycode) { //считывание клавиш
	console.log(keycode);
}

function keyup(keycode) { //отжатие клавиши
	console.log(keycode);

}
