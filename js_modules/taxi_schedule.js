var border_image = null;
var border_image_height = 250;
var border_image_width = 335;

var ok_image;

var font_big = null;
var font_small = null;
var font_middle = null;
var font_middle_ok = null;

var data = [];
var last_update_time = 0;

function init() {

}

function start() {

    Sys.start_text_input_mode();//переход в текстовый режим

    border_image = new Image('taxi_schedule/border.jpg');
    border_image.set_transparency(0.7);
    ok_image = new Image('taxi_schedule/ok.png');

    font_big = new Font('worktime_counter/font.ttf');
    font_big.set_color(25, 0, 100, 0);
    font_big.set_size(30);


    font_middle = new Font('worktime_counter/font.ttf');
    font_middle.set_color(0, 0, 0, 0);
    font_middle.set_size(20);

    font_middle_ok = new Font('worktime_counter/font.ttf');
    font_middle_ok.set_color(255, 150, 21, 0);
    font_middle_ok.set_size(20);

    font_small = new Font('worktime_counter/font.ttf');
    font_small.set_color(0, 0, 0, 0);
    font_small.set_size(13);

    update_data();

}

function draw() {
    Screen.set_color(255, 150, 21, 0);
    var taxi_cards = [];
    var margin = 10;

    for (var j = 0; j < data.length; j++) {
        taxi_cards.push(new TaxiCard(j * border_image_width + margin, margin, data[j]));
    }

    var c = 0;
    var r = 0;
    for (var i = 0; i < taxi_cards.length; i++) {
        var tx = c * (border_image_width + margin);
        if (tx > Screen.get_width() - border_image_width + margin) {
            c = 0;
            r++;
        }
        taxi_cards[i].draw(c * (border_image_width + margin), r * (border_image_height + margin));
        c++;
    }
    if (Math.floor(Date.now() / 1000) - this.last_update_time > 10) {
        update_data();
    }
}


function TaxiCard(x, y, data) {
    this.x = x;
    this.y = y;

    this.users = data.group;
    this.name_taxi = data.name_taxi;
    this.taxi_desc = data.taxi_desc;

    this.draw = function (x, y) {
        this.x = x;
        this.y = y;
        Screen.draw(border_image, this.x, this.y, border_image_width, border_image_height);
        font_big.draw_text(this.name_taxi, this.x + 30, this.y + 20);
        if (this.taxi_desc !== null)
            font_small.draw_text(this.taxi_desc, this.x + 30, this.y + 47);

        for (var i = 0; i < this.users.length; i++) {
            if (this.users[i].last_address !== null)
                font_small.draw_text('Улица: ' + this.users[i].last_address, this.x + 30, this.y + 90 + i * 40);


            if (this.users[i].status === 1) {
                Screen.draw(ok_image, this.x + 30 + 250, this.y + 70 + i * 40, 25, 25);
                font_middle_ok.draw_text(this.users[i].u_name, this.x + 30, this.y + 70 + i * 40);
            } else {
                font_middle.draw_text(this.users[i].u_name, this.x + 30, this.y + 70 + i * 40);
            }
        }
    }
}

function update_data() {
    console.log('update data');
    d = Sys.system("php", ["taxi_schedule/get_taxi_schedule.php"]);
    try {
        data = JSON.parse(d.output);
    } catch (e) {
        console.log('Error: ' + d.output);
        return;
    }
    last_update_time = Math.floor(Date.now() / 1000);
    console.log('update data finish')
}


var card_code = "";

function text_input(str) {

    if (str !== '?') {
        card_code += str;
    } else {
        var num_card = card_code.match('[0-9]{9}$');
        d = Sys.system("php", ["taxi_schedule/change_status.php", "" + num_card[0]]);
        console.log(d.output);
        console.log('finish');
        console.log(num_card);
        update_data();

        card_code = "";
    }
}

function keydown(keycode) { //считывание клавиш
}

function keyup(keycode) { //отжатие клавиши

}