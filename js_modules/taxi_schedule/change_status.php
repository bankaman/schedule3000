<?php
define('MYSQL_HOST', '192.168.1.219');
define('MYSQL_DB', 'info');
define('MYSQL_LOGIN', 'timelord');
define('MYSQL_PASSWORD', 'minipass');


//define('MYSQL_HOST', '127.0.0.1');
//define('MYSQL_DB', 'info');
//define('MYSQL_LOGIN', 'root');
//define('MYSQL_PASSWORD', '123456789zombie');
function std_debug($message, $die = true)
{
    print_r($message);

    if ($die)
        die;
}

/**
 * Run mysql query and fetch result
 *
 * @param $mysql mysqli - mysql connect
 * @param $query string - query
 *
 * @return array - result
 */
function fetch_result($mysql, $query)
{
    $q = query($mysql, $query);
    $res = [];
    while ($row = $q->fetch_assoc()) {
        $res[] = $row;
    }
    return $res;
}

/**
 * Run mysql query and fetch result
 *
 * @param $mysql mysqli - mysql connect
 * @param $query string - query
 *
 * @return bool|mysqli_result
 */
function query($mysql, $query)
{
    $q = $mysql->query($query);
    if (!$q)
        std_debug('query error');

    return $q;
}

function get_group_taxi($mysql)
{
    $data = fetch_result($mysql,
        "SELECT * FROM i_taxi where status = 1");


    return $data;
}

$mysql = new mysqli(MYSQL_HOST, MYSQL_LOGIN, MYSQL_PASSWORD, MYSQL_DB);
if (!$mysql)
    std_debug('connection error');
$mysql->set_charset("utf8");


$user_card = $argv[1];

$active_groups = get_group_taxi($mysql);


$edit_group_taxi_key = array();

foreach ($active_groups as $key => &$group) {
    $all_active = true;

    $group['group'] = unserialize($group['group']);

    foreach ($group['group'] as &$user) {
        if ($user_card == $user['mo_card']) {
            array_push($edit_group_taxi_key, $key);
            $user['status'] = 1;
            $user['time_confirm'] = time();
        }

        if ($user['status'] == 0) {
            $all_active = false;
        }
    }

    if ($all_active) {
        $group['status'] = 0;
    }
}

$sql = "";
if ($edit_group_taxi_key) {
    foreach ($active_groups as $key => $active_group) {
        foreach ($edit_group_taxi_key as $edit_group) {
            if ($key == $edit_group) {
                $id = $active_group['id'];
                $status = $active_group['status'];
                $d = serialize($active_group['group']);
                $sql = "update i_taxi set `status` = {$status}, `group` = '{$d}' where id = '{$id}';";
                query($mysql, $sql);
            }
        }
    }
}


//std_debug($sql);
//std_debug($active_groups);