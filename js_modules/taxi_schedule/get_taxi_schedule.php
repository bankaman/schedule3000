<?php
define('MYSQL_HOST', '192.168.1.219');
define('MYSQL_DB', 'info');
define('MYSQL_LOGIN', 'timelord');
define('MYSQL_PASSWORD', 'minipass');

//define('MYSQL_HOST', '127.0.0.1');
//define('MYSQL_DB', 'info');
//define('MYSQL_LOGIN', 'root');
//define('MYSQL_PASSWORD', '123456789zombie');

function std_debug($message, $die = true)
{
    print_r($message);

    if ($die)
        die;
}


/**
 * Run mysql query and fetch result
 *
 * @param $mysql mysqli - mysql connect
 * @param $query string - query
 *
 * @return array - result
 */
function fetch_result($mysql, $query)
{
    $q = $mysql->query($query);
    if (!$q)
        std_debug('query error');
    $res = [];
    while ($row = $q->fetch_assoc()) {
        $res[] = $row;
    }
    return $res;
}

function get_group_taxi($mysql)
{
    $data = fetch_result($mysql,
        "SELECT * FROM i_taxi where status = 1");


    return $data;
}

$mysql = new mysqli(MYSQL_HOST, MYSQL_LOGIN, MYSQL_PASSWORD, MYSQL_DB);
if (!$mysql)
    std_debug('connection error');
$mysql->set_charset("utf8");


$taxi_groups = get_group_taxi($mysql);
foreach ($taxi_groups as &$group){
    $group['group'] = unserialize($group['group']);
}

//std_debug($taxi_groups);

echo json_encode($taxi_groups);