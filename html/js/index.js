function add_event(){

    var date_start = $('#date_begin_select').val();
    var time_begin = $('#time_begin_select').val();

    var d = date_start.split('.');
    var t = time_begin.split(':');
    console.log(d[2],d[1],d[0],t[0],t[1],0,0);
    date_start = new Date(d[2],d[1]-1,d[0],t[0],t[1]);

    var query = {
      command : 'add_event',
      params : {
            window_id : $('#window_id_select').val(),
            module : $('#event_module_select').val(),
            mode : $('#event_type_select').val(),
            date_start : Math.floor(date_start.getTime()/1000),
            priority : $('#priority').val(),
            draw_order : $('#draw_order').val(),
            playlist_name : $('#playlist_select').val(),
            params : {
                dummy:true
            },
        }
    }
    console.log(query);
    send_api_query(query,function(r){
        if(r.error){
           alert(r.error);
           return;
        }
        $('#modal1').modal('close');
        update_timeline_table();
    });
}

function update_timeline_table(){
    var query = {
      command : 'get_timeline',
      params : {}
    }
    send_api_query(query,function(r){
        var result = r.result;

        var table = document.createElement("table");
        $(table).addClass('striped');
        $(table).addClass('centered');
        var tbody = document.createElement("tbody");
        var head = document.createElement("tr");
        var keys = {
            id: "Event ID",
            window_id: "Window #",
            date_start: "Start Date",
            module:"Module",
            playlist_name: "Playlist",
            mode: "Mode",
            duration: "Duration",
            draw_order: "Draw Order",
            priority:"Priority",
            is_played: "Is Played",
            is_skipped: "Is Skipped",
            params: "Params",
            action_btn: ""
        }
        for(var i=0;i<Object.keys(keys).length;i++){
            var th = document.createElement("th");
            $(th).html(keys[Object.keys(keys)[i]]);
            $(head).append(th);
        }
        $(tbody).append(head);
        for(var k=0;k<result.length;k++){
            var row = document.createElement("tr");
            for(var i=0;i<Object.keys(keys).length;i++){
                var td = document.createElement("td");
                switch(Object.keys(keys)[i]){
                    case 'action_btn':
                        $(td).html("<a onclick='remove_from_timeline("+result[k].id+")' class='waves-effect waves-light btn red'>Remove</a>");
                    break;
                    case 'duration':{
                        var data = result[k][Object.keys(keys)[i]];
                        if(data<0){
                            $(td).html('none');
                            break;
                        }

                        var hours = Math.floor(data/1000/60/60);
                        var minutes = Math.floor(data/1000 / 60 )% 60;
                        var seconds = Math.floor(data/1000 )% 60;
                        hours = hours < 10 ? "0"+hours : ""+hours;
                        minutes = minutes < 10 ? "0"+minutes : ""+minutes;
                        seconds = seconds < 10 ? "0"+seconds : ""+seconds;
                        $(td).html(hours+":"+minutes+":"+seconds);
                    }break;
                    default:
                        $(td).html(result[k][Object.keys(keys)[i]]);
                    break;
                }

                $(row).append(td);
            }
            $(tbody).append(row);
        }
        $(table).append(tbody);
        $("#timeline").html(table);
    });
}

function remove_from_timeline(id){
    if(confirm('Are you sure want to remove this event?')){
        var query={
            command : 'remove_event_from_timeline',
            params : {
                event_id:id
            }
        };
        send_api_query(query,function(r){
            update_timeline_table();
        });
    }
}

$(document).ready(function(){

  var playlists = api_get_playlists(function(r){
        for(var i=0;i<r.length;i++)
            $("#playlist_select").append('<option value="'+r[i]+'">'+r[i]+'</option>');
        $("#playlist_select").material_select('destroy');
        $("#playlist_select").material_select();
    });


    var modules = api_get_modules(function(r){
        for(var i=0;i<r.length;i++)
            $("#event_module_select").append('<option value="'+r[i]+'">'+r[i]+'</option>');
        $("#event_module_select").material_select('destroy');
        $("#event_module_select").material_select();
    });

    //window_id_select

    send_api_query({
        command : 'get_windows_count',
        params:[]
    },function(e){
        var windows = e.result;
        console.log("windows_count:"+windows);
        var html="<option value='' disabled selected>Select Window</option>";
        for(var i=0;i<windows;i++)
            html+="<option value='"+i+"'>"+i+"</option>";
        $('#window_id_select').html(html);
        $("#window_id_select").material_select('destroy');
        $('#window_id_select').material_select();
    });


    $('.modal').modal();
    $('select').material_select();
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: false, // Close upon selecting a date,
        format: 'dd.mm.yyyy',
    });
    $('.timepicker').pickatime({
        default: 'now', // Set default time: 'now', '1:30AM', '16:30'
        fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
        twelvehour: false, // Use AM/PM or 24-hour format
        donetext: 'OK', // text for done-button
        cleartext: 'Clear', // text for clear-button
        canceltext: 'Cancel', // Text for cancel-button
        autoclose: false, // automatic close timepicker
        ampmclickable: true, // make AM PM clickable
        aftershow: function(){} //Function for after opening timepicker
    });

    $('#add_event_button').click(add_event);

    //timeline table
    update_timeline_table();
});
