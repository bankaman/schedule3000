var editor,answer;
function send(){
    var data = JSON.stringify(editor.get());
    send_query("/api",data,function(text){
        answer.set(JSON.parse(text));
    });
}

//___________________________________
function new_playlist(){
    var name,type;
    if(null == (name = prompt("Имя плейлиста", 'new playlist'))) return;
    if(null == (type = prompt("Тип плейлиста", '0'))) return;

    editor.set({
        command : 'new_playlist',
        params : {
            'name':name,
            'type':type,
        },
    });
}
function get_playlists(){
    editor.set({
        command : 'add_from_template',
    });
}
function add_from_template(){
    var name;
    if(null == (name = prompt("Имя шаблона", 'miku'))) return;

    var time = Math.floor((new Date()).getTime()/1000)+3;
    editor.set({
        command : 'add_from_template',
        params : {
            'name':name,
            'time_begin':time,
        },
    });
}

function set_up(id){

    editor.set({
        command : id,
        params : {},
    });
}


$(document).ready(function(){
    var container = document.getElementById("jsoneditor");
    var answCont = document.getElementById("answer");
    var options = {};
    editor = new JSONEditor(container, options);
    answer = new JSONEditor(answCont, options);

    // set json
    var json = {command:'',params:{}};
    editor.set(json);
    send_api_query({command:'get_api_functions_list',params:{}},function(r){
        var btn = $('#buttons');
        var result = r.result;
        var html='';
        for(var i=0;i<result.length;i++){
            html+='<input type="button" value="'+result[i]+'" onclick="set_up(\''+result[i]+'\')">';
        }
        $(btn).html(html);
    });
});
