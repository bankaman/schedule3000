var files_queue=[];

function init(){
  update_playlists();


    $('.modal').modal();

    $('#modal_view_file').modal({
            complete: function() { $('#modal_view_file .modal-content').html("") } // Callback for Modal close
        });

    $('#add_playlist_button').on('click',new_playlist);

}

function update_playlists(){
  var query = {
      command : 'get_playlists',
      params : ''
  };
  send_api_query(query,function(r){
    var sel;
    sel = document.getElementById('playlist_selector');
    sel.innerHTML='';
    for(var i=0;i<r.result.length;i++){
      sel.innerHTML+="<option>"+r.result[i]+"</option>";
    }
    $(sel).material_select();
    get_current_playlist_files();
  });
}

function new_playlist(){
    var name,type;
    type = $('#select_playlist_type').val();
    name = $('#input_playlist_name').val();

    if(name.length===0)
        return alert('Playlist name is empty!');

    var query = {
        command : 'new_playlist',
        params : {
            'name':name,
            'type':type,
        },
    };
    send_api_query(query,function(result){
        update_playlists();
        $('#modal_add_playlist').modal('close');
    });
}


function upload_file_part(file,file_id,chunck_size,cur_chunk){
    var chunk = file.slice(cur_chunk*chunck_size, (cur_chunk+1)*chunck_size);
    var query = {
        command : "upload_file_part",
            params : {
            file_id : file_id,
            data_length : chunk.size,
            chunk_num : cur_chunk
        }
    };
    query = new Blob([JSON.stringify(query), "\0" ,chunk],  {type: 'application/octet-binary'});
    send_query('/api',query,function(r){
        r=JSON.parse(r);
        if(r.result!=cur_chunk){
            console.log(cur_chunk+" - fail, retry");
            upload_file_part(file,file_id,chunck_size,cur_chunk);
            return;
        }
        console.log(r.result+" - ok");

        cur_chunk++;
        if((cur_chunk-1)*chunck_size>file.size){
            console.log("Success uploading ''"+file.name+"'!");
            $('#file_progress').css('display','none');
            get_current_playlist_files();
            if(files_queue.length>0)
                upload_file();
        }else{
            $('#file_progress .progress .determinate').css('width',Math.floor(100*cur_chunk*chunck_size/file.size)+'%');
            upload_file_part(file,file_id,chunck_size,cur_chunk);
        }
    });
}

function upload_file(){
    if(files_queue.length==0)
        return;

    console.log(files_queue);
    var file = files_queue.shift();
    console.log(file);

    //update info on page
    $('#file_progress').css('display','block');
    $('#file_progress .cur_file').html(file.name);
    $('#file_progress .progress .determinate').css('width','0%');
    //end

    var chunck_size = 2 * 1024 * 1024; //2MB
    var plist = document.getElementById('playlist_selector').value;
    console.log('playlist',plist);
    var query = {
        command : 'upload_file',
        params : {
            playlist_name : plist,
            file_name : file.name,
            type : 0,
            byte_length : ""+file.size,
            chunk_size : chunck_size
        }
    }
    send_api_query(query,function(r){
        var file_id = r.result;
        upload_file_part(file,file_id,chunck_size,0);
    });
}

function upload_files(){
    var files = document.getElementById('input_file').files;

    console.log('upload files:',files);
    for(var i=0;i<files.length;i++){
        files_queue.push(files[i]);
    }

    console.log('upload files queue:',files_queue);

    upload_file();
}

function get_current_playlist_files(){
  var plist = document.getElementById('playlist_selector').value;
  var query = {
    command : 'get_playlist_files',
    params : {
      name : plist
    }
  }
  send_api_query(query,function(r){
    var tb=document.getElementById('playlist');
    var res = "<table class='striped centered'><tr><th>Name</th><th>Options</th></tr>";
    console.log(r.result);
    for (var i = 0; i < r.result.length; i++) {
      res += "<tr><td>"+r.result[i].name+"</td><td>\
        <!-- <a onclick='delete_from_playlist("+r.result[i].id+")' class='waves-effect waves-light btn red'>Delete from playlist</a> -->\
        <a data-href='/files/"+r.result[i].id+"' data-ext='"+r.result[i].ext+"' onclick='view_file(this)' class='waves-effect waves-light btn green'>View</a>\
        <a onclick='delete_from_disk("+r.result[i].id+")' class='waves-effect waves-light btn red'>Delete</a>\
      </td></tr>";
    }
    res += "</table>";
    tb.innerHTML = res;
  });
}

function delete_from_disk(file_id){
    if(!confirm("Are you sure want to delete this file?"))
        return;
    var query = {
        command : 'delete_file_from_disk',
        params : {
            file_id : file_id
        }
    }
  send_api_query(query,function(r){
      get_current_playlist_files();
  });
}

function delete_curent_playlist(){
    if(!confirm("Are you sure want to delete this playlist?"))
        return;
    var name=document.getElementById('playlist_selector').value;
    var query = {
        command : 'remove_playlist',
        params : {
          'name' : name
        }
      }
    console.log(query);
    send_api_query(query,function(r){
        update_playlists();
    });
}

function view_file(el){
    var ext = $(el).data('ext');
    var href = $(el).data('href');
    ext=ext.toLowerCase();
    switch(ext){
        case "png":
        case "jpg":
        case "jpeg":
            $('#modal_view_file .modal-content').html("<img src='"+href+"'/><a target='_blank' href='"+href+"'>download</>");
            $('#modal_view_file').modal('open');
        break;
        case "mp4":
        case "webm":
        case "mp3":
            $('#modal_view_file .modal-content').html("<video autoplay controls src='"+href+"'/><a target='_blank' href='"+href+"'>download</>");
            $('#modal_view_file').modal('open');
        break;
        default:
            window.open(href);
        break;
    }
}

$(document).ready(function() {
    $('select').material_select();
});
