//Запросы ----------------------------------------------------------------------------------------------------------------

function api_get_playlists(callback){
	var query = {
      command : 'get_playlists',
      params : ''
  };
  send_api_query(query, function(r){
		callback(r.result);
	});
}

function api_get_modules(callback){
    var query = {
      command : 'get_modules',
      params : ''
  };
  send_api_query(query, function(r){
        callback(r.result);
    });
}

//api query
function send_api_query(query, callback){
	send_query("/api",JSON.stringify(query),function(text){
			var responce = JSON.parse(text);
			if(responce.error)
				return show_error(responce.error);
			callback(responce);
	});
}

function show_error(e){
	alert(e);
}

//Отправка запросаresponce.errorresponce.error
function send_query(filename,query,after_complete){

	var xmlhttp = getXmlHttp(); // Создаём объект XMLHTTP
	xmlhttp.open('POST', filename, true); // Открываем асинхронное соединение
	xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); // Отправляем кодировку
	xmlhttp.send(query);

	//t=outP.innerHTML;
	query_in_process=true;
	xmlhttp.onreadystatechange = function() { // Ждём ответа от сервера
		if (xmlhttp.readyState == 4) { // Ответ пришёл
			query_in_process=false;
			if(xmlhttp.status == 200) { // Сервер вернул код 200 (что хорошо)
				//console.log(xmlhttp.responseText); // Выводим ответ сервера
				//outP.innerHTML=outputVar+xmlhttp.responseText;
				/*if(!xmlhttp.responseText){
					return;
				}
				try{
					data = JSON.parse(xmlhttp.responseText);
				}catch(e){
					show_error(xmlhttp.responseText);
					return;
				}

				if(!data){
					show_error(xmlhttp.responseText);
					return;
				}

				if(data.errors.length)
					show_error(JSON.stringify(data.errors));*/

				if(after_complete!=null) {
				    //after_complete(data.text);
				    after_complete(xmlhttp.responseText);
			    }


			}else show_error("Ошибка при отправке запроса "+xmlhttp.status);
		}
	}
}

function getXmlHttp() {
	var xmlhttp;
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
			xmlhttp = false;
		}
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}
