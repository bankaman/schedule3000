#ifndef TEXTURE_H
#define TEXTURE_H

/*
 * libsdl2 texture wrapper class
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <QString>
#include <QImage>
#include <QDebug>

//#include "glob.h"

class Texture
{
public:
    Texture();
    ~Texture();

    void createForStreaming(SDL_Renderer* r, int width, int height, char* format);
    bool load(SDL_Renderer* r,QString path);
    bool load_from_resources(SDL_Renderer* renderer, QString path);
    bool preload_surface(QString path);
    bool construct_texture(SDL_Renderer* r);
    void draw();
    void setBlendMode(SDL_BlendMode blending);
    Uint8 getAlpha();
    void setAlpha(Uint8 alpha_);
    void resizeToScreen(int window_id);

    void createBlack(SDL_Renderer*r);

    int getOrigW();
    int getOrigH();

    bool getIsLoaded();

    SDL_Texture *getTexturePointer();
    SDL_Surface *getSurfacePointer();

    void rotate(double angle_);
    void setClipRect(int x, int y, int w, int h);
    void setFlip(bool horisontal, bool vertical);


    //Эффекты
    void fadeout(Uint8 speed);
    void fadein(Uint8 speed);

    SDL_Rect position;
    SDL_Rect clip_rect;
    SDL_Point center;

private:
    SDL_Texture* sdl_texture;
    SDL_Renderer* render;
    SDL_Surface* sdl_surface;

    int alpha;

    double angle=0;
    SDL_RendererFlip flip=SDL_FLIP_NONE;

    bool fadeout_mode=false;
    bool fadein_mode=false;
    Uint8 fade_speed=1;

    int origW, origH;

    bool is_loaded;
    QString filename;

    void destroy_surface();
};

#endif // TEXTURE_H
