
/*
 * Schedule3000 music player module
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include "vlcmusicplayer.h"

#include <vlc/libvlc_version.h>
#include <ctime>

VlcMusicPlayer::VlcMusicPlayer()
{
    cur=-1;
    duration = -1;
    media = nullptr;
    player = nullptr;
}

VlcMusicPlayer::~VlcMusicPlayer()
{
    stop();
}


void VlcMusicPlayer::setFileList(QStringList files)
{
    if(cur>=files.length())
        cur=0;
    playlist=files;
    if(cur<0)
        cur=0;

    if(playlist_random)
        randomize_plylist();
}

void VlcMusicPlayer::init()
{
    //qDebug()<<"init";

    state = STATE_IDLE;
    loadFile();
}

void VlcMusicPlayer::process()
{
    if(state == STATE_OVER) return;

    player_state=libvlc_media_player_get_state(player);
    if(player_state==libvlc_Ended){
        nextElement();
    }
    if( state == STATE_PAUSED && player_state!=libvlc_Paused)
        pause();

    if( state == STATE_PLAYING && player_state==libvlc_Error)
        nextElement();
}

void VlcMusicPlayer::draw()
{
}

void VlcMusicPlayer::nextElement()
{
    libvlc_media_player_stop (player);

    cur++;
    if(cur>=playlist.length()){
        if(mode==MODE_PERMANENT){
            cur=0;
            if(playlist_random)
                randomize_plylist();
        }else{
            cur = playlist.length()-1;
            state = STATE_OVER;
            return;
        }
    }
    if(!loadFile()){
        nextElement();
        return;
    }
    state=STATE_PLAYING;
    libvlc_audio_set_volume (player,volume);
    libvlc_media_player_play (player);
}

void VlcMusicPlayer::prevElement()
{
}

bool VlcMusicPlayer::loadFile()
{
    if(cur>=playlist.length())
        return false;
    freeMedia();
    media = libvlc_media_new_path (vlc_instance, playlist.at(cur).toStdString().c_str());
    if(media==nullptr){
        qDebug()<<"Error loading file "<<playlist.at(cur)<<endl;
        return false;
    }
    if(player!=nullptr){
        libvlc_media_player_release(player);
        qDebug()<<"vlc audio: releasing player";
        player=nullptr;
    }
    player = libvlc_media_player_new_from_media (media);

    if(G::debug)
        qDebug()<<"Loaded file "<<playlist.at(cur);

    return true;
}

void VlcMusicPlayer::freeMedia()
{
    if(media!=NULL){
        libvlc_media_release (media);
        media = NULL;
    }
}

int VlcMusicPlayer::get_file_duration(int i)
{
    int dur=0;
    libvlc_media_t *m;
    m = libvlc_media_new_path (vlc_instance, playlist.at(i).toStdString().c_str());
    if(m==NULL){
        qDebug()<<"Error loading file "<<playlist.at(i)<<endl;
        libvlc_media_release (m);
        return -1;
    }
    vlc_parce_media(m);
    dur = libvlc_media_get_duration(m);
    if(dur<0){
        qDebug()<<"Error determaning duration of file "<<playlist.at(i)<<endl;
        libvlc_media_release (m);
        return -2;
    }
    libvlc_media_release (m);
    return dur;
}

void VlcMusicPlayer::randomize_plylist()
{
    qDebug()<<"musicplayer RandomizE!";
    QStringList list = playlist;
    playlist.clear();
    int rnd;
    std::srand(unsigned(std::time(0))+window_id);
    while(list.length()>0){
        rnd=std::rand()%list.length();
        playlist.append(list.at(rnd));
        list.removeAt(rnd);
    }
}


void VlcMusicPlayer::pause()
{
    state = STATE_PAUSED;
    if(player==nullptr){
        qWarning()<<"Trying to pause not existing player"<<playlist;
        return;
    }
    qDebug()<<"pausing music player";
    libvlc_media_player_set_pause(player, 1);
    qDebug()<<"music player was paused";
}

void VlcMusicPlayer::resume()
{
    state = STATE_PLAYING;
    libvlc_media_player_set_pause(player, 0);
}


void VlcMusicPlayer::start()
{
    //qDebug()<<"start";
    state = STATE_PLAYING;
    libvlc_audio_set_volume (player,volume);
    libvlc_media_player_play (player);
}

void VlcMusicPlayer::stop()
{
    qDebug()<<"module state:"<<state;
    qDebug()<<"musicplayer stop";
    if(player==nullptr){
        qDebug()<<"player is null";
        return;
    }
    player_state=libvlc_media_player_get_state(player);
    qDebug()<<"player state was gotten";
    if(player_state==libvlc_Playing){
        qDebug()<<"trying to stop player";
        libvlc_media_player_stop (player);
        qDebug()<<"player was stopped";
    }
    qDebug()<<"releasing player";
    libvlc_media_t *m;
    m=libvlc_media_player_get_media(player);
    libvlc_media_release(m);
    libvlc_media_player_release (player);
    player = nullptr;
    qDebug()<<"musicplayer stopped";
}


void VlcMusicPlayer::fadein()
{
    state = STATE_PLAYING;
    start();
}

void VlcMusicPlayer::fadeout()
{
    state = STATE_FADE;
    stop();
}

void VlcMusicPlayer::seek(uint64_t position)
{
    state=STATE_PLAYING;
    qDebug()<<"musicplayer seek";
    int curfile=-1;
    uint64_t elapsed=0;
    uint64_t duration=0;
    while(elapsed<position){
        curfile++;
        duration = get_file_duration(curfile);
        elapsed += duration;
    }
    elapsed-=duration;
    uint64_t pos;
    pos = position - elapsed;
    cur=curfile;
    loadFile();
    libvlc_media_player_play(player);
    libvlc_audio_set_volume (player,volume);
    libvlc_media_player_set_position(player,(float)pos/duration);
}


int VlcMusicPlayer::getDuration()
{
    int sdur=0;
    for(int i=0;i<playlist.length();i++){
        sdur+=get_file_duration(i);

    }
    return sdur;
}

void VlcMusicPlayer::setParams(QHash<QString, QVariant> params)
{
    LibvlcPlayer::setParams(params);
    if(player!=nullptr)
        libvlc_audio_set_volume (player,volume);
}
