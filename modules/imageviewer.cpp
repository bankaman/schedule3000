
/*
 * Schedule3000 image viewer module
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include "imageviewer.h"
#include "windowctr.h"

#include <ctime>

ImageViewer::ImageViewer()
{
    change_image_timer = -1;
    image_duration = 5000;
    fade_duration = 5;
    cur_img = -1;
    SCR_H = 0;
    SCR_W = 0;
    paused = false;
    in_update = false;
}

ImageViewer::~ImageViewer()
{
    this->stop();
    clear_images();
}


void ImageViewer::clear_images()
{
    for(int i=0;i<images.length();i++){
        if(images.at(i)->isBuffered)
            delete images.at(i)->image;
        delete images.at(i);
    }
    images.clear();
}

void ImageViewer::draw_no_images()
{
    if(!no_image.getIsLoaded())
        no_image.load_from_resources(renderer,":/resources/empty_playlist.jpg");
    resizeImg(&no_image);
    no_image.draw();
}

void ImageViewer::randomize_images()
{
    qDebug()<<"imageviewer RandomizE!";
    QVector<ImgEl*> im = images;
    images.clear();
    int rnd;
    std::srand(unsigned(std::time(0))+window_id);
    while(im.length()>0){
        rnd=std::rand()%im.length();
        images.append(im.at(rnd));
        im.removeAt(rnd);
    }
}


void ImageViewer::setFileList(QStringList files)
{
    mutex.lock();
    in_update = true;

    images_list = files;
    clear_images();
    ImgEl* imel;
    for(int i=0;i<images_list.length();i++){
        imel = new ImgEl();
        imel->filename = images_list.at(i);
        imel->isBuffered = false;
        images.append(imel);
    }
    in_update = false;
    cur_img=0;
    if(playlist_random)
        randomize_images();
    mutex.unlock();
}


void ImageViewer::init()
{
    cur_img = 0;
    if(images.length()>0){
        updateBuffer();
        images.at(cur_img)->image->setAlpha(255);
    }
}

void ImageViewer::process()
{

}

void ImageViewer::draw(){
    mutex.lock();

    if(cur_img<0){
        mutex.unlock();
        return;
    }

    if(in_update){
        mutex.unlock();
        return;
    }

    if(images.length()==0){
        state=STATE_OVER;
        draw_no_images();
        mutex.unlock();
        return;
    }

    updateBuffer();
    if((SCR_H != WindowCtr::get_height(window_id)) || (SCR_W != WindowCtr::get_width(window_id)))
        resizeImg(images.at(cur_img)->image);
    for(int i=0;i<images.length();i++){
        if(images.at(i)->isBuffered)
            images.at(i)->image->draw();
    }
    if(state == STATE_FADE){
        if(images.at(cur_img)->image->getAlpha()==0)
            state=STATE_OVER;
    }

    mutex.unlock();
}


void ImageViewer::resizeImg(Texture* image){
    if(image==NULL)
        return;
    if(!image->getIsLoaded())
        return;
    int w = WindowCtr::get_width(window_id);
    int h = WindowCtr::get_height(window_id);
    image->position.w = w;
    image->position.h = image->position.w * image->getOrigH() / image->getOrigW();
    if(image->position.h > h){
        image->position.h = h;
        image->position.w = image->position.h * image->getOrigW() / image->getOrigH();
    }
    image->position.x=w/2-image->position.w/2;
    image->position.y=h/2-image->position.h/2;

    SCR_W=w;
    SCR_H=h;
}


// Обновление буффера изображений
void ImageViewer::updateBuffer()
{    
    //Нахождение предыдущего и следующего соединения
    int prev,next;
    prev = cur_img - 1;
    next = cur_img + 1;
    if(prev<0)
        prev = images.length()-1;
    if(next>=images.length())
        next = 0;

    if((images.at(next)->isBuffered) && (images.at(prev)->isBuffered))
        return;

    ImgEl *e;
    //Загрузка изображений и выгрузка не нужных
    for(int i=0;i<images.length();i++){
        e = images.at(i);
        if((i!=prev)&&(i!=next)&&(i!=cur_img)){
            if(e->isBuffered){
                if(e->image->getAlpha()==255){
                    e->image->fadeout(fade_duration);
                }else if(e->image->getAlpha()==0){
                    delete e->image;
                    e->image=nullptr;
                    e->isBuffered = false;
                }
            }
        }else{
            if(!e->isBuffered){
                e->image = new Texture();
                if(!e->image->load(renderer, e->filename)){
                    delete e->image;
                    images.remove(i);
                    break;
                }
                if(i!=cur_img)
                    e->image->setAlpha(0);
                else{
                    e->image->setAlpha(255);
                    resizeImg(e->image);
                }
                e->isBuffered = true;
            }
        }
    }
}

void ImageViewer::nextElement()
{
    mutex.lock();
    if(images.length()==0)
        return;
    if(cur_img<0)
        return;
    if(images.length()==1)
        return;
    if(images.at(cur_img)->isBuffered)
        images.at(cur_img)->image->fadeout(fade_duration);


    cur_img++;

    if(cur_img>=images.length()){
        switch (mode) {
        case MODE_DAILY:
        case MODE_NORMAL:
            cur_img = images.length()-1;
            stop();
            fadeout();
            return;
        break;
        case MODE_PERMANENT:
            cur_img = 0;
            if(playlist_random)
                randomize_images();
        break;
        }
    }
    if(images.at(cur_img)->isBuffered){
        resizeImg(images.at(cur_img)->image);
        images.at(cur_img)->image->fadein(fade_duration);
    }
    mutex.unlock();
}

void ImageViewer::prevElement()
{

}


void ImageViewer::start()
{
    if(change_image_timer < 0)
        change_image_timer = SDL_AddTimer( image_duration, ImageViewer::change_image_callback, this );
    state = STATE_PLAYING;
}

void ImageViewer::stop()
{
    SDL_RemoveTimer(change_image_timer);
    change_image_timer = -1;
    state = STATE_IDLE;
}

void ImageViewer::fadein()
{       
    start();
    if(images.length()==0)
        return;
    images.at(cur_img)->image->setAlpha(0);
    images.at(cur_img)->image->fadein(fade_duration);

}

void ImageViewer::fadeout()
{
    images.at(cur_img)->image->fadeout(fade_duration);
    state = STATE_FADE;
}

Uint32 ImageViewer::change_image_callback(Uint32 interval, void *param)
{
    if(param==NULL) return 0;
    ImageViewer *me = (ImageViewer*)param;
    me->nextElement();
    return interval;
}


void ImageViewer::setParams(QHash<QString, QVariant> params)
{
    qDebug()<<params;
    ModuleGeneric::setParams(params);
    image_duration = params.value("image_duration",5000).toInt();
    fade_duration = params.value("fade_duration",5).toInt();


}

int ImageViewer::getDuration()
{
    return images_list.length()*image_duration*1000;
}


void ImageViewer::pause()
{
    if(!paused){
        SDL_RemoveTimer(change_image_timer);
        change_image_timer = -1;
        state = STATE_PAUSED;
        paused = true;
    }
}

void ImageViewer::resume()
{
    if(paused){
        start();
        paused=false;
    }
}
