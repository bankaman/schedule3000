#ifndef CLOCKOVERLAY_H
#define CLOCKOVERLAY_H

/*
 * Schedule3000 clock overlay module
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include <QTime>

#include "../modulegeneric.h"
#include "../texture.h"


class ClockOverlay : public ModuleGeneric
{
public:
    ClockOverlay();
    ~ClockOverlay();

    // ModuleGeneric interface
    void setFileList(QStringList files);
    void init();
    void process();
    void draw();
    void nextElement();
    void prevElement();
    void start();
    void stop();
    void pause();
    void resume();
    void setParams(QHash<QString, QVariant>);

private:
    QTime time;
    int h,m;
    int h1,h2,m1,m2;

    int x,y;

    bool paused; //Вдруг понадбится остановить время

    Texture numbers;
    Texture points;
    Texture bg;


};

#endif // CLOCKOVERLAY_H
