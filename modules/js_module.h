#ifndef JS_MODULE_H
#define JS_MODULE_H

#include "../modulegeneric.h"
#include "../texture.h"
#include "../font.h"

#include <QProcess>

extern "C"{
    #include <duktape/src/duktape.h>
}

class js_module : public ModuleGeneric
{

public:
    js_module();
    ~js_module();
    static void fatal_handler(void *udata, const char *msg);

public:
    void set_js(QString filename);

    void init();
    void draw();
    void start();
    void stop();
    void pause();
    void resume();
    void fadein();

    QVector<Texture*> images;
    QVector<int> images_deleted_ids;

    QVector<Font*> fonts;
    void debug_stack();

    QString get_filename();

    void on_keydown(SDL_Event &);
    void on_keyup(SDL_Event &);
    void on_textinput(SDL_Event &e);
    void on_textedit(SDL_Event &e);

private:
    duk_context *ctx;
    QString filename;

    void call_js(const char* func_name);
    bool check_function_exists(const char* func_name);

    static js_module *get_js_module(duk_context *ctx);
    static duk_ret_t Image_constructor(duk_context *ctx);
    static duk_ret_t Image_get_width(duk_context *ctx);
    static duk_ret_t Image_get_height(duk_context *ctx);
    static duk_ret_t Image_rotate(duk_context *ctx);
    static duk_ret_t Image_set_transparency(duk_context *ctx);
    static duk_ret_t Image_set_clip(duk_context *ctx);
    static duk_ret_t Image_set_flip(duk_context *ctx);
    static duk_ret_t Image_free(duk_context *ctx);

    static duk_ret_t Font_constructor(duk_context *ctx);
    static duk_ret_t Font_draw_text(duk_context *ctx);
    static duk_ret_t Font_set_color(duk_context *ctx);
    static duk_ret_t Font_set_size(duk_context *ctx);

    static duk_ret_t Screen_draw(duk_context *ctx);
    static duk_ret_t Screen_get_width(duk_context *ctx);
    static duk_ret_t Screen_get_height(duk_context *ctx);
    static duk_ret_t Screen_set_color(duk_context *ctx);
    static duk_ret_t Screen_draw_rect(duk_context *ctx);

    static duk_ret_t Sys_system(duk_context *ctx);
    static duk_ret_t Sys_api(duk_context *ctx);
    static duk_ret_t Sys_get_state(duk_context *ctx);
    static duk_ret_t Sys_set_state(duk_context *ctx);
    static duk_ret_t Sys_start_text_input_mode(duk_context *ctx);
    static duk_ret_t Sys_stop_text_input_mode(duk_context *ctx);


};

#endif // JS_MODULE_H
