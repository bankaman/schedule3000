#ifndef VLCVIDEOPLAYER_H
#define VLCVIDEOPLAYER_H

/*
 * Schedule3000 video player module
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_mutex.h>

#include "libvlcplayer.h"
#include "../glob.h"
#include "../texture.h"

#include <vlc/vlc.h>

#include <QDebug>

class VlcVideoPlayer : public LibvlcPlayer
{
public:
    VlcVideoPlayer();
    ~VlcVideoPlayer();

    // ModuleGeneric interface
    void setFileList(QStringList files);
    void init();
    void process();
    void draw();
    void nextElement();
    void prevElement();
    void start();
    void stop();
    void fadein();
    void fadeout();
    int getDuration();
    void pause();
    void resume();
    void seek(uint64_t position);

    bool isOver();

    static int pcounter;
    static void* lock(void *data, void **p_pixels); // callback lock VLC prepares to render a video frame.
    static void unlock(void *data, void *id, void *const *p_pixels); // callback unlock VLC just rendered a video frame.
    static void display(void *data, void *id); // callback display VLC wants to display a video frame.
    static unsigned format(void **opaque, char *chroma, unsigned *width, unsigned *height, unsigned *pitches, unsigned *lines);
    static void format_cleanup(void *opaque);

    int getId();
    Texture texture;
    Texture bg;
    int video_w, video_h;
    unsigned int display_w, display_h;

    SDL_mutex* mutex;
    void *videoBuffer[3];
    bool update_texture;
    bool tone_updated;

    libvlc_media_player_t *player;

    static bool debug;

    void setParams(QHash<QString, QVariant> params);

private:
    QStringList playlist;
    int cur;



    libvlc_state_t player_state;
    bool first_update;
    bool is_fadein;
    bool is_fadeout;
    int SCR_H,SCR_W;

    int id;
    int duration;

    void loadFile();
    void sizeToScreen();
    void freeVideoBuffer();

    void free_player();

    char chroma[15]; //pixel format

    int state_before_paused;

    void randomize_plylist();

    uint64_t get_file_duration(int id);


};

#endif // VLCVIDEOPLAYER_H
