#ifndef DUBLICATOR_H
#define DUBLICATOR_H

#include "../modulegeneric.h"
#include "../windowctr.h"

#include <SDL2/SDL.h>

class Dublicator : public ModuleGeneric
{
public:
    Dublicator();
    ~Dublicator();
    int orig_window_id;
    int event_id;
    bool updated;
    ModuleGeneric *module;

    SDL_Texture *target;
    SDL_Texture *copy;
    SDL_Surface* buffer;

    void init();
    void setParams(QHash<QString, QVariant> params);
    void process();
    void draw();

private:
    void setup();
    void destroy();

    void process_videoplayer();
    void process_dublicator();
};

#endif // DUBLICATOR_H
