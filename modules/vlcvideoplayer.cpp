
/*
 * Schedule3000 video player module
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include "vlcvideoplayer.h"
#include "windowctr.h"

#include <vlc/libvlc_version.h>

#include <ctime>

int VlcVideoPlayer::pcounter = 0;
bool VlcVideoPlayer::debug = true;

VlcVideoPlayer::VlcVideoPlayer()
{
    player=nullptr;
    cur=-1;
    id=VlcVideoPlayer::pcounter;
    VlcVideoPlayer::pcounter++;

    SCR_H=0;
    SCR_W=0;

    mutex = SDL_CreateMutex();
    update_texture = false;
    playlist_random = false;
    video_w=0;
    video_h=0;
    display_w=1;
    display_h=1;
    videoBuffer[0]=nullptr;
    videoBuffer[1]=nullptr;
    videoBuffer[2]=nullptr;
    strcpy(chroma,"RGB24");
    is_fadeout = false;
    is_fadein = false;
    tone_updated = false;
}

VlcVideoPlayer::~VlcVideoPlayer()
{
    free_player();
    VlcVideoPlayer::pcounter--;
    SDL_DestroyMutex(mutex);
    freeVideoBuffer();
    qDebug()<<"deleting videoplayer";
}


void VlcVideoPlayer::setFileList(QStringList files)
{
    if(cur>=files.length())
        cur=0;
    playlist=files;
    if(cur<0)
        cur=0;
    if(playlist_random)
        randomize_plylist();
}

void VlcVideoPlayer::init()
{
    bg.createBlack(renderer);
    bg.setAlpha(255);
}

void VlcVideoPlayer::process()
{
    if((state != STATE_OVER) || (state != STATE_FADE) ){
        if(player==nullptr) return;
        player_state=libvlc_media_player_get_state(player);

        if(player_state==libvlc_Ended){
            nextElement();
        }
        if(state==STATE_PAUSED && player_state==libvlc_Playing)
            pause();
    }
}

void VlcVideoPlayer::draw()
{
    if(update_texture){
        if((texture.getOrigW()!=video_w) || (texture.getOrigH()!=video_h) ){
            texture.createForStreaming(renderer,video_w,video_h,chroma);
            sizeToScreen();
        }
        //SDL_UpdateTexture(texture.getTexturePointer(),NULL,videoBuffer[0], video_w);
        SDL_LockMutex(mutex);
        SDL_UpdateYUVTexture(
                    texture.getTexturePointer(),
                    nullptr,
                    (unsigned char*)videoBuffer[0],
                    video_w,
                    (unsigned char*)videoBuffer[1],
                    video_w,
                    (unsigned char*)videoBuffer[2],
                    video_w
                );
        SDL_UnlockMutex(mutex);

        update_texture=false;
        if(first_update){
            first_update = false;
            if(is_fadein){
                texture.setAlpha(0);
                texture.fadein(5);
                bg.setAlpha(0);
                bg.fadein(5);
                //state = STATE_FADE;
            }
        }
        tone_updated=true;
    }

    //if(state==STATE_FADE){
    if(is_fadein){
        libvlc_audio_set_volume(player,texture.getAlpha()*100/255);
        if(texture.getAlpha()==255){
            //state = STATE_PLAYING;
            is_fadein=false;
            libvlc_audio_set_volume (player,volume);
        }
    }
    if(is_fadeout){
        if(texture.getAlpha()<=0){
            state = STATE_OVER;
            is_fadeout=false;
        }
    }
    //}


     if((SCR_H != WindowCtr::get_height(window_id)) || (SCR_W != WindowCtr::get_width(window_id)))
        sizeToScreen();


    bg.draw();
    if(tone_updated)
        texture.draw();

    //qDebug()<<bg.getAlpha();
}

void VlcVideoPlayer::sizeToScreen(){
    Texture* image;


    image = &texture;

    int w = WindowCtr::get_width(window_id);
    int h = WindowCtr::get_height(window_id);

    image->position.w = w;
    image->position.h = image->position.w * image->getOrigH() / display_w;
    if(image->position.h > h){
        image->position.h = h;
        image->position.w = image->position.h * image->getOrigW() / display_h;
    }
    image->position.x=w/2-image->position.w/2;
    image->position.y=h/2-image->position.h/2;

    bg.position.x=0;
    bg.position.y=0;
    bg.position.w=w;
    bg.position.h=h;

    SCR_W=w;
    SCR_H=h;
}

void VlcVideoPlayer::freeVideoBuffer()
{
    if(videoBuffer[0]!=nullptr){
        delete[] (char*)videoBuffer[0];
        videoBuffer[0]=nullptr;
    }
    if(videoBuffer[1]!=nullptr){
        delete[] (char*)videoBuffer[1];
        videoBuffer[1]=nullptr;
    }
    if(videoBuffer[2]!=nullptr){
        delete[] (char*)videoBuffer[2];
        videoBuffer[2]=nullptr;
    }
}

void VlcVideoPlayer::free_player()
{
    if(player!=nullptr){
        qDebug()<<"free_player!";
        libvlc_media_player_stop(player);
        libvlc_media_player_release(player);
        player = nullptr;
    }
}

void VlcVideoPlayer::randomize_plylist()
{
    qDebug()<<"videoplayer RandomizE!";
    QStringList list = playlist;
    playlist.clear();
    int rnd=0;
    std::srand(unsigned(std::time(0))+window_id);
    while(list.length()>0){
        rnd=std::rand()%list.length();
        playlist.append(list.at(rnd));
        list.removeAt(rnd);
    }
}

uint64_t VlcVideoPlayer::get_file_duration(int id)
{
    libvlc_media_t *m;
    int64_t dur;
    m = libvlc_media_new_path (vlc_instance, playlist.at(id).toStdString().c_str());
    if(m==NULL){
        qWarning()<<"Error loading file "<<playlist.at(id)<<endl;
        //libvlc_media_release (m);
        return -1;
    }
    //libvlc_media_parse_with_options(m,libvlc_media_parse_local,-1);
    if(!vlc_parce_media(m)){
        qWarning()<<"Error parsing file "<<playlist.at(id)<<endl;
        libvlc_media_release (m);
        return -2;
    }

    dur = libvlc_media_get_duration(m);
    qDebug()<<"duration:"<<dur<<"| video: "<<playlist.at(id)<<endl;
    if(dur<0){
        qDebug()<<"Error determaning duration of file "<<playlist.at(id)<<endl;
        libvlc_media_release (m);
        return -3;
    }
    libvlc_media_release (m);
    return dur;
}



void VlcVideoPlayer::nextElement()
{
    free_player();
    tone_updated=false;
    first_update=true;

    cur++;
    if(cur>=playlist.length()){
        switch(mode){
            case MODE_DAILY:
            case MODE_NORMAL:
                cur=0;
                //state = STATE_OVER;
                fadeout();
                return;
            break;

            case MODE_PERMANENT:
                cur=0;
                randomize_plylist();
            break;

            default:
                return;
        }
    }
    loadFile();
    libvlc_audio_set_volume (player,volume);
    libvlc_media_player_play (player);
}

void VlcVideoPlayer::prevElement()
{
}

// VLC prepares to render a video frame.
void* VlcVideoPlayer::lock(void *data, void **p_pixels)
{
    VlcVideoPlayer* c = (VlcVideoPlayer*)data;

    SDL_LockMutex(c->mutex);

    p_pixels[0] = c->videoBuffer[0];
    p_pixels[1] = c->videoBuffer[1];
    p_pixels[2] = c->videoBuffer[2];

    return nullptr;
}
// VLC just rendered a video frame.
void VlcVideoPlayer::unlock(void *data, void*, void * const *)
{
    VlcVideoPlayer* c = (VlcVideoPlayer*)data;

    SDL_UnlockMutex(c->mutex);
    c->update_texture=true;

}
// VLC wants to display a video frame.
void VlcVideoPlayer::display(void *, void *)
{

}

unsigned VlcVideoPlayer::format(void **opaque, char *chroma, unsigned *width, unsigned *height, unsigned *pitches, unsigned *lines)
{
    if(debug)
        qDebug()<<"FORMAT: width:"<<*width<<" height:"<<*height<<" chroma:"<<(chroma)
           <<" pitches:"<<*pitches<<" lines:"<<*lines;

    VlcVideoPlayer* me;
    me=(VlcVideoPlayer*)*opaque;

    strcpy(me->chroma,chroma);

    int w = *width;
    int h = *height;

    int size = w*h;

    //me->freeVideoBuffer();

    /*me->videoBuffer[0] = new char[size];
    me->videoBuffer[1] = new char[size/2];
    me->videoBuffer[2] = new char[size/2];*/

    me->videoBuffer[0] = new char[size];
    me->videoBuffer[1] = new char[size];
    me->videoBuffer[2] = new char[size];

    me->video_w=w;
    me->video_h=h;

    pitches[0] = w;
    pitches[1] = w;
    pitches[2] = w;

    lines[0] = h;
    lines[1] = h;
    lines[2] = h;

    libvlc_video_get_size(me->player,0,&me->display_w, &me->display_h);

    if(debug)
        qDebug()<<"width:"<<*width<<" height:"<<*height<<" chroma:"<<(chroma)
           <<" pitches:"<<*pitches<<" lines:"<<*lines<<"d_w:"<<me->display_w<<"| d_h:"<<me->display_h;

    return 1;
}

void VlcVideoPlayer::format_cleanup(void *opaque)
{
    VlcVideoPlayer* me;
    me=(VlcVideoPlayer*)opaque;
    me->freeVideoBuffer();
    qDebug()<<"format cleanup!"<<endl;
}

int VlcVideoPlayer::getId()
{
    return id;
}

void VlcVideoPlayer::setParams(QHash<QString, QVariant> params)
{
    LibvlcPlayer::setParams(params);
    if(player!=nullptr)
        libvlc_audio_set_volume (player,volume);
}


void VlcVideoPlayer::loadFile()
{
    if(cur>=playlist.length())
        return;

    libvlc_media_t *m;
    m = libvlc_media_new_path (vlc_instance, playlist.at(cur).toStdString().c_str());
    if(m==nullptr){
        qWarning()<<"Error loading file "<<playlist.at(cur)<<endl;
        nextElement();
        return;
    }
    if(player==nullptr)
        player = libvlc_media_player_new_from_media (m);
    else
        libvlc_media_player_set_media(player,m);
    libvlc_media_release(m);

    QString format="YV12";
    texture.createForStreaming(renderer,320,240, (char*)format.toStdString().c_str());

    libvlc_video_set_callbacks(player, VlcVideoPlayer::lock, VlcVideoPlayer::unlock, VlcVideoPlayer::display, this);

    libvlc_video_set_format_callbacks(player, VlcVideoPlayer::format, VlcVideoPlayer::format_cleanup);

    //libvlc_audio_set_volume(player,256);
    if(debug)
        qDebug()<<"Loaded video file "<<playlist.at(cur);

}


void VlcVideoPlayer::start()
{
    if(playlist.length()==0){
        state=STATE_OVER;

        return;
    }

    first_update=true;
    tone_updated = false;
    cur=0;
    loadFile();
    libvlc_media_player_play(player);
    libvlc_audio_set_volume (player,volume);
    state = STATE_PLAYING;
}

void VlcVideoPlayer::stop()
{
    libvlc_media_player_stop (player);
}

bool VlcVideoPlayer::isOver()
{
    return (state == STATE_OVER);
}


void VlcVideoPlayer::fadein()
{

    start();
    libvlc_audio_set_volume(player,0);
    state = STATE_PLAYING;
    first_update=true;
    is_fadein = true;
    is_fadeout = false;
    texture.setAlpha(0);
    bg.setAlpha(0);
    bg.fadein(5);
    texture.fadein(5);
}

void VlcVideoPlayer::fadeout()
{
    state = STATE_FADE;
    is_fadein = false;
    is_fadeout = true;
    tone_updated=true;
    texture.fadeout(5);
    bg.fadeout(5);
}


int VlcVideoPlayer::getDuration()
{
    int dur=0;    
    for(int i=0;i<playlist.length();i++){
        dur+=get_file_duration(i);
    }
    return dur;
}


void VlcVideoPlayer::pause()
{
    if(player==NULL){
        qDebug()<<"pausing not loaded video:"<<state;
        return;
    }

    libvlc_media_player_pause(player);
    state=STATE_PAUSED;
}

void VlcVideoPlayer::resume()
{
    if(state==STATE_PAUSED){
        libvlc_media_player_play(player);
        libvlc_audio_set_volume (player,volume);
        state=STATE_PLAYING;
    }else{
        qDebug()<<"resuming not paused video, state:"<<state;
    }
}

void VlcVideoPlayer::seek(uint64_t position)
{
    state=STATE_PLAYING;
    qDebug()<<"videoplayer seek";
    int curfile=-1;
    uint64_t elapsed=0;
    uint64_t duration=0;
    while(elapsed<position){
        curfile++;
        duration = get_file_duration(curfile);
        elapsed += duration;
    }
    elapsed-=duration;
    uint64_t pos;
    pos = position - elapsed;
    cur=curfile;
    loadFile();
    libvlc_media_player_play(player);
    libvlc_audio_set_volume (player,volume);
    libvlc_media_player_set_position(player,(float)pos/duration);

}
