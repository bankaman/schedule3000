#include "js_module.h"

#include "windowctr.h"

extern "C"{
    #include <duktape_helpers/console/duk_console.h>
}

#include <QFile>

//duk_context *js_module::ctx;

js_module::js_module()
{
    ctx = duk_create_heap(nullptr, nullptr, nullptr, ctx, js_module::fatal_handler);
    if (!ctx) {
        qWarning("Failed to create a Duktape heap.\n");
    }
    duk_console_init(ctx, DUK_CONSOLE_PROXY_WRAPPER /*flags*/);
    //qDebug()<<(long) duk_get_top(ctx);

    //IMAGE CLASS
    duk_push_c_function(ctx, js_module::Image_constructor, 1 /*nargs*/);
    duk_push_object(ctx);
    duk_push_c_function(ctx,js_module::Image_get_width,0);
    duk_put_prop_string(ctx,-2,"get_width");
    duk_push_c_function(ctx,js_module::Image_get_height,0);
    duk_put_prop_string(ctx,-2,"get_height");
    duk_push_c_function(ctx,js_module::Image_rotate,3);
    duk_put_prop_string(ctx,-2,"rotate");
    duk_push_c_function(ctx,js_module::Image_set_transparency,1);
    duk_put_prop_string(ctx,-2,"set_transparency");
    duk_push_c_function(ctx,js_module::Image_set_clip,4);
    duk_put_prop_string(ctx,-2,"set_clip");
    duk_push_c_function(ctx,js_module::Image_set_flip,2);
    duk_put_prop_string(ctx,-2,"set_flip");
    duk_push_c_function(ctx,js_module::Image_free,2);
    duk_put_prop_string(ctx,-2,"free");

    duk_put_prop_string(ctx, -2, "prototype");
    duk_put_global_string(ctx,"Image");

    //FONT CLASS
    duk_push_c_function(ctx, js_module::Font_constructor, 1 /*nargs*/);
    duk_push_object(ctx);

    duk_push_c_function(ctx,js_module::Font_draw_text,3);
    duk_put_prop_string(ctx,-2,"draw_text");
    duk_push_c_function(ctx,js_module::Font_set_color,4);
    duk_put_prop_string(ctx,-2,"set_color");
    duk_push_c_function(ctx,js_module::Font_set_size,1);
    duk_put_prop_string(ctx,-2,"set_size");

    duk_put_prop_string(ctx, -2, "prototype");
    duk_put_global_string(ctx,"Font");

    duk_push_object(ctx);
    duk_push_c_function(ctx,js_module::Screen_draw,5);
    duk_put_prop_string(ctx,-2,"draw");
    duk_push_c_function(ctx,js_module::Screen_get_width,0);
    duk_put_prop_string(ctx,-2,"get_width");
    duk_push_c_function(ctx,js_module::Screen_get_height,0);
    duk_put_prop_string(ctx,-2,"get_height");
    duk_push_c_function(ctx,js_module::Screen_set_color,4);
    duk_put_prop_string(ctx,-2,"set_color");
    duk_push_c_function(ctx,js_module::Screen_draw_rect,4);
    duk_put_prop_string(ctx,-2,"draw_rect");
    duk_put_global_string(ctx,"Screen");

    duk_push_object(ctx);
    duk_push_c_function(ctx,js_module::Sys_system,2);
    duk_put_prop_string(ctx,-2,"system");
    duk_push_c_function(ctx,js_module::Sys_api,1);
    duk_put_prop_string(ctx,-2,"api");
    duk_push_c_function(ctx,js_module::Sys_get_state,0);
    duk_put_prop_string(ctx,-2,"get_state");
    duk_push_c_function(ctx,js_module::Sys_set_state,1);
    duk_put_prop_string(ctx,-2,"set_state");
    duk_push_c_function(ctx,js_module::Sys_start_text_input_mode,0);
    duk_put_prop_string(ctx,-2,"start_text_input_mode");
    duk_push_c_function(ctx,js_module::Sys_stop_text_input_mode,0);
    duk_put_prop_string(ctx,-2,"stop_text_input_mode");
    duk_put_global_string(ctx,"Sys");

    duk_push_global_stash(ctx);
    duk_push_pointer(ctx,(void*)this);
    duk_put_prop_string(ctx, -2, "module");
    duk_pop(ctx);

}

js_module::~js_module()
{
    duk_destroy_heap(ctx);
    for(int i=0;i<images.length();i++)
        if(images.at(i)!=nullptr)
            delete images.at(i);

    for(int i=0;i<fonts.length();i++)
        if(fonts.at(i)!=nullptr)
            delete fonts.at(i);
}

void js_module::fatal_handler(void*, const char *msg)
{

    /* Note that 'msg' may be NULL. */
    qWarning()<<"Fatal Error : "<<msg;
    //abort();
}

void js_module::set_js(QString filename)
{
    this->filename = WindowCtr::js_modules_folder+filename;
    QFile file(this->filename);
    if(!file.open(QIODevice::ReadOnly)) {
        qWarning()<<"cant open file "<<filename;
    }

    QTextStream in(&file);
    QString code = in.readAll();
    file.close();

    if (duk_peval_string(ctx, code.toStdString().c_str()) != 0) {
        qDebug()<<"error loading js "<<filename<<" | "<<duk_safe_to_string(ctx, -1);
        duk_pop(ctx);
    }

}


void js_module::init()
{

    call_js("init");
}

void js_module::draw()
{
    call_js("draw");
}

void js_module::start()
{
    state=STATE_PLAYING;
    call_js("start");
}

void js_module::stop()
{
    call_js("stop");
}

void js_module::pause()
{
    call_js("pause");
}

void js_module::resume()
{
    call_js("resume");
}

void js_module::fadein()
{
    state=STATE_FADE;
    duk_get_global_string(ctx, "fadein");
    if(!duk_is_undefined(ctx,-1)){
        duk_call(ctx,0);
        duk_pop(ctx);
    }else{
        start();
    }
}

void js_module::call_js(const char *func_name)
{

    if(!check_function_exists(func_name))
        return;

    duk_get_global_string(ctx, func_name);
    duk_call(ctx,0);
    duk_pop(ctx);
}

bool js_module::check_function_exists(const char *func_name)
{
    duk_get_global_string(ctx, func_name);
    if(!duk_is_callable(ctx,-1)){
        qDebug()<<"cat't find function :"<<func_name;
        duk_pop(ctx);
        return false;
    }
    duk_pop(ctx);
    return true;
}

void js_module::debug_stack()
{
    duk_push_context_dump(ctx);
    qDebug()<<"stack: "<<duk_get_string(ctx,-1);
    duk_pop(ctx);
}

QString js_module::get_filename()
{
    return this->filename;
}

void js_module::on_keydown(SDL_Event &e)
{
    if(!check_function_exists("keydown"))
        return;

    duk_get_global_string(ctx, "keydown");
    duk_push_int(ctx,e.key.keysym.sym);
    duk_call(ctx,1);
    duk_pop(ctx);
}

void js_module::on_keyup(SDL_Event &e)
{
    if(!check_function_exists("keyup"))
        return;

    duk_get_global_string(ctx, "keyup");
    duk_push_int(ctx,e.key.keysym.sym);
    duk_call(ctx,1);
    duk_pop(ctx);
}

void js_module::on_textinput(SDL_Event &e)
{
    if(!check_function_exists("text_input"))
        return;

    duk_get_global_string(ctx, "text_input");
    duk_push_string(ctx,(char*)&e.text.text);
    duk_call(ctx,1);
    duk_pop(ctx);
}

void js_module::on_textedit(SDL_Event &e)
{
    if(!check_function_exists("text_edit"))
        return;

    duk_get_global_string(ctx, "text_edit");
    duk_push_string(ctx,(char*)&e.edit.text);
    duk_push_int(ctx,e.edit.start);
    duk_push_int(ctx,e.edit.length);
    duk_call(ctx,3);
    duk_pop(ctx);
}

js_module *js_module::get_js_module(duk_context *ctx)
{
    duk_push_global_stash(ctx);
    duk_get_prop_string(ctx, -1, "module");
    js_module* ret = (js_module*)duk_to_pointer(ctx,-1);
    duk_pop_2(ctx);
    return ret;
}

duk_ret_t js_module::Image_constructor(duk_context *ctx)
{
    if (!duk_is_constructor_call(ctx)) {
        return DUK_RET_TYPE_ERROR;
    }
    js_module *me = get_js_module(ctx);

    duk_push_this(ctx);
    /* Set this.name to name. */
    duk_dup(ctx, 0);  /* -> stack: [ name this name ] */
    duk_put_prop_string(ctx, -2, "src");  /* -> stack: [ name this ] */

    Texture* t;
    t=new Texture();
    t->load(me->renderer,WindowCtr::js_modules_folder+duk_get_string(ctx,0));

    if(me->images_deleted_ids.length()==0){
        duk_push_int(ctx,me->images.length());
        me->images.append(t);
    }else{
        int id = me->images_deleted_ids.takeLast();
        duk_push_int(ctx,id);
        me->images[id] = t;
    }

    duk_put_prop_string(ctx, -2, "img_id");

    /* Return undefined: default instance will be used. */
    return 0;
}

duk_ret_t js_module::Image_get_width(duk_context *ctx)
{
    js_module *me = get_js_module(ctx);
    Texture *texture;

    duk_push_this(ctx);
    duk_get_prop_string(ctx,-1,"img_id");
    texture = me->images.at(duk_get_int(ctx,-1));
    duk_pop_2(ctx);
    duk_push_int(ctx,texture->getOrigW());
    return 1;
}

duk_ret_t js_module::Image_get_height(duk_context *ctx)
{
    js_module *me = get_js_module(ctx);
    Texture *texture;

    duk_push_this(ctx);
    duk_get_prop_string(ctx,-1,"img_id");
    texture = me->images.at(duk_get_int(ctx,-1));
    duk_pop_2(ctx);
    duk_push_int(ctx,texture->getOrigH());
    return 1;
}

duk_ret_t js_module::Image_rotate(duk_context *ctx)
{
    js_module* me = get_js_module(ctx);
    Texture* tex;

    duk_push_this(ctx);
    duk_get_prop_string(ctx,-1,"img_id");
    tex = me->images.at(duk_get_int(ctx,-1));
    duk_pop_2(ctx);

    tex->rotate((double)duk_get_number(ctx,0));

    if(!duk_is_undefined(ctx,1))
        tex->center.x = duk_get_int(ctx,1);

    if(!duk_is_undefined(ctx,2))
        tex->center.y = duk_get_int(ctx,2);

    return 0;
}

duk_ret_t js_module::Image_set_transparency(duk_context *ctx)
{
    js_module* me = get_js_module(ctx);
    Texture* tex;

    duk_push_this(ctx);
    duk_get_prop_string(ctx,-1,"img_id");
    tex = me->images.at(duk_get_int(ctx,-1));
    duk_pop_2(ctx);

    tex->setAlpha(duk_get_number(ctx,0)*255);
    return 0;
}

duk_ret_t js_module::Image_set_clip(duk_context *ctx)
{
    js_module* me = get_js_module(ctx);
    Texture* tex;

    duk_push_this(ctx);
    duk_get_prop_string(ctx,-1,"img_id");
    tex = me->images.at(duk_get_int(ctx,-1));
    duk_pop_2(ctx);

    tex->clip_rect.x = duk_get_int(ctx,0);
    tex->clip_rect.y = duk_get_int(ctx,1);
    tex->clip_rect.w = duk_get_int(ctx,2);
    tex->clip_rect.h = duk_get_int(ctx,3);
    return 0;
}

duk_ret_t js_module::Image_set_flip(duk_context *ctx)
{
    js_module* me = get_js_module(ctx);
    Texture* tex;

    duk_push_this(ctx);
    duk_get_prop_string(ctx,-1,"img_id");
    tex = me->images.at(duk_get_int(ctx,-1));
    duk_pop_2(ctx);

    bool h = duk_get_boolean(ctx,0);
    bool v = duk_get_boolean(ctx,1);

    tex->setFlip(h,v);

    return 0;
}

duk_ret_t js_module::Image_free(duk_context *ctx)
{
    js_module* me = get_js_module(ctx);
    Texture* tex;
    int id;

    duk_push_this(ctx);
    duk_get_prop_string(ctx,-1,"img_id");
    id = duk_get_int(ctx,-1);
    tex = me->images.at(id);
    duk_pop_2(ctx);
    //qDebug()<<"id:"<<id;
    delete tex;
    tex=nullptr;
    me->images_deleted_ids.push_back(id);
    return 0;
}

duk_ret_t js_module::Font_constructor(duk_context *ctx)
{
    if (!duk_is_constructor_call(ctx)) {
        return DUK_RET_TYPE_ERROR;
    }
    js_module *me = get_js_module(ctx);

    duk_push_this(ctx);
    /* Set this.name to name. */
    duk_dup(ctx, 0);  /* -> stack: [ name this name ] */
    duk_put_prop_string(ctx, -2, "src");  /* -> stack: [ name this ] */

    Font* font;
    font = new Font();
    font->load(me->renderer,WindowCtr::js_modules_folder+duk_get_string(ctx,0));
    int font_id = me->fonts.length();
    me->fonts.push_back(font);

    duk_push_int(ctx,font_id);
    duk_put_prop_string(ctx, -2, "font_id");


    return 0;
}

duk_ret_t js_module::Font_draw_text(duk_context *ctx)
{
    js_module *me = get_js_module(ctx);
    Font* font;

    duk_push_this(ctx);
    duk_get_prop_string(ctx,-1,"font_id");

    int id = duk_get_int(ctx,-1);
    duk_pop_2(ctx);
    QString text = duk_get_string(ctx,0);
    int x = duk_get_int(ctx,1);
    int y = duk_get_int(ctx,2);

    font = me->fonts.at(id);
    font->drawText(text,x,y);

    return 0;
}

duk_ret_t js_module::Font_set_color(duk_context *ctx)
{
    js_module *me = get_js_module(ctx);
    Font* font;

    duk_push_this(ctx);
    duk_get_prop_string(ctx,-1,"font_id");

    int id = duk_get_int(ctx,-1);
    duk_pop_2(ctx);
    font=me->fonts.at(id);

    int r = duk_get_int(ctx,0);
    int g = duk_get_int(ctx,1);
    int b = duk_get_int(ctx,2);
    int a = duk_get_int(ctx,3);

    SDL_Color color;
    color.r=(Uint8)r;
    color.g=(Uint8)g;
    color.b=(Uint8)b;
    color.a=(Uint8)a;

    font->set_color(color);

    return 0;
}

duk_ret_t js_module::Font_set_size(duk_context *ctx)
{
    js_module *me = get_js_module(ctx);
    Font* font;

    duk_push_this(ctx);
    duk_get_prop_string(ctx,-1,"font_id");

    int id = duk_get_int(ctx,-1);
    duk_pop_2(ctx);
    font=me->fonts.at(id);

    font->set_size(duk_get_int(ctx,0));

    return 0;
}

duk_ret_t js_module::Screen_draw(duk_context *ctx)
{

    js_module *me = get_js_module(ctx);

    Texture *texture;

    duk_get_prop_string(ctx,0,"img_id");

    if(duk_is_undefined(ctx,-1)){
        qDebug()<<"drawing not a image";
        return 0;
    }

    int id = duk_get_int(ctx,-1);
    duk_pop(ctx);

    texture = me->images.at(id);

    if(!duk_is_undefined(ctx,1))
        texture->position.x = duk_get_int(ctx,1);
    if(!duk_is_undefined(ctx,2))
        texture->position.y = duk_get_int(ctx,2);

    //ширина и высота
    if(!duk_is_undefined(ctx,3))
        texture->position.w = duk_get_int(ctx,3);

    if(!duk_is_undefined(ctx,4))
        texture->position.h = duk_get_int(ctx,4);


    texture->draw();
    return 0;
}

duk_ret_t js_module::Screen_get_width(duk_context *ctx)
{
    js_module* me = get_js_module(ctx);
    duk_push_int(ctx,WindowCtr::get_width(me->window_id));
    return 1;
}

duk_ret_t js_module::Screen_get_height(duk_context *ctx)
{
    js_module* me = get_js_module(ctx);
    duk_push_int(ctx,WindowCtr::get_height(me->window_id));
    return 1;
}

duk_ret_t js_module::Screen_set_color(duk_context *ctx)
{
    js_module *me = get_js_module(ctx);
    Uint8 r = (Uint8)duk_get_int(ctx,0);
    Uint8 g = (Uint8)duk_get_int(ctx,1);
    Uint8 b = (Uint8)duk_get_int(ctx,2);
    Uint8 a = (Uint8)duk_get_int(ctx,3);
    SDL_SetRenderDrawColor(me->renderer,r,g,b,a);

    return 0;
}

duk_ret_t js_module::Screen_draw_rect(duk_context *ctx)
{
     js_module *me = get_js_module(ctx);
    SDL_Rect rect;
    rect.x = duk_get_int(ctx,0);
    rect.y = duk_get_int(ctx,1);
    rect.w = duk_get_int(ctx,2);
    rect.h = duk_get_int(ctx,3);

    SDL_RenderFillRect(me->renderer,&rect);
    return 0;
}

//Системный вызов
duk_ret_t js_module::Sys_system(duk_context *ctx)
{
    QString command = QString(duk_get_string(ctx,0));

    QStringList args;

    int n = duk_get_length(ctx,1);
    for (int i = 0; i < n; i++) {
        duk_get_prop_index(ctx, 1, i);

        args.append(QString(duk_get_string(ctx,-1)));

        duk_pop(ctx);
    }

    QProcess proc;

    proc.setWorkingDirectory(WindowCtr::js_modules_folder);
    proc.setArguments(args);
    proc.setProgram(command);
    proc.start();

    proc.waitForFinished();

    QString out = proc.readAllStandardOutput();
    QString error = proc.readAllStandardError();
    int status = proc.exitCode();

    duk_push_object(ctx);
    duk_push_string(ctx,out.toStdString().c_str());
    duk_put_prop_string(ctx,-2,"output");
    duk_push_string(ctx,error.toStdString().c_str());
    duk_put_prop_string(ctx,-2,"error");
    duk_push_int(ctx,status);
    duk_put_prop_string(ctx,-2,"exit_status");

    return 1;
}

duk_ret_t js_module::Sys_api(duk_context *ctx)
{
    QByteArray query = duk_json_encode(ctx,0);
    QByteArray out;

    API::process(out,query);

    duk_push_string(ctx,out.toStdString().c_str());
    duk_json_decode(ctx,-1);

    return 1;
}

duk_ret_t js_module::Sys_get_state(duk_context *ctx)
{
    js_module* me = get_js_module(ctx);
    duk_push_int(ctx,me->getState());
    return 1;
}

duk_ret_t js_module::Sys_set_state(duk_context *ctx)
{
    js_module* me = get_js_module(ctx);
    int state = duk_get_int(ctx,0);
    me->state=state;
    return 0;
}

duk_ret_t js_module::Sys_start_text_input_mode(duk_context *ctx)
{
    js_module* me = get_js_module(ctx);
    WindowCtr::start_text_input_mode(me->window_id,me);
    return 0;
}

duk_ret_t js_module::Sys_stop_text_input_mode(duk_context *ctx)
{
    js_module* me = get_js_module(ctx);
    WindowCtr::stop_text_input_mode(me->window_id);
    return 0;
}
