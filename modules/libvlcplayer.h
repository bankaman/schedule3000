#ifndef LIBVLCPLAYER_H
#define LIBVLCPLAYER_H

#include "modulegeneric.h"
#include <vlc/vlc.h>

class LibvlcPlayer : public ModuleGeneric
{
public:
    LibvlcPlayer();
    ~LibvlcPlayer();

    void setParams(QHash<QString, QVariant>);

protected:
    static libvlc_instance_t* vlc_instance;
    static int vlc_ussage_counter;
    void static create_vlc_instance();
    void static destroy_vlc_instance();

    bool vlc_parce_media(libvlc_media_t* p_md);

    int volume;

};

#endif // LIBVLCPLAYER_H
