#include "logo_anim.h"
#include "windowctr.h"

logo_anim::logo_anim()
{
    tokiko_y=0;
    start_time = QTime::currentTime();
}

logo_anim::~logo_anim()
{
    clear();
}

void logo_anim::init()
{

}

void logo_anim::draw()
{
    if(state==STATE_OVER)
        return;

    if(frame>=cur->length())
        frame=0;
    vir_draw(logo_bg,0,0);
    vir_draw(logo_items,0,0);
    vir_draw(*cur->at(frame),tokiko_x,tokiko_y);
    vir_draw(logo_text,0,0);

    delay_t++;
    if(delay_t>delay){
        delay_t=0;
        frame++;
    }

    if(utmb && frame>=cur->length()){
        cur=&loop;
        frame=0;
        utmb=false;
    }

    if(st==0){
        angle++;
        if(angle>360)
            angle=0;

        //tokiko_y+=(tokiko_target_y-tokiko_y)/50;
        tokiko_y+=(tokiko_target_y-tokiko_y)/25;
        tokiko_target_y=130+sin(angle*3.14159265/180)*50;

        if(tokiko_y<200)
            st++;
    }


    if(st==1){
        cur=&utm;
        frame=0;
        delay=10;
        logo_bg.fadein(5);
        logo_text.fadein(5);
        logo_items.fadein(5);
        st++;
        utmb=true;
        state=STATE_PLAYING;
        wink_frame=0;
        wink_mode=false;
        clock_mode=false;
    }

    if(st==2){
        if(!clock_mode){
            if(!utmb && wink_mode){
                vir_draw(*loop_wink.at(wink_frame),tokiko_x,tokiko_y);
                wink_delay++;
                if(wink_delay>5){
                    wink_frame++;
                    wink_delay=0;
                }
                if(wink_frame>=loop_wink.length()){
                    wink_frame=0;
                    wink_mode=false;
                }
            }


            if(!wink_mode){
                wink_mode=((rand()%10000)<10) ? true : false;
            }

            if(abs(QTime::currentTime().msecsSinceStartOfDay()-start_time.msecsSinceStartOfDay())>600000){ //(600000)10 min
                clock_glitch=true;
                clock_mode=true;
                clock_glitch_in=true;
                start_time=QTime::currentTime();
            }

            angle++;
            if(angle>360)
                angle=0;

            tokiko_y+=(tokiko_target_y-tokiko_y)/100;
            tokiko_target_y=130+sin(angle*3.14159265/180)*50;
        }else{
            if(clock_glitch){
                cur=&glitch;
                tokiko_x=0;
                tokiko_y=0;
                delay=0;
                if(abs(QTime::currentTime().msecsSinceStartOfDay()-start_time.msecsSinceStartOfDay())>2000){
                    if(clock_glitch_in)
                        cur=&clock_tokiko;
                    else{
                        cur=&loop;
                        clock_mode=false;
                        tokiko_x=135;
                        tokiko_y=tokiko_target_y+130;
                        delay=10;
                    }
                    clock_glitch=false;
                }
            }else{
                //clock mode
                angle++;
                if(angle>360)
                    angle=0;

                tokiko_y+=(tokiko_target_y-tokiko_y)/100;
                tokiko_target_y=sin(angle*3.14159265/180)*50;
                QTime time = QTime::currentTime();
                int h = time.hour();
                int m = time.minute();
                vir_draw(*clock_numbers.at(h/10),tokiko_x+400,tokiko_y+526);
                vir_draw(*clock_numbers.at(h%10),tokiko_x+676,tokiko_y+516);
                vir_draw(*clock_numbers.at(m/10),tokiko_x+1043,tokiko_y+516);
                vir_draw(*clock_numbers.at(m%10),tokiko_x+1328,tokiko_y+516);

                vir_draw(clocks,tokiko_x,tokiko_y);

                if(abs(QTime::currentTime().msecsSinceStartOfDay()-start_time.msecsSinceStartOfDay())>600000){
                    clock_glitch=true;
                    clock_glitch_in=false;
                    start_time=QTime::currentTime();
                }
            }
        }
    }

    if(st==3){
        logo_bg.fadeout(5);
        logo_text.fadeout(5);
        logo_items.fadeout(5);
        st++;
        speedy=0;
        cur=&up;
    }

    if(st==4){
        speedy-=2;
        tokiko_y+=speedy;
        if(tokiko_y<-300-cur->at(0)->getOrigH()){
            st++;
        }
    }

    if(st==5){
        if(logo_bg.getAlpha()==0){
            state=STATE_OVER;
            clear();
        }
    }
    //qDebug()<<"frame:"<<frame;
}

void logo_anim::fadein()
{
    state=STATE_FADE;

    logo_bg.load_from_resources(renderer,":/resources/logo_anim/logo_bg.png");
    logo_text.load_from_resources(renderer,":/resources/logo_anim/logo_text.png");
    logo_items.load_from_resources(renderer,":/resources/logo_anim/logo_items.png");
    logo_text.setAlpha(0);
    logo_items.setAlpha(0);

    Texture *t;
    for(int i=0;i<5;i++){
        t = new Texture();
        t->load_from_resources(renderer,":/resources/logo_anim/up_000"+QString::number(i)+".png");
        up.append(t);
    }

    for(int i=0;i<3;i++){
        t = new Texture();
        t->load_from_resources(renderer,":/resources/logo_anim/uptl_000"+QString::number(i)+".png");
        utm.append(t);
    }

    for(int i=0;i<5;i++){
        t = new Texture();
        t->load_from_resources(renderer,":/resources/logo_anim/ml_000"+QString::number(i)+".png");
        loop.append(t);
    }

    for(int i=0;i<2;i++){
        t = new Texture();
        t->load_from_resources(renderer,":/resources/logo_anim/ml_wink_000"+QString::number(i)+".png");
        loop_wink.append(t);
    }

    for(int i=0;i<2;i++){
        t = new Texture();
        t->load_from_resources(renderer,":/resources/logo_anim/glitch"+QString::number(i)+".jpg");
        glitch.append(t);
    }


    t = new Texture();
    t->load_from_resources(renderer,":/resources/logo_anim/clocks.png");
    clock_tokiko.append(t);

    clocks.load_from_resources(renderer,":/resources/logo_anim/clocks_tokiko.png");

    for(int i=0;i<10;i++){
        t = new Texture();
        t->load_from_resources(renderer,":/resources/logo_anim/numbers/"+QString::number(i)+".png");
        clock_numbers.append(t);
    }


    cur = &up;
    tokiko_x=135;
    tokiko_y=2000;
    tokiko_target_y=130;
    frame=0;
    delay=3;
    delay_t=0;
    dir=-0.02;
    st=0;
    angle=0;
    utmb=false;
    logo_bg.setAlpha(0);

}

void logo_anim::fadeout()
{
    state=STATE_FADE;
    st=3;
    qDebug()<<"placeholder fadeout";
}

void logo_anim::vir_draw(Texture &t, float x, float y)
{
    int w = WindowCtr::get_width(window_id);
    int h = WindowCtr::get_height(window_id);

    int bw=1920;
    int bh=1920*h/w;

    if(bh<1080){
        bh=1080;
        bw=1080*w/h;
    }

    int sy = (bh-1080)/2;
    int sx = (bw-1920)/2;

    t.position.x = (int)(w*x/bw)+(sx*w/bw);
    t.position.y = (int)(h*y/bh)+(sy*h/bh);
    t.position.w = t.getOrigW()*w/bw;
    t.position.h = t.getOrigH()*h/bh;

    t.draw();
}

void logo_anim::vir_draw(Texture &t, float x, float y, float ow, float oh)
{
    int w = WindowCtr::get_width(window_id);
    int h = WindowCtr::get_height(window_id);

    int bw=1920;
    int bh=1920*h/w;

    if(bh<1080){
        bh=1080;
        bw=1080*w/h;
    }

    int sy = (bh-1080)/2;
    int sx = (bw-1920)/2;

    //
    t.position.x = (int)(w*x/bw)+(sx*w/bw);
    t.position.y = (int)(h*y/bh)+(sy*h/bh);
    t.position.w = ow*w/bw;
    t.position.h = oh*h/bh;

    t.draw();
}

void logo_anim::clear()
{
    for(int i=0;i<up.length();i++)
        delete up.at(i);
    up.clear();
    for(int i=0;i<utm.length();i++)
        delete utm.at(i);
    utm.clear();
    for(int i=0;i<loop.length();i++)
        delete loop.at(i);
    loop.clear();
    for(int i=0;i<loop_wink.length();i++)
        delete loop_wink.at(i);
    loop_wink.clear();
    for(int i=0;i<glitch.length();i++)
        delete glitch.at(i);
    glitch.clear();

    for(int i=0;i<clock_tokiko.length();i++)
        delete clock_tokiko.at(i);
    clock_tokiko.clear();

    for(int i=0;i<clock_numbers.length();i++)
        delete clock_numbers.at(i);
    clock_numbers.clear();
}
