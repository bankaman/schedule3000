
/*
 * Schedule3000 clock overlay module
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include "clockoverlay.h"

ClockOverlay::ClockOverlay()
{
    x=60;
    y=50;
}

ClockOverlay::~ClockOverlay()
{
}


void ClockOverlay::setFileList(QStringList)
{
}

void ClockOverlay::init()
{
    numbers.load_from_resources(renderer,":/resources/clock/numbers.png");
    points.load_from_resources(renderer,":/resources/clock/points.png");
    bg.load_from_resources(renderer,":/resources/clock/bg.png");
    paused = false;
}

void ClockOverlay::process()
{
    //обновление часов
    if(!paused){
        time = QTime::currentTime();
        m = time.minute();
        h = time.hour();
        h2 = h % 10;
        h1 = h / 10;
        m2 = m % 10;
        m1 = m / 10;

        //мигание точек
        if(points.getAlpha()==255)
            points.fadeout(5);
        if(points.getAlpha()==0)
            points.fadein(5);
    }

}

void ClockOverlay::draw()
{
    bg.position.x = x-65;
    bg.position.y = y-60;
    bg.draw();

    numbers.position.x=x;
    numbers.position.y=y;

    //Час
    numbers.setClipRect(70*h1,0,70,99);
    numbers.position.w=70;
    numbers.draw();
    numbers.setClipRect(70*h2,0,70,99);
    numbers.position.x+=70;
    numbers.draw();

    //минута
    numbers.setClipRect(70*m1,0,70,99);
    numbers.position.x+=110;
    numbers.draw();
    numbers.setClipRect(70*m2,0,70,99);
    numbers.position.x+=70;
    numbers.draw();

    //точки
    points.position.x=x+70*2+10;
    points.position.y=y-15;
    points.draw();
}

void ClockOverlay::nextElement()
{
}

void ClockOverlay::prevElement()
{
}

void ClockOverlay::start()
{
}

void ClockOverlay::stop()
{
}

void ClockOverlay::pause()
{
    paused=true;
}

void ClockOverlay::resume()
{
    paused=false;
}

void ClockOverlay::setParams(QHash<QString, QVariant> params)
{
    ModuleGeneric::setParams(params);
    x = params.value("x",60).toInt();
    y = params.value("y",50).toInt();
}
