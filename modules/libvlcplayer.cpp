#include "libvlcplayer.h"

libvlc_instance_t* LibvlcPlayer::vlc_instance = nullptr;
int LibvlcPlayer::vlc_ussage_counter=0;

LibvlcPlayer::LibvlcPlayer()
{
    vlc_instance=nullptr;
    create_vlc_instance();
    vlc_ussage_counter++;
    volume = 100;
}

LibvlcPlayer::~LibvlcPlayer()
{
    vlc_ussage_counter--;
    if(vlc_ussage_counter<=0)
        destroy_vlc_instance();
}

void LibvlcPlayer::setParams(QHash<QString, QVariant> params)
{
    ModuleGeneric::setParams(params);
    volume = params.value("volume",100).toInt();
}


void LibvlcPlayer::create_vlc_instance()
{
    if(vlc_instance!=nullptr)
        return;

    qDebug()<<"Creating vlc instance!";
    //Инициализация LibVLC
    char const *vlc_argv[] = {
        "--no-xlib", // Don't use Xlib.
        /*"--audio-filter=compressor",
        "--compressor-rms-peak=1.0",
        "--compressor-attack=1.5",
        "--compressor-release=2.0",
        "--compressor-threshold=-20.0",
        "--compressor-ratio=20.0",
        "--compressor-knee=1.0",
        "--compressor-makeup-gain=20.0"*/
    };
    int vlc_argc = sizeof(vlc_argv) / sizeof(*vlc_argv);

    vlc_instance = libvlc_new (vlc_argc, vlc_argv);
    if(vlc_instance == nullptr){
        qWarning()<<"Cannot create vlc instance"<<endl;
    }

}

void LibvlcPlayer::destroy_vlc_instance()
{
    if(vlc_instance!=nullptr){
        qDebug()<<"Destroying vlc_instance!";
        libvlc_release (vlc_instance);
        vlc_instance=nullptr;
    }
}

bool LibvlcPlayer::vlc_parce_media(libvlc_media_t *p_md)
{
    #if LIBVLC_VERSION_MAJOR<3
        libvlc_media_parse(p_md);
        return libvlc_media_is_parsed(p_md);
    #else
        libvlc_media_parse(p_md);
        return libvlc_media_is_parsed(p_md);
        /*libvlc_media_parse(p_md);
        return true;*/
        /*int l = libvlc_media_parse_with_options(p_md,libvlc_media_parse_local,-1);

        qDebug()<<"l:"<<l<<libvlc_media_parsed_status_done<<libvlc_media_is_parsed(p_md);
        return libvlc_media_is_parsed(p_md);*/
#endif
}
