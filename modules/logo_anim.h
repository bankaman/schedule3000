#ifndef LOGO_ANIM_H
#define LOGO_ANIM_H

#include <modulegeneric.h>
#include <texture.h>
#include <QTime>



class logo_anim : public ModuleGeneric
{
public:
    logo_anim();
    ~logo_anim();

    void init();
    void draw();
    void fadein();
    void fadeout();

private:
    QVector<Texture*> up;
    QVector<Texture*> utm;
    QVector<Texture*> loop;
    QVector<Texture*> loop_wink;
    QVector<Texture*> glitch;
    QVector<Texture*> clock_tokiko;
    QVector<Texture*> clock_numbers;
    QVector<Texture*>* cur;

    Texture logo_bg;
    Texture logo_text;
    Texture logo_items;

    Texture clocks;

    float tokiko_x;
    float tokiko_y;
    float tokiko_target_y;
    float speedy;
    float dir;
    int frame;
    int delay;
    int delay_t;
    int st=0;
    int angle;

    int wink_frame;
    int wink_delay;
    bool wink_mode;


    QTime start_time;

    bool clock_mode;
    bool clock_glitch;
    bool clock_glitch_in;

    bool utmb;

    void vir_draw(Texture &t,float x, float y);
    void vir_draw(Texture &t, float x, float y, float ow, float oh);

    void clear();
};

#endif // LOGO_ANIM_H
