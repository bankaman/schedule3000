#include "dublicator.h"

Dublicator::Dublicator()
{
    module = NULL;
    event_id = -1;
    orig_window_id = -1;
    target = NULL;
    copy = NULL;
    buffer = NULL;
    qDebug()<<"creating dublicator";
}

Dublicator::~Dublicator()
{
    destroy();
}

void Dublicator::init()
{

}

void Dublicator::setParams(QHash<QString, QVariant> params)
{
    ModuleGeneric::setParams(params);
    event_id = params.value("event_id","-1").toInt();
    orig_window_id = params.value("orig_window_id","-1").toInt();

    //qDebug()<<"dublicator event_id";
}

void Dublicator::process()
{
    if(module==NULL){
        qDebug()<<"processing dublicator "<<event_id<<"|"<<orig_window_id;
        module=WindowCtr::get_module(orig_window_id,event_id);
        if(module==NULL)
            return;
        setup();
    }
}

void Dublicator::draw()
{
    updated = false;
    process();
    if(module==NULL)
        return;

    if(module->module_name == "videoplayer")
        return process_videoplayer();

    if(module->module_name == "dublicator")
        return process_dublicator();

    SDL_SetRenderTarget(module->renderer, target);
    module->draw();
    SDL_RenderReadPixels(module->renderer, NULL, SDL_GetWindowPixelFormat(WindowCtr::get_window(window_id)), buffer->pixels, buffer->pitch);
    SDL_SetRenderTarget(module->renderer, NULL);
    SDL_UpdateTexture(copy,NULL,buffer->pixels, buffer->pitch);
    SDL_RenderCopy(renderer,copy,NULL,NULL);
    updated = true;
}

void Dublicator::setup()
{
    destroy();

    target = SDL_CreateTexture(
                module->renderer,
                SDL_GetWindowPixelFormat(WindowCtr::get_window(window_id)),
                SDL_TEXTUREACCESS_TARGET,
                WindowCtr::get_width(window_id),
                WindowCtr::get_height(window_id)
            );
    copy = SDL_CreateTexture(
                renderer,
                SDL_GetWindowPixelFormat(WindowCtr::get_window(window_id)),
                SDL_TEXTUREACCESS_STREAMING,
                WindowCtr::get_width(window_id),
                WindowCtr::get_height(window_id)
                );

    buffer = SDL_CreateRGBSurfaceWithFormat(
                0,
                WindowCtr::get_width(window_id),
                WindowCtr::get_width(window_id),
                32,
                SDL_GetWindowPixelFormat(WindowCtr::get_window(window_id))
             );
}

void Dublicator::destroy()
{
    if(target!=NULL){
        SDL_DestroyTexture(target);
    }
    if(copy!=NULL){
        SDL_DestroyTexture(copy);
    }
    if(buffer!=NULL){
        SDL_FreeSurface(buffer);
    }
}

void Dublicator::process_videoplayer()
{
    VlcVideoPlayer *m = (VlcVideoPlayer*)module;

    if(m->tone_updated){
        //TODO: need to be more optimized
        SDL_SetRenderTarget(module->renderer, target);
        module->draw();
        SDL_RenderReadPixels(module->renderer, NULL, SDL_GetWindowPixelFormat(WindowCtr::get_window(window_id)), buffer->pixels, buffer->pitch);
        SDL_SetRenderTarget(module->renderer, NULL);
        SDL_UpdateTexture(copy,NULL,buffer->pixels, buffer->pitch);
        SDL_RenderCopy(renderer,copy,NULL,NULL);
        updated=true;
    }
}

void Dublicator::process_dublicator()
{
    Dublicator *m = (Dublicator*) module;

    if(m->updated){
        SDL_UpdateTexture(copy,NULL,m->buffer->pixels,m->buffer->pitch);
        SDL_RenderCopy(renderer,copy,NULL,NULL);
        updated=true;
    }
}


