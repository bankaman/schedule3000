
/*
 * Schedule3000 image viewer module
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include "../modulegeneric.h"
#include "../texture.h"
#include "../glob.h"
#include <QMutex>

class ImageViewer : ModuleGeneric
{
struct ImgEl{
    QString filename;
    Texture* image;
    bool isBuffered;
};
public:
    ImageViewer();
    ~ImageViewer();

    // ModuleGeneric interface
    void setFileList(QStringList files);
    void init();
    void process();
    void draw();
    void nextElement();
    void prevElement();
    void start();
    void stop();
    void fadein();
    void fadeout();
    void pause();
    void resume();

    void setParams(QHash<QString, QVariant> params);
    int getDuration();

    static Uint32 change_image_callback (Uint32 interval, void* param);

private:
    QStringList images_list;

    QVector<ImgEl*> images;
    int cur_img;

    void resizeImg(Texture* image);
    void updateBuffer();
    void clear_images();

    void draw_no_images();

    void randomize_images();

    int SCR_H,SCR_W;
    SDL_TimerID change_image_timer;

    int image_duration; // длительность показа одного изображения
    int fade_duration;  // длительность эффекта исчезновения

    bool paused;

    bool in_update;

    Texture no_image;
    QMutex mutex;
};

#endif // IMAGEVIEWER_H
