#ifndef VLCMUSICPLAYER_H
#define VLCMUSICPLAYER_H

/*
 * Schedule3000 music player module
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include "libvlcplayer.h"
#include "../glob.h"

#include <QDebug>

#include <vlc/vlc.h>

class VlcMusicPlayer : public LibvlcPlayer
{
public:
    VlcMusicPlayer();
    ~VlcMusicPlayer();

    // ModuleGeneric interface
    void setFileList(QStringList files);
    void init();
    void process();
    void draw();
    void nextElement();
    void prevElement();
    void pause();
    void resume();
    void start();
    void stop();
    void fadein();
    void fadeout();
    void seek(uint64_t position);

private:


    libvlc_media_player_t *player;
    libvlc_media_t *media;

    QStringList playlist;
    int cur;
    libvlc_state_t player_state;

    int duration;

    bool loadFile();
    void freeMedia();


    int get_file_duration(int id);
    void randomize_plylist();

    // ModuleGeneric interface
public:
    int getDuration();
    void setParams(QHash<QString, QVariant>);
};

#endif // VLCMUSICPLAYER_H
