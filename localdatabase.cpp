/*
 * local database class
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include "localdatabase.h"
#include "localdatabase.h"
#include "windowctr.h"

bool LocalDatabase::first_init=true;
int LocalDatabase::next_id=0;

LocalDatabase::LocalDatabase()
{
    //qDebug()<<"DB_id Load:"<<next_id;
    last_error = "";
    my_id=next_id;
    while(QSqlDatabase::contains(QString("localdatabase")+QString::number(my_id))){
        next_id++;
        my_id=next_id;
    }

    if(first_init){
        //qDebug()<<"FIRST RUN!!!";

        db=QSqlDatabase::addDatabase("QSQLITE",QString("localdatabase")+QString::number(next_id));
        db.setDatabaseName("database.db3");
        if(!db.open()){
            qDebug()<<"ERROR:"<<db.lastError();
        }

        first_init=false;
        QSqlQuery query(db);


        // timeline table
        if(!table_exsist("timeline")){
            query_execute(query,
                       "create table 'timeline' ("
                           "window_id int default 0,"
                           "date_start datetime,"
                           "duration int,"
                           "is_played int default 0,"
                           "is_skipped int default 0, "
                           "draw_order int DEFAULT 0, "
                           "module	TEXT, "
                           "mode	TEXT, "
                           "params text, "
                           "priority int DEFAULT 0, "
                           "playlist_id int "
                       ");");
            qDebug()<<"timeline table created!";
        }
        // playlist table
        if(!table_exsist("playlist")){
            query_execute(query,
                          "CREATE TABLE `playlist` ("
                            "`name`	TEXT NOT NULL UNIQUE, "
                            "`type`	INTEGER, "
                            "`files` TEXT, "
                            "`duration` INTEGER, "
                            "`is_random` INTEGER DEFAULT 0"
                          ");"
                          );
        }
        //file table
        if(!table_exsist("file")){
            query_execute(query,
                "CREATE TABLE `file` ( "
                    "`name`	TEXT UNIQUE NOT NULL, "
                    "`byte_length`	INTEGER NOT NULL, "
                    "`type`	INTEGER, "
                    "`ext`	TEXT, "
                    "`uploaded`	INTEGER "
                ");"
            );
        }
        //template
        if(!table_exsist("template")){
            query_execute(query,
                      "CREATE TABLE `template` ( "
                          "`name`	TEXT UNIQUE, "
                          "`window_id`	INTEGER, "
                          "`module`	TEXT, "
                          "`mode`	TEXT, "
                          "`priority`	INTEGER, "
                          "`draw_order`	INTEGER, "
                          "`params` text, "
                          "`playlist_name` text "
                      ");"
            );
        }
    }else{
        //qDebug()<<"NOT FIRST RUN!!!";
        db=QSqlDatabase::addDatabase("QSQLITE",QString("localdatabase")+QString::number(my_id));
        db.setDatabaseName("database.db3");
        db.open();
    }

    next_id++;
}

LocalDatabase::~LocalDatabase()
{
    //qDebug()<<"DB_id Unload:"<<my_id;
    db.close();
    db = QSqlDatabase();
    QSqlDatabase::removeDatabase("localdatabase"+QString::number(my_id));
}

/*void LocalDatabase::get_timeline(int window_id, QVector<ScheduleElement *> *timeline)
{
    QSqlQuery query(db);
    if(!query_execute(query,"select "
                        "tl.rowid, "
                        "strftime('%s',tl.date_start,'utc'), "
                        "tl.params, "
                        "tl.is_played, "
                        "tl.duration as _duration, "
                        "tl.module, "
                        "tl.mode, "
                        "tl.priority, "
                        "tl.window_id, "
                        "tl.draw_order, "
                        "tl.playlist_id, "
                        "pl.duration as duration "
                      "from timeline tl "
                      "join playlist pl on pl.rowid = tl.playlist_id "
                      "where is_played = 0 "
                      "and is_skipped = 0 "
                      "and window_id = "+QString::number(window_id)+" "
                      "order by draw_order"))
        return;
    ScheduleElement* el;
    QJsonDocument doc;

    QVariantHash params;
    QVector<int> no_duration;

    while(query.next()){
        doc = doc.fromJson(query.value(2).toByteArray());
        if( (!query.value(2).isNull()) && (doc.isNull())){
            qDebug()<<"error in json: "<<query.value(2).toString();
            return;
        }

        int playlist_id = query.value("playlist_id").toInt();

        params.clear();
        QString module = query.value(5).toString();
        QString mode = query.value(6).toString();

        int rowid = query.value("rowid").toInt();
        int start_time = query.value(1).toInt();
        int priority = query.value(7).toInt();
        int draw_order = query.value(9).toInt();
        int duration = query.value(4).toInt();

        params.unite(doc.object().toVariantHash());

        el = new ScheduleElement(
                            rowid,
                            playlist_id,
                            start_time,
                            priority,
                            draw_order,
                            window_id,
                            module,
                            mode,
                            params);
        if(el->is_error()){
            delete_event(rowid);
            continue;
        }
        timeline->append(el);
        if(duration<=0){
            no_duration.append(timeline->length()-1);
        }
    }

    for(int i = 0; i<no_duration.length();i++){
        el = timeline->at(no_duration.at(i));
        int duration = el->getDuration();
        set_duration(el->rowid, duration);
    }
}
*/
void LocalDatabase::set_as_played(int rowid)
{
    QSqlQuery query(db);
    query_execute(query,"update timeline set is_played=1 where rowid="+QString::number(rowid));
}

void LocalDatabase::set_as_skipped(int rowid)
{
    QSqlQuery query(db);
    query_execute(query,"update timeline set is_skipped=1 where rowid="+QString::number(rowid));
}

void LocalDatabase::set_duration(int rowid, int duration)
{
    QSqlQuery query(db);
    query_execute(query,"select playlist_id from timeline where rowid="+QString::number(rowid));
    if(query.size()<=0)
        return;
    query.first();
    int playlist_id = query.value("playlist_id").toInt();
    query_execute(query,"update playlist set duration="+QString::number(duration)+" where rowid="+QString::number(playlist_id));

    query_execute(query,"update timeline set duration="+QString::number(duration)+" where rowid="+QString::number(rowid));
}

void LocalDatabase::set_start_time(int rowid, int datetime_timestamp)
{
    QSqlQuery query(db);
    query_execute(query,"update timeline set date_start = datetime("+QString::number(datetime_timestamp)+",'unixepoch', 'localtime') where rowid="+QString::number(rowid));
}

void LocalDatabase::set_file_as_uploaded(int file_id)
{
    QSqlQuery query(db);
    query.prepare("update file set uploaded=1 where rowid=:file_id");
    query.bindValue(":file_id",file_id);
    query_execute(query);
}

void LocalDatabase::append_file_to_paylist(int file_id, int playlist_id)
{
    QSqlQuery query(db);
    query_execute(query,"select files from playlist where rowid="+QString::number(playlist_id));
    query.first();
    QString r_files = query.value("files").toString();
    if(!r_files.isEmpty()){
        QStringList files = r_files.split(",");
        files.append(QString::number(file_id));
        r_files = files.join(",");
    }else{
        r_files = QString::number(file_id);
    }
    query.prepare("update playlist set files=:files where rowid=:playlist_id");
    query.bindValue(":files",r_files);
    query.bindValue(":playlist_id",playlist_id);
    query_execute(query);
}

bool LocalDatabase::delete_event(int rowid)
{
    QSqlQuery query(db);
    qDebug()<<"deleting event "<<rowid;
    return query_execute(query,"delete from timeline where rowid="+QString::number(rowid));
}

bool LocalDatabase::new_playlist(QString name, int type)
{
    QSqlQuery query(db);
    if(!query.prepare("insert into playlist (name,type) values(:name,:type)")){
        return false;
    }
    query.bindValue(":name",name);
    query.bindValue(":type",type);
    return query_execute(query);
}

bool LocalDatabase::add_event(int window_id, QString module, QString mode, uint time_begin, int priority, int draw_order, QString _params, QString playlist_name)
{
    qDebug()<<"module:"<<module<<"window:"<<window_id;
    //qDebug()<<_params;
    //qFatal("die");
    //getting playlist id
    int playlist_id = get_playlist_id_by_name(playlist_name);
    if(playlist_id<0){
        last_error="Playlist was not found!";
        return false;
    }

    QJsonDocument doc;
    doc = doc = doc.fromJson(QTextCodec::codecForName("UTF-8")->fromUnicode(_params));
    if(doc.isNull()){
        last_error = "Error parsing params json: '"+_params+"'";
        return false;
    }

    QVariantHash params;
    params.clear();
    //params.insert("module",module);
    //params.insert("window_id",window_id);
    //params.insert("mode",mode);
    //params.insert("files_list",get_playlist_filepaths(playlist_id).join("\n"));
    params.unite(doc.object().toVariantHash());

    QDateTime date;
    date.setTime_t(time_begin);
    QSqlQuery query(db);
    if(!query.prepare(""
                       "insert into timeline (window_id, module, mode, priority, draw_order, params, date_start,playlist_id) values ("
                       ":window_id,:module,:mode,:priority,:draw_order,:params,:date_start,:playlist_id);"
                       ))
    {
        last_error="Error in: "+query.lastQuery()+" | "+query.lastError().text();
        return false;
    }
    query.bindValue(":window_id",window_id);
    query.bindValue(":module",module);
    query.bindValue(":mode",mode);
    query.bindValue(":priority",priority);
    query.bindValue(":draw_order",draw_order);
    query.bindValue(":params",QString(doc.toJson()));
    query.bindValue(":date_start",date.toString("yyyy-MM-dd hh:mm:ss"));
    query.bindValue(":playlist_id",playlist_id);

    if(!query_execute(query)){
        return false;
    }

    int rowid = query.lastInsertId().toInt();
    ScheduleElement *el;
    el = new ScheduleElement(
                rowid,
                playlist_id,
                time_begin,
                priority,
                draw_order,
                window_id,
                module,
                mode,
                params);
    if(el->is_error()){
        delete_event(rowid);
        return false;
    }
    Schedule *sch = WindowCtr::get_schedule(window_id);
    sch->add_element(el);

    return true;
}


// Добавление элемента на линию времени из шаблона
bool LocalDatabase::add_from_template(QString name, int time_begin)
{
    QSqlQuery query(db);
    //getting template
    if(!query.prepare("select "
                           "window_id, "
                           "module, "
                           "mode, "
                           "priority, "
                           "draw_order, "
                           "params, "
                           "playlist_name "
                       "from template "
                        "where name = :name"))
    {
        return false;
    }
    query.bindValue(":name",name);
    query_execute(query);
    if(!query.first()){
        last_error = "Template '"+name+"', not found!";
        return false;
    }


    return add_event(query.value("window_id").toInt(),
                     query.value("module").toString(),
                     query.value("mode").toString(),
                     time_begin,
                     query.value("priority").toInt(),
                     query.value("draw_order").toInt(),
                     query.value("params").toString(),
                     query.value("playlist_name").toString()
                     );
}

//получение списка плейлистов
QStringList LocalDatabase::get_playlists(){
    QStringList res;
    QSqlQuery query(db);
    query_execute(query,"select name from playlist");
    while(query.next()){
        res.append(query.value(0).toString());
    }
    return res;
}

QJsonArray LocalDatabase::get_all_timeline()
{
    QJsonArray out;
    QJsonObject obj;
    QSqlQuery query(db);

    query_execute(query,
                        "select "
                        "tl.rowid, "
                        "tl.window_id, "
                        "tl.date_start, "
                        "strftime('%s',tl.date_start,'utc') as date_start_sec, "
                        "pl.duration, "
                        "tl.is_played, "
                        "tl.is_skipped, "
                        "tl.draw_order, "
                        "tl.module, "
                        "tl.mode, "
                        "tl.params, "
                        "tl.priority, "
                        "tl.playlist_id, "
                        "pl.name as playlist_name "
                  "from timeline tl "
                  "join playlist pl on pl.rowid = tl.playlist_id "
                  "order by date_start");

    while(query.next()){
        obj = {
            {"id",query.value("rowid").toInt()},
            {"window_id",query.value("window_id").toInt()},
            {"date_start",query.value("date_start").toString()},
            {"date_start_sec",query.value("date_start_sec").toString()},
            {"duration",query.value("duration").toInt()},
            {"is_played",query.value("is_played").toInt()},
            {"is_skipped",query.value("is_skipped").toInt()},
            {"draw_order",query.value("draw_order").toInt()},
            {"module",query.value("module").toString()},
            {"mode",query.value("mode").toString()},
            {"params",query.value("params").toString()},
            {"priority",query.value("priority").toInt()},
            {"playlist_id",query.value("playlist_id").toInt()},
            {"playlist_name",query.value("playlist_name").toString()}
        };
        out.append(obj);
    }

    return out;
}

int LocalDatabase::create_file(QString filename, int type, long unsigned int byte_length)
{
    QString ext;
    QSqlQuery query(db);
    ext = filename.mid(filename.lastIndexOf(".")+1);
    if(!query.prepare("insert into file (name, type, byte_length, uploaded, ext) "
                  "values ( "
                  ":filename, "
                  ":type, "+
                  QString::number(byte_length)+", "
                  "0, "
                  ":ext "
       ")"))
            return -1;
    query.bindValue(":filename",filename);
    query.bindValue(":type",type);
    query.bindValue(":ext",ext);

    if(!query_execute(query)){
        return -1;
    }

    int id = query.lastInsertId().toInt();

    return id;
}

int LocalDatabase::get_playlist_id_by_name(QString name)
{
    QSqlQuery query(db);
    query.prepare("select rowid from playlist where name =:name");
    query.bindValue(":name",name);
    if(!query_execute(query)){
        last_error = "Playlist '"+name+"' not found!";
        return -1;
    }
    if(!query.next()){
        last_error = "Playlist '"+name+"' not found!";
        return -1;
    }
    return query.value(0).toInt();
}

QStringList LocalDatabase::get_playlist_filenames(int playlist_id)
{
    QSqlQuery query(db);
    query.prepare("select files from playlist where rowid=:playlist_id");
    query.bindValue(":playlist_id",playlist_id);
    query.exec();
    query.first();
    QString files = query.value("files").toString();
    QStringList result;
    if(files.isEmpty())
        return result;
    query_execute(query,"select name from file where rowid in ("+files+")");
    while(query.next()){
        result.append(query.value(0).toString());
    }
    return result;
}

QJsonArray LocalDatabase::get_playlist_files(int playlist_id)
{
    QSqlQuery query(db);
    query.prepare("select files from playlist where rowid=:playlist_id");
    query.bindValue(":playlist_id",playlist_id);
    query_execute(query);
    query.first();
    QString files = query.value("files").toString();
    QJsonArray result;
    QJsonObject obj;
    if(files.isEmpty())
        return result;
    query_execute(query,"select rowid, name, ext from file where rowid in ("+files+")");
    while(query.next()){
        obj = {
            {"id",query.value("rowid").toInt()},
            {"name",query.value("name").toString()},
            {"ext",query.value("ext").toString()},
        };
        result.append(obj);
    }
    return result;
}

QStringList LocalDatabase::get_playlist_filepaths(int playlist_id)
{
    QSqlQuery query(db);
    query.prepare("select files from playlist where rowid=:playlist_id");
    query.bindValue(":playlist_id",playlist_id);
    query_execute(query);
    query.first();
    QString files = query.value("files").toString();
    QStringList result;
    if(files.isEmpty())
        return result;
    query_execute(query,"select rowid, ext from file where rowid in ("+files+")");
    QString path;
    while(query.next()){
        path=WindowCtr::file_storage_folder+query.value(0).toString()+"."+query.value(1).toString();
        result.append(path);
    }
    return result;
}

QString LocalDatabase::get_file_path_by_id(int file_id)
{
    QSqlQuery query(db);
    query.prepare("select ext from file where rowid=:file_id");
    query.bindValue(":file_id",file_id);
    if(!query_execute(query))
        return "";

    query.first();

    QString filename = WindowCtr::file_storage_folder+QString::number(file_id)+"."+query.value("ext").toString();
    return filename;
}

bool LocalDatabase::delete_file_from_disk(int file_id)
{
    //getting info
    QSqlQuery query(db);

    QString filename = get_file_path_by_id(file_id);
    if(filename.length()==0)
        return false;

    //deleting from files table
    query.prepare("delete from file where rowid=:file_id");
    query.bindValue(":file_id",file_id);
    if(!query_execute(query))
        return false;

    //deleting from timeline;

    if(!query_execute(query,"select rowid, files from playlist"))
        return false;
    QStringList files;
    QString orig_files;
    QStringList update_sql;
    QStringList update_files;

    QVector<int> playlist_to_update;

    bool found;
    while(query.next()){
        found = false;
        update_files.clear();
        orig_files = query.value("files").toString();
        files = orig_files.split(",");
        for(int i=0; i<files.length(); i++){
            if(files[i].toInt()==file_id){
                found=true;
            }else{
                update_files.append(files[i]);
            }
        }
        if(found){
            playlist_to_update.append(query.value("rowid").toInt());
            update_sql.append("update playlist set files = '"+update_files.join(",")+"' where rowid = "+query.value("rowid").toString());
        }
    }

    for(int i=0;i<update_sql.length();i++){
        if(!query_execute(query,update_sql[i]))
            return false;
    }

    for(int i=0;i<playlist_to_update.length();i++){
        WindowCtr::update_playlist(playlist_to_update[i]);
        int duration=-1;
        int playlist_type = get_playlist_type(playlist_to_update[i]);
        LocalDatabase db;
        switch(playlist_type){
            case G::PLAYLIST_TYPE_VIDEO:{
                VlcVideoPlayer player;
                player.setFileList(db.get_playlist_filepaths(playlist_to_update[i]));
                qDebug()<<"duration:"<<player.getDuration();
                duration = player.getDuration();
                }break;
            case G::PLAYLIST_TYPE_MUSIC:{
                VlcMusicPlayer player;
                player.setFileList(db.get_playlist_filepaths(playlist_to_update[i]));
                qDebug()<<"duration:"<<player.getDuration();
                duration = player.getDuration();
            }break;
        }

        db.update_playlist_duration(playlist_to_update[i],duration);
    }

    QFile file;
    file.setFileName(filename);
    if(!file.remove()){
        last_error = "Error deleting file '"+filename+"' from filesystem.";
        return false;
    }

    return true;
}

bool LocalDatabase::remove_event_from_timeline(int event_id)
{
    QSqlQuery query(db);
    query.prepare("delete from timeline where rowid=:event_id");
    query.bindValue(":event_id",event_id);
    if(!query_execute(query))
        return false;
    WindowCtr::remove_event(event_id);
    return true;
}

int LocalDatabase::get_playlist_type(int playlist_id)
{
    QSqlQuery query(db);
    query.prepare("select type from playlist where rowid=:playlist_id");
    query.bindValue(":playlist_id",playlist_id);
    if(!query_execute(query))
        return -1;
    query.first();
    return query.value("type").toInt();
}

int LocalDatabase::get_playlist_duration(int playlist_id)
{
    QSqlQuery query(db);
    query.prepare("select duration from playlist where rowid=:playlist_id");
    query.bindValue(":playlist_id",playlist_id);
    if(!query_execute(query))
        return -1;
    query.first();
    return query.value("duration").toInt();
}

bool LocalDatabase::delete_playlist(int playlist_id)
{
    WindowCtr::remove_events_by_playlist(playlist_id);

    QSqlQuery query(db);

    query.prepare("delete from timeline where playlist_id=:playlist_id");
    query.bindValue(":playlist_id",playlist_id);
    if(!query_execute(query))
        return false;

    //deleting files
    QJsonArray files = get_playlist_files(playlist_id);
    for(int i=0;i<files.count();i++){
        if(!delete_file_from_disk(files.at(i).toObject().value("id").toInt())){
            qDebug()<<"error deleting file"<<files.at(i);
            return false;
        }
    }

    //deleting playlist
    query.prepare("delete from playlist where rowid=:playlist_id");
    query.bindValue(":playlist_id",playlist_id);
    if(!query_execute(query))
        return false;

    return true;
}

bool LocalDatabase::update_playlist_duration(int playlist_id, int duration)
{
    QSqlQuery query(db);
    query.prepare("update playlist set duration=:duration where rowid=:playlist_id");
    query.bindValue(":playlist_id",playlist_id);
    query.bindValue(":duration",duration);
    if(!query_execute(query))
        return false;
    return true;
}

QJsonObject LocalDatabase::get_playlist_settings(int playlist_id)
{
    QSqlQuery query(db);
    query.prepare("select is_random from playlist where rowid=:playlist_id");
    query.bindValue(":playlist_id",playlist_id);

    QJsonObject obj;

    if(!query_execute(query))
        return obj;
    query.first();
    obj = {
        {"is_random",query.value("is_random").toInt()}
    };

    return obj;
}

bool LocalDatabase::set_playlist_settings(int playlist_id, bool is_random)
{
    QSqlQuery query(db);
    query.prepare("update playlist set is_random=:is_random where rowid=:playlist_id");
    query.bindValue(":playlist_id",playlist_id);
    query.bindValue(":is_random",is_random ? 1 : 0);

    if(!query_execute(query))
        return false;

    return true;
}

bool LocalDatabase::set_event_params(int event_id, QString params)
{
    QSqlQuery query(db);
    query.prepare("update timeline set params=:params where rowid=:event_id");
    query.bindValue(":event_id",event_id);
    query.bindValue(":params",params);
    if(!query_execute(query))
        return false;

    WindowCtr::set_event_params(event_id,params);

    return true;

}

//Создание файла для загрузки


QString LocalDatabase::get_last_error()
{ 
    qDebug()<<last_error;
    QString t = last_error;
    last_error = "";
    return t;
}



bool LocalDatabase::query_execute(QSqlQuery &query,QString query_string)
{

    if(!query.exec(query_string)){
        qDebug()<<"error executing query!";
        last_error=query.lastError().text();
        qDebug()<<last_error;
        return false;
    }
    return true;
}

bool LocalDatabase::query_execute(QSqlQuery &query)
{
    if(!query.exec()){
        qDebug()<<"error executing query!";
        last_error=query.lastError().text();
        qDebug()<<last_error;
        return false;
    }
    return true;
}

bool LocalDatabase::table_exsist(QString table_name)
{
    QSqlQuery query(db);
    query_execute(query,"SELECT count(name) FROM sqlite_master WHERE type='table' AND name='"+table_name+"';");
    query.first();
    return (query.value(0).toInt()>0);
}
